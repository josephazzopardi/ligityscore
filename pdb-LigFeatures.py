"""
# Course: MSc AI
# Date: 02/02/2020
# Author: Joseph Azzopardi
# Credits:
# File: pdb-LigFeature.py
# Version: 1.0
# Description: Program to generate LigityScore features using the HotSpots dataset created using pdb-LigHotpoints.py. 
#              Creates a HyperCube Feature per PL Complex for LigityScore3D, and a feature matrix for LigityScore1D.
#              DataFrame is saved to target location to be used in next step to split dataset in Training, Validation, Test sets.
"""

import itertools
import operator
import argparse
import os
import gc


import pandas as pd
import numpy as np

from itertools import combinations
from itertools import permutations 

from sbvscnnmsc.sbvsHelper import loadDataFrame
from sbvscnnmsc.sbvsHelper import saveDataFrame
from sbvscnnmsc.sbvsHelper import saveDataFrameFeather

#--df_in_dataset_filname "PDB2016dataframeOLD"
# Results Commands Final
# ----2016
# 1D 
#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-0  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features1D1-0"  --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-0-2016.txt
#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-1  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features1D1-1"  --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-1-2016.txt
#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-25 --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features1D1-25" --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-25-2016.txt
#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-4  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features1D1-4"  --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-4-2016.txt
#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-5  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features1D1-5"  --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-5-2016.txt
#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-6  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features1D1-6"  --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-6-2016.txt

#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-4L  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features1D1-4L"  --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-4L-2016.txt
#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-5L  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features1D1-5L"  --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-5L-2016.txt

#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-4  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features1D1-4-30" --maxDist 30 --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-4-30-2016.txt
#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-4  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features1D1-4-40" --maxDist 40 --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-4-40-2016.txt

#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-4  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features1D1-4-0-5" --maxDist 20 --resolution 0.5 --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-4-0-5-2016.txt
# ----
# 3D
#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-0  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features3D1-0"  --LigDimension 3 2>&1 | tee msc-results-ligfeatures-console-output-3D-1-0-2016.txt
#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-1  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features3D1-1"  --LigDimension 3 2>&1 | tee msc-results-ligfeatures-console-output-3D-1-1-2016.txt
#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-25 --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features3D1-25" --LigDimension 3 2>&1 | tee msc-results-ligfeatures-console-output-3D-1-25-2016.txt
#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-4  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features3D1-4"  --LigDimension 3 2>&1 | tee msc-results-ligfeatures-console-output-3D-1-4-2016.txt
#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-5  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features3D1-5"  --LigDimension 3 2>&1 | tee msc-results-ligfeatures-console-output-3D-1-5-2016.txt
#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-6  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features3D1-6"  --LigDimension 3 2>&1 | tee msc-results-ligfeatures-console-output-3D-1-6-2016.txt

#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-4L --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features3D1-4L" --LigDimension 3 2>&1 | tee msc-results-ligfeatures-console-output-3D-1-4L-2016.txt
#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-5L --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features3D1-5L" --LigDimension 3 2>&1 | tee msc-results-ligfeatures-console-output-3D-1-5L-2016.txt

#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-4  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features3D1-4-30" --maxDist 30 --LigDimension 3 2>&1 | tee msc-results-ligfeatures-console-output-3D-1-4-30-2016.txt
#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-4  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features3D1-4-40" --maxDist 40 --LigDimension 3 2>&1 | tee msc-results-ligfeatures-console-output-3D-1-4-40-2016.txt

#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints1-4  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2016-Lig-Features3D1-4-0-5" --maxDist 20 --resolution 0.5 --LigDimension 3 2>&1 | tee msc-results-ligfeatures-console-output-3D-1-4-0-5-2016.txt
# ----
# ----2018
# 1D 
#python3 pdb-LigFeatures.py --df_in_dataset_filname "PDB2018dataframe" --df_in_hotpoint_filename PDB2018Hotpoints1-0  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2018-Lig-Features1D1-0"  --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-0-2018.txt
#python3 pdb-LigFeatures.py --df_in_dataset_filname "PDB2018dataframe" --df_in_hotpoint_filename PDB2018Hotpoints1-1  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2018-Lig-Features1D1-1"  --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-1-2018.txt
#python3 pdb-LigFeatures.py --df_in_dataset_filname "PDB2018dataframe" --df_in_hotpoint_filename PDB2018Hotpoints1-25 --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2018-Lig-Features1D1-25" --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-25-2018.txt
#python3 pdb-LigFeatures.py --df_in_dataset_filname "PDB2018dataframe" --df_in_hotpoint_filename PDB2018Hotpoints1-4  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2018-Lig-Features1D1-4"  --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-4-2018.txt
#python3 pdb-LigFeatures.py --df_in_dataset_filname "PDB2018dataframe" --df_in_hotpoint_filename PDB2018Hotpoints1-5  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2018-Lig-Features1D1-5"  --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-5-2018.txt
#python3 pdb-LigFeatures.py --df_in_dataset_filname "PDB2018dataframe" --df_in_hotpoint_filename PDB2018Hotpoints1-6  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2018-Lig-Features1D1-6"  --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-6-2018.txt

#python3 pdb-LigFeatures.py --df_in_dataset_filname "PDB2018dataframe" --df_in_hotpoint_filename PDB2018Hotpoints1-4L  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2018-Lig-Features1D1-4L"  --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-4L-2018.txt
#python3 pdb-LigFeatures.py --df_in_dataset_filname "PDB2018dataframe" --df_in_hotpoint_filename PDB2018Hotpoints1-5L  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2018-Lig-Features1D1-5L"  --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-5L-2018.txt

#python3 pdb-LigFeatures.py --df_in_dataset_filname "PDB2018dataframe" --df_in_hotpoint_filename PDB2018Hotpoints1-4  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2018-Lig-Features1D1-4-30" --maxDist 30 --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-4-30-2018.txt
#python3 pdb-LigFeatures.py --df_in_dataset_filname "PDB2018dataframe" --df_in_hotpoint_filename PDB2018Hotpoints1-4  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2018-Lig-Features1D1-4-40" --maxDist 40 --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-4-40-2018.txt

#python3 pdb-LigFeatures.py --df_in_dataset_filname "PDB2018dataframe" --df_in_hotpoint_filename PDB2018Hotpoints1-4  --verbose 0 --input_dir_df "./LigityScore_Output" --output_dir "./LigityScore_Output" --df_out_features_filename "PDB2018-Lig-Features1D1-4-0-5" --maxDist 20 --resolution 0.5 --LigDimension 1 2>&1 | tee msc-results-ligfeatures-console-output-1D-1-4-0-5-2018.txt
# ----




# Script Arguments
# -------------------------------
parser = argparse.ArgumentParser(description='LigityScore Hotpoint Extraction',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

io_group = parser.add_argument_group('Input & Output Files-Directories')

io_group.add_argument('--input_dir_df',  '-idf', default='/home/joseph/msc/dissertation/myscripts/LigityScore_Output',
                      help='Directory containing the saved dataframes from previous scripts - (1) PDBBind details, and (2) Hotpoints')
io_group.add_argument('--df_in_dataset_filname', default='PDB2016dataframe',
                      help='Filename for saved dataframe that contains all PDB Codes.')
io_group.add_argument('--df_in_hotpoint_filename', default='PDB2016Hotpoints',
                      help='Filename for saved dataframe that contains the list of conformant LigityScore PL Hotpoints.')
io_group.add_argument('--output_dir', '-o', default='/home/joseph/msc/dissertation/myscripts/LigityScore_Output',
                      help='Directory to store Dataframe containing LigityScore HyperCube features.')
io_group.add_argument('--df_out_features_filename', default='PDB2016-Lig-Features',
                      help='Filename for saved dataframe that contains the list of conformant LigityScore PL Hotpoints.')

features_group = parser.add_argument_group('Feature Variables that effect feature matrix or hypercube output')
features_group.add_argument('--maxDist', '-mD', default=20, type=int, 
                            help='Max distance to allow. Feature distance exceeding this threshold will be discarded as they fall out of the feature matrix or hypercube.')
features_group.add_argument('--resolution', '-r', default=1.0, type=float, 
                            help='Resolution to discretize distances. Allowed values are \'1.0\' or \'0.5\'.')


runtime_group = parser.add_argument_group('Runtime Variables')
runtime_group.add_argument('--verbose', '-v', default=0, type=int, 
                           help='Enable Script Print output.')
runtime_group.add_argument('--iterations', '-it', default=-1, type=int,
                           help='Number of PDBCodes to process. To be used whilst debugging.')
runtime_group.add_argument('--LigDimension', '-ld', default=3, type=int,
                           help='Number of features to group to find distance.  \'1\' of \'3\' are allowed values. \
                           A \'1\' implies that features will be grouped in pairs producing one distance (1D). \
                           A \'3\' implies that 3 features will be grouped forming a traingle and producing 3 distance (3D).')
runtime_group.add_argument('--nptype', '-npt', default="np.int16",
                           help='NP dtype used to initialize feature cube.')


args = parser.parse_args()


# TO DO - Validate Inputs


class Cube:
    # Store LigityScore3D details per Protein-Ligand Complex
    def __init__(self):
        self.featureCube = None
        self.binSize = None

class Matrix:
    # Store LigityScore1D details per Protein-Ligand Complex
    def __init__(self):
        self.featureMatrix = None
        self.binSize = None

class binCoord:
    # Store binning coordinates for LigityScore3D
    def __init__(self):
        self.x = None
        self.y = None
        self.z = None


def eucDist(a, b):
    # Compute Euclidean distance of paramaters a, and b.

    a = np.asarray(a)
    b = np.asarray(b)
    d = (a-b)**2
    d = (sum(d))**0.5
    return d

def getIndicesPL(l_set, P_set, l_r_num, P_r_num, n):
    
    """
    Function to provide the indices for the combintation of features from the ligand feature dataframe 
    and the Protein feature dataframe.
    
    Arguments:
        l_set (set): Set containing all the ligand PIPs for a particular protein-ligand complex.
        P_set (set): Set containing all the protein PIPs for a particular protein-ligand complex.
        l_r_num (int): Number of PIPs to take from ligand pool at a time.
        P_r_num (int): Number of PIPs to take from protein pool at a time.
        n (int):  Number of Features (PIPs) to consider. 2 imples 1D since only one demnsion can be taken, whilst 3
                  implies a triangle structure with 3 distances.
    Returns:
        indicesPL (list): List of lists containing all the indices of the 3-PIP in sequence.
    """
    
    PLset = []
    indicesPL = []
    temp = []
    c = 0

    l_comb = combinations(l_set, l_r_num)
    
    for l_index in l_comb:
        P_comb = combinations(P_set, P_r_num )
        for P_index in P_comb:
            PLset.extend(list(l_index)) 
            PLset.extend(list(P_index))
            #print (*l_index, *P_index)
            c += 1
            
    for i, index in enumerate(PLset, 1):   #group each 3 index in a seperate list
        temp.append(index)
        if i % n == 0:
            indicesPL.append(temp)
            temp = []
    
    if n == 2:
        print ("\n\tTotal number of PIP interations with %d Ligand and %d Protein: \t%d" % (l_r_num, P_r_num, c))
    elif n ==3:
        print ("\n\tTotal number of 3-PIP triangles with %d Ligand and %d Protein: \t%d" % (l_r_num, P_r_num, c))
    
    return indicesPL


def mysort (unsortedList):
    """
    Function to return a sorted List after sorting using the first to element of the list.
    This is used to match all 3-PIP family sequences.
    
    parameters:
        unsortedList (List): List of lists with 3 elements    
    returns: 
        sortedList (List): list sorted based on the first 2 elements
    """

    # sort on the first two rows from the provided list.
    sortedList = sorted (unsortedList, key = lambda k: (k[0], k[1]) )
    #print (sortedList)
    
    #fam_combs = sorted (fam_combs, key = operator.itemgetter(0, 1, 2))
    #fam_combs = sorted(sorted (fam_combs, key=lambda x: x[0]), key=lambda x: x[1])
    
    return sortedList


def setCube (fam_comb_num, nptype, cubeMaxDist=20, res = 1.0):
    """
    Function to initialize Feature HyperCube for LigityScore3D. Since the 3D feature object can be large the dtype is change to uint8 or int16 
    in order to reduce memory requirements.

    Arguments:
        fam_comb_num (int): Number of 3 different family combinations (with replacement)
        nptype (string): dtype to define initalize type of np.array (int16 by default). uint8 can only be used for PIP distance threshold factor of 1.0.
        cubeMaxDist (int):  Maximum distance to allowed between PIPs
        res (float):        Resolution for distance discretisation. Allowed values 0.5 or 1.0.
    Return: 
        c (Cube()):         class of type 'Cube' that stored bin size and zeroed feature cube.
    """
    c = Cube()
    
    c.binSize = int(cubeMaxDist / res)  
    #c.featureCube = np.zeros ((c.binSize*fam_comb_num, c.binSize, c.binSize), dtype=np.uint8)
    c.featureCube = np.zeros ((fam_comb_num*(c.binSize+1), c.binSize+1, c.binSize+1), dtype=eval(nptype)) #add zero position
    #print ("\nSize of Feature Cube per PL complex: \t %s" % str(c.featureCube.shape))
    
    return c


def setMatrixLigityScore1D (fam_comb_num, cubeMaxDist=20, res = 1.0):
    """
    Function to initialize Feature Matrix for LigityScore1D

    Arguments:
        fam_comb_num(int): Number of 2 different family combinations (with replacement)
        cubeMaxDist(int):  Maximum distance to allowed between PIPs
        res (float):       Resolution for distance discretisation. Allowed values 0.5 or 1.0.
    Return: 
        m (Matrix):       class of type 'Matrix' that stores bin size and zeroed feature matrix.
    """
    m = Matrix()
    
    m.binSize = int(cubeMaxDist / res)
    m.featureMatrix = np.zeros ((fam_comb_num, m.binSize+1))
    
    #print ("\nSize of Feature Cube per PL complex: \t %s" % str(c.featureCube.shape))
    
    return m


def getBinRound (d, r):
    """
    Function to round computed 3-PIP distances.
    
    parameters:
        d (List):  List of np.array for computed 3-PIP distances.
        r (float): Binning Resolution
    return:
        distances (List):  Return rounded distanced based on the given resolution.
    """

    if not (r == 1.0 or r == 0.5):
        raise ValueError("Incorrect resolution!\n\t    %f provided. Allowed values are \'0.5\' or  \'1.0\'.\n" % r)
    
    d = np.asarray(d) # convert list of np.asarray
    
    if r == 0.5:
        
        distances = ((np.round(d*2, decimals=0)))/2
        
    elif r == 1: 
        
        distances = (np.round(d, decimals=0)).astype(int)
    
    
    return distances


def famOption(f, verbose):
    """
    Function to determine the which set of families are given (same family, all different, or two the same).

    Parameters:
        f (List): List of Families
        verbose (int) : Print Verbose output.
    Returns:
        family_count_max (int): Options from 1,2,3 used to determine Family Case.
    """
    
    family_count_max = 0

    for family in f:
        #print(f.count(family))
        c = f.count(family)

        if family_count_max < c:
            family_count_max = c
    
    if verbose:
        print ("\nFeature Family Case is: %d" % family_count_max)
        
    return family_count_max


def getBinningCoord(f1, f2, f3, distances, verbose):
    """
    Function to return the binning coordinate (x, y, z) of the bin to increment.

    Parameters:
        f1, f2, f3 (string): Family for features
        distances (List): List of distances between feature (already rounded off)
        verbose (int) : Print Verbose output.
    Returns:
        x, y, z (int):
    """
    
    coord = binCoord()  # class for coordinates to return
    f = [f1, f2 ,f3]
    
    # Determine Family Option (all same, all distinct, two of a kind)
    family_count_max = famOption(f, verbose)

    # To select cube coordinates for a set of 3 families there are 3 cases!
    
    # example: Acceptor-Acceptor-Donor
    # p1               p2               p3
    # |------d1(x)---->|------d2(y)---->|
    # A -------------- A -------------- D
    # |<--------------d3(z)-------------|
    #

    ## CASE 1: 2 out of the 3 Feature families are of the same type
    ##         This imples 6 distance permutations, and results in 3 binning options. Since two families are the same 
    ##         there can be the case of mirrored traingles.

    bin_options_list = []

    if family_count_max == 2:
        
        # Set Family Pairs (AA-->AD-->DA)
        family_comb = [f1+":"+f2, f2+":"+f3, f3+":"+f1]

        # Get all permutations of distances 
        bin_options = permutations(distances)

        for b in bin_options: # convert tuples to list
            bin_options_list.append(list(b)) 
            #print (bin_options_list)
    
        # Create DataFrame to store possible distances combinations
        df = pd.DataFrame(np.asarray(bin_options_list), columns = family_comb)
        df['Valid_Bin'] = False

        if verbose:
            print ("Valid Bins DataFrame:")
            print (df.head(6))

        c = 1
        
        # Check which pairs are possible Mirrored Triangles.
        # Iterate over index combinations
        for bin_pair in combinations(df.index, 2):

            if bin_pair[0] > 0:
                # we are only interested in matching the first row with rest of rows. others are discarded.
                continue
            
            if verbose:
                print ("\nBin Pair: \t", bin_pair)
            
            # Create tuples for key value pair (distance, family1:family2) for FIRST row selected
            a1  = (df.loc[bin_pair[0], df.columns[0]], df.columns[0])
            a2  = (df.loc[bin_pair[0], df.columns[1]], df.columns[1]) 
            a3  = (df.loc[bin_pair[0], df.columns[2]], df.columns[2])
            
            # Create tuples for key value pair (distance, family1:family2) for SECOND row selected
            b1  = (df.loc[bin_pair[1], df.columns[0]], df.columns[0]) 
            b2  = (df.loc[bin_pair[1], df.columns[1]], df.columns[1])
            b3  = (df.loc[bin_pair[1], df.columns[2]], df.columns[2]) 
            
            # Create tuples for key value pair (distance, family2:family1) for SECOND row selected with REVERSED families (AD = DA)
            b10 = (df.loc[bin_pair[1], df.columns[0]], df.columns[0].split(':')[1]+":"+df.columns[0].split(':')[0]) 
            b20 = (df.loc[bin_pair[1], df.columns[1]], df.columns[1].split(':')[1]+":"+df.columns[1].split(':')[0])
            b30 = (df.loc[bin_pair[1], df.columns[2]], df.columns[2].split(':')[1]+":"+df.columns[2].split(':')[0]) 

            a  = [a1, a2, a3]
            b  = [b1, b2, b3] 
            b0 = [b10, b20, b30]

            if verbose:
                print ("\nRow %d: %s" % (bin_pair[0], str(a)))
                print ("Row %d: %s" % (bin_pair[1], str(b)))
            
            # Sort Distance  in Ascending order for comparison
            a  = sorted (a)
            b  = sorted (b)
            b0 = sorted (b0)

            if verbose:
                print ("\nRow %d Sorted: %20s" % (bin_pair[0], str(a)))
                print ("Row %d Sorted: %20s" % (bin_pair[1], str(b)))
                print ("Row %d Sorted and families reversed: %10s\n" % (bin_pair[1], str(b0)))
            
            # Check if elements of two rows have equal tuples (distance, family1:family2)
            if (a[0] == b[0] or a[0] == b0[0]) and (a[1] == b[1] or a[1] == b0[1]) and (a[2] == b[2] or a[2] == b0[2]):
                if verbose:
                    print ("Same Bin\n")
                valid_bin_pair = bin_pair
            else:
                if verbose:
                    print ("Different Bin")

            if c == -1:
                    break
        
        # Update DataFrame with rows for Mirrored Triangles
        df.loc[valid_bin_pair[0], "Valid_Bin"] = True
        df.loc[valid_bin_pair[1], "Valid_Bin"] = True
        
        # Filter DataFrame
        df = df[df["Valid_Bin"] == True]

        if verbose:
            print ("Valid Bins:")
            print (df.head(6))


        # Order Valid Bins
        df.sort_values([df.columns[0], df.columns[1]], ascending=[True, True], inplace=True)
        if verbose:
            print ("Valid Bins:")
            print (df.head(6))

        coord.x, coord.y, coord.z = df.iloc[0, 0], df.iloc[0, 1], df.iloc[0, 2]
        if verbose:
            print("Increment Bin at:\t x:%d  y:%d  z:%d" % (coord.x, coord.y, coord.z))




    ## CASE 2: All Feature families are of the same type.
    ##         This imples 1 distance permutations, and therefore results in 1 binning options.
    ##         Bin is the SORTED distance (ascending order) - since all 6 permutations correspond to 1 bin

    elif family_count_max == 3:
        distances = sorted(distances)
        coord.x, coord.y, coord.z = distances[0], distances[1], distances[2]
        if verbose:
            print("Increment Bin at:\t x:%d  y:%d  z:%d" % (coord.x, coord.y, coord.z))

     
    
        
    ## CASE 3: All Feature families are distinct.
    ##         This imples 6 distance permutations, and therefore results in 6 binning options.
    ##         Bin is the original UNSORTED distance


    elif family_count_max == 1:
        coord.x, coord.y, coord.z = distances[0], distances[1], distances[2]
        if verbose:
            print("Increment Bin at:\t x:%d  y:%d  z:%d" % (coord.x, coord.y, coord.z))

            
            
    else:
        print ("Error")
    
    
    return coord



def getBinningCoord2(f1, f2, f3, distances, verbose):
    """
    Function to return the binning coordinate (x, y, z) of the bin to increment.
    Function enhanced over getBinningCoord as it uses less code and caters for all possible scenarios directly.
    To provide a 3-PIP combination to the correct bin, the 3-PIP's distance and family type are sort using both values.

    Parameters:
        f1, f2, f3 (string): Family for features
        distances (List): List of distances between feature (already rounded off)
        verbose (int) : Print Verbose output.
    Returns:
        x, y, z (int):
    """

    coord = binCoord()  # class for coordinates to return
    f = [f1, f2 ,f3]
    
    if verbose:
        print ("\nBinning Coord ...\nFamily Pairs: %65s" % str(f))

    # To select cube coordinates for a set of 3 families there are 3 cases!

    # example: Acceptor-Acceptor-Donor
    # p1               p2               p3
    # |------d1(x)---->|------d2(y)---->|
    # A -------------- A -------------- D
    # |<--------------d3(z)-------------|
    #

    # Compute family interactions
    #Hydrophobe, Acceptor, Donor, PosIonizable, NegIonizable, Aromatic 
    fam_symbol = { "Acceptor":"A", "Aromatic":"Ar", "Donor":"D", "Hydrophobe":"H", "PosIonizable":"+", "NegIonizable":"-"}
    
    f_bar = f[1:]
    f_bar.append(f[0])

    # Get Family pairs for Distance. Convert using 'Family Symbol' 
    # Sort Family pairs so that family pairs are always represented by same set of symbols
    # Contatenate Sorted Family Symbols.
    fam_pairs = [ "".join(sorted(list([fam_symbol[f1], fam_symbol[f2]]))) for f1, f2 in zip(f, f_bar)]
    if verbose:
        print ("Family Pairs with keys sorted and summarised: %15s" % str(fam_pairs))

    # Combine Family Pairs with their corresponding distance
    fam_pairs = list(zip(fam_pairs, distances))
    if verbose:
        print ("Family Pairs with Distance: %51s" % str(fam_pairs))  

    # Final Sort using both Family Pairs and distance and sorting keys.
    fam_pairs = sorted(fam_pairs, key=lambda x: (x[0],x[1]))
    if verbose:
        print ("Sorted Family Pairs with Distance: %44s" % str(fam_pairs))  

    coord.x, coord.y, coord.z = fam_pairs[0][1], fam_pairs[1][1], fam_pairs[2][1]
    if verbose:
        print("Increment Bin at:\t x:%d  y:%d  z:%d\n" % (coord.x, coord.y, coord.z))

        
    return coord



def GenerateLigFeatures3D(PDBHotpoints, PL_comb_list, allowed_family, cubeMaxDist=20, r=1.0, verbose=0):
    
    """
    Function used to Generate 3D HyperCuber for a given PL complex using its combinations of Ligand and Protein Hotpoints.

    Parameters:
        PDBHotpoints (DataFrame): DataFrame containing all hotpoints for each complex in PDBBind. 
        PL_comb_list (List of Lists): List of lists containing indexes of 3-PIP combination to process.
        allowed_family (List): List of strings containing allowed pharmacophoric family types.
        cubeMaxDist (int): Default is 20
        r (int): Default is 1.0
        verbose (int): Default is 0
    Return:
        df_Lig_Features (DataFrame): DataFrame of the PDBbind dataset including items such as PDBCode, Folder Location, and HyberCube Features.
    """
    
    
    c = 0                           # Counter to keep track of number of PL complexes processed.
    combIgnored = 0                 # Counter to keep track of any combination that exceed cubeMaxDist
    fam_comb_Dict = {}              # Dictionary to store index:3-PIP-Sequence key-value pair

    #select PIP pairs for a particular PDBCode
    d = PDBHotpoints
    #d = PDBHotpoints.copy()

    #d.groupby('l_PDBCode').head()
    d['l_PDBCode'].unique()

    totalCodes = len(d['l_PDBCode'].unique())

    # Get combinations from families. Combination should be done with replacement since each PIP can be any item from the list of allowed families
    fam_combs = list(itertools.combinations_with_replacement(sorted(allowed_family), numPIPs))

    # Filter by the first two elements as key. Since we have 3 elements this will result in a unique combination.
    # This combination can be used to the 3-PIPs for a PL complex. The 3-PIP need to be sorted in the same way.
    #fam_combs.sort(key = lambda k: (k[0], k[1]) )
    fam_combs = mysort (fam_combs)

    # assign index to each family combination

    if verbose:
        print ("Family Dictionary:\n")
        
    for i, fam_comb in enumerate(fam_combs, 0):
        # key:value - The key is the family combination. Value is the index.
        fam_comb_Dict.update( {fam_comb : i} )
        if verbose:
            print("%d, %s"% (i, str(fam_comb)) )
            
    fam_comb_num = len(fam_comb_Dict) # Get number total number of difference sequences
            

    for plcomplex in d['l_PDBCode'].unique(): #iterate for every PL complex
        
        #plcomplex = "1sc8" #Override PDBCode to test a particular code. set args.iterations=1
        

        print ("\n\n-------------------------\nPL Complex:\t%s\n" % plcomplex)
        
        # Find unique PIPs for Query Ligand and Protein	
        #temp = d.copy()
        temp = d
        temp = temp[temp['l_PDBCode'] == plcomplex]
        #l_temp = temp.iloc[:, :6].drop_duplicates(subset=['l_atom'])
        #P_temp = temp.iloc[:, 6:].drop_duplicates(subset=['P_atom'])
        l_temp = temp.iloc[:, :5].drop_duplicates(subset=['l_fPos'])
        P_temp = temp.iloc[:, 5:].drop_duplicates(subset=['P_fPos'])
        
        #print(P_temp.head())
        
        # LumpedHydrophobe Features are changed to Hydrophobe and it is assumed they are the same feature.
        pd.set_option('mode.chained_assignment', None)
        l_temp['l_family'][l_temp['l_family'] == 'LumpedHydrophobe'] = "Hydrophobe"
        P_temp['P_family'][P_temp['P_family'] == 'LumpedHydrophobe'] = "Hydrophobe"
        
        # Find combinations from the Unique Protein Ligand Features.
        l = set(l_temp.index)
        P = set(P_temp.index)
        
        if verbose:
            print("\tLigand Set:\t", l)
            print("\tProtein Set:\t", P)
        
        # Initialize the cube for PL complex
        PL = setCube (fam_comb_num, args.nptype, cubeMaxDist, r)

        #featureCube = np.zeros ((cubeMaxDist*len(fam_comb_Dict), cubeMaxDist, cubeMaxDist))
        #print ("Size of Feature Cube per PL complex: \t %s" % str(featureCube.shape))
        if verbose:
            print ("\nSize of Feature Cube per PL complex: \t %s" % str(PL.featureCube.shape))
            print ("Bin Size: ", PL.binSize)
                  
            
        for i in range(len(PL_comb_list)): # iterate for different sets
            
            # Find combinations with 1 Ligand and 2 Protein PIPs
            # Find combinations with 2 Ligand and 1 Protein PIPs
        
            l_r_num = PL_comb_list[i][0]  # First Element is number Ligand Features 
            P_r_num = PL_comb_list[i][1]  # Second Element is number Protein Features

            # find total number of 3-PIP sets. Change l_r_num and P_r_num to different combinations across sets.
            #print(getIndicesPL(l, P, l_r_num, P_r_num))
            indicesPL = getIndicesPL(l, P, l_r_num, P_r_num, numPIPs)
            
            if verbose:
                print ("\n\tSample of \'indicesPL\':", indicesPL[0:5])
            
            # Get values for 'family' and 'distance' for each PIP bases on distances.
            # For each 3-PIP combination find distances            
            for index in indicesPL: # iterate over each 3-PIP index
            
                if l_r_num == 1 and P_r_num == 2:
                    # Get PIP information from dataframes - Family Feature Label and Feature Position
                    f1 = l_temp.loc[index[0],'l_family']
                    f2 = P_temp.loc[index[1],'P_family'] 
                    f3 = P_temp.loc[index[2],'P_family']
                    p1 = l_temp.loc[index[0],'l_fPos']
                    p2 = P_temp.loc[index[1],'P_fPos']
                    p3 = P_temp.loc[index[2],'P_fPos']
                elif l_r_num == 2 and P_r_num == 1:
                    f1 = l_temp.loc[index[0],'l_family']
                    f2 = l_temp.loc[index[1],'l_family'] 
                    f3 = P_temp.loc[index[2],'P_family']
                    p1 = l_temp.loc[index[0],'l_fPos']
                    p2 = l_temp.loc[index[1],'l_fPos']
                    p3 = P_temp.loc[index[2],'P_fPos']
                else:
                    raise ValueError("Value for \'l_r_num\' and \'P_r_num\' not correct. %d and %d provided. Expected [1,2] or [2,1]" % (l_r_num, P_r_num ))


                # Find the Cube Index from dict based on 3 PIP triangles.
                PIPtriangle = mysort(list((f1, f2, f2)))
                #PIPtriangle = ('Donor', 'Aromatic', 'Hydrophobe') # Uncomment to Test try-excempt with non-Existing Family Sequence

                try:
                    cubeIndex = fam_comb_Dict[tuple(PIPtriangle)]
                except KeyError as err:
                    print("ERROR: 3PIP Combination Not Found!\nCombination may not sorted by 1st and 2nd columns.\nKey Provided: {0} \n\n".format(err))
                    #raise ValueError('features must be an array of floats of shape (N, F)')

                # Get Binning coordinates
                #
                # example: Acceptor-Acceptor-Donor
                # p1               p2               p3
                # |------d1(x)---->|------d2(y)---->|
                # A -------------- A -------------- D
                # |<--------------d3(z)-------------|
                #
                distances = [eucDist(p1, p2), eucDist(p2, p3), eucDist(p3, p1)]
                #print (distances[0], distances[1], distances[2])

                # Round Distances for binning. This will be used as coordinates.
                #distances = (np.round(distances, decimals=0)).astype(int)
                distances = getBinRound (distances, r)

                ## TO DO sort by pairs and values
                #distances = getBinRound (distances, r)
                #getBinningCoord(f1, f2, f3, distances, verbose)
                bin_coords = getBinningCoord2(f1, f2, f3, distances, verbose)

                # Get Coordinates for Feature Cube
                x, y, z = bin_coords.x, bin_coords.y, bin_coords.z

                # Filter by max distance of 20
                if x > PL.binSize or y > PL.binSize or z > PL.binSize:
                    if verbose:
                        print ("Coord x, y or x reached Max distance of %3d. Ignore value" % cubeMaxDist)
                    combIgnored += 1
                    continue       # Skip Combination (Do not add to hypercube since out of range)

                # Find sub-Cube for 3PIP Family sequence by shifting x-coord by multiple of max bins 
                x = x + (cubeIndex*PL.binSize)

                if verbose:
                    print ("PIP Family Sequence:", PIPtriangle)
                    print ("Cube Index for Family Sequence:", cubeIndex)
                    print ("Cube Coordinates ->  x:%d  y:%d  z:%d \n\n" % (x, y, z))
                
                #if c == 9:
                #    PL.featureCube[x, y, z] += 254

                # Bin 3-PIP sample in the correct grid bin. Increment the bin count
                PL.featureCube[x, y, z] += 1

                if args.nptype == "np.uint8":
                    if PL.featureCube[x, y, z] == 255: #if 255 is reach with uint8 overflow of 255 is assigned. Therefore limit is 255 to flag error.
                        print ("****Binning Count: %d" % PL.featureCube[x, y, z])
                        raise ValueError('Binning Count Reached 255 Value. Change Type of np array to int16')
                    
                #print ("****Binning Count: %d" % PL.featureCube[x, y, z])                       
        

        # Add the LigityScore Feature to the features dataframe. The row index return an np.array. np.asscalar is used to convert to int.
        df_Lig_Features_index = df_Lig_Features['features'][df_Lig_Features['pdbCode'] == plcomplex].index.values
        #print ("\n\n")
        #print (type(df_Lig_Features_index))
        #print (df_Lig_Features_index)
        print ("\ndf_Lig_Features index: %10d" % df_Lig_Features_index.item())
        #df_Lig_Features.loc[ df_Lig_Features_index.item(), "features"]  = (PL.featureCube)
        df_Lig_Features.loc[ df_Lig_Features_index.item(), "features"]  = [PL.featureCube]
        #print (type(df_Lig_Features.loc[ df_Lig_Features_index.item(), "features"] ))
        df_Lig_Features.loc[ df_Lig_Features_index.item(), "dataAvail"] = True
        
        if combIgnored != 0:
            print ("\nPL Complex \'%s\' had %d points outside the coordinate space.\n" % (plcomplex, combIgnored))
            combIgnored = 0

        c += 1
        #if verbose:
        print ("\nIteration: %10d/%d" % (c, totalCodes))
        print ("Memory Usage: %12.2f Mb\n"  % (df_Lig_Features['features'].memory_usage(index = True, deep=True)/(1024*1024) ) )

        #watch -n 1 free -m
        if c == args.iterations:
            break

    
    return df_Lig_Features



def GenerateLigFeatures1D(PDBHotpoints, PL_comb_list, allowed_family, cubeMaxDist=20, r=1.0, verbose=0):
    
    """
    Function used to Generate 1D feature matrix for a given PL complex using its combinations of Ligand and Protein Hotpoints.
    Parameters:
        PDBHotpoints (DataFrame): DataFrame containing all hotpoints for each complex in PDBBind. 
        PL_comb_list (List of Lists): List of lists containing indexes of 2-PIP combination to process.
        allowed_family (List):
        cubeMaxDist (int): Default is 20
        r (int): Default is 1.0
        verbose (int): Default is 0
    Return:
        df_Lig_Features (DataFrame): DataFrame of the PDBbind dataset including items such as PDBCode, Folder Location, and HyberCube Features.
    """
    
    
    c = 0                           # Counter to keep track of number of PL complexes processed.
    combIgnored = 0                 # Counter to keep track of any combination that exceed cubeMaxDist
    fam_comb_Dict = {}              # Dictionary to store index:3-PIP-Sequence key-value pair

    #select PIP pairs for a particular PDBCode
    d = PDBHotpoints
    #d = PDBHotpoints.copy()

    #d.groupby('l_PDBCode').head()
    d['l_PDBCode'].unique()

    totalCodes = len(d['l_PDBCode'].unique())

    # Get combinations from families. Combination should be done with replacement since each PIP can be any item from the list of allowed families
    fam_combs = list(itertools.combinations_with_replacement(sorted(allowed_family), numPIPs))

    # Filter by the first two elements as key. 
    fam_combs = mysort (fam_combs)

    # assign index to each family combination
    if verbose:
        print ("Family Dictionary:\n")
        
    for i, fam_comb in enumerate(fam_combs, 0):
        # key:value - The key is the family combination. Value is the index.
        fam_comb_Dict.update( {fam_comb : i} )
        if verbose:
            print("%d, %s"% (i, str(fam_comb)) )
            
    fam_comb_num = len(fam_comb_Dict) # Get number total number of difference sequences
            

    for plcomplex in d['l_PDBCode'].unique(): #iterate for every PL complex
        
        #plcomplex = "1sc8" #Override PDBCode to test a particular code. set args.iterations=1
        
        print ("\n\n-------------------------\nPL Complex:\t%s\n" % plcomplex)
        
        # Find unique PIPs for Query Ligand and Protein	
        #temp = d.copy()
        temp = d
        temp = temp[temp['l_PDBCode'] == plcomplex]

        #temp['l_fPos'] = [(lambda x: tuple(x))(x) for x in temp['l_fPos']]
        #temp['P_fPos'] = [(lambda x: tuple(x))(x) for x in temp['P_fPos']]

        l_temp = temp.iloc[:, :5].drop_duplicates(subset=['l_fPos'])
        P_temp = temp.iloc[:, 5:].drop_duplicates(subset=['P_fPos'])
        
        #print(P_temp.head())
        
        # LumpedHydrophobe Features are changed to Hydrophobe and it is assumed they are the same feature.
        pd.set_option('mode.chained_assignment', None)
        l_temp['l_family'][l_temp['l_family'] == 'LumpedHydrophobe'] = "Hydrophobe"
        P_temp['P_family'][P_temp['P_family'] == 'LumpedHydrophobe'] = "Hydrophobe"
        
        # Find combinations from the Unique Protein Ligand Features.
        l = set(l_temp.index)
        P = set(P_temp.index)
        
        if verbose:
            print("Ligand Set:\t", l)
            print("Protein Set:\t", P)
        
        # Initialize the matrix for PL complex
        PL = setMatrixLigityScore1D  (fam_comb_num, cubeMaxDist, r)

        #featureCube = np.zeros ((cubeMaxDist*len(fam_comb_Dict), cubeMaxDist, cubeMaxDist))
        #print ("Size of Feature Cube per PL complex: \t %s" % str(featureCube.shape))
        if verbose:
            print ("\nSize of Feature Cube per PL complex: \t %s" % str(PL.featureMatrix.shape))
            print ("Bin Size: ", PL.binSize)
            
                       
        for i in range(len(PL_comb_list)): # iterate for different sets. [1, 1] only for 1D
            
            # Find combinations with 1 Ligand and 1 Protein PIPs
        
            l_r_num = PL_comb_list[i][0]  # First Element is number Ligand Features 
            P_r_num = PL_comb_list[i][1]  # Second Element is number Protein Features

            # find total number of 2-PIP sets. Change l_r_num and P_r_num to different combinations across sets.
            indicesPL = getIndicesPL(l, P, l_r_num, P_r_num, numPIPs)
            
            if verbose:
                print ("\nSample of \'indicesPL\':", indicesPL[0:10])
            

            # Get values for 'family' and 'distance' for each PIP bases on distances.
            # For each 2-PIP combination find distances            
            for index in indicesPL: # iterate over each 2-PIP index
            
                if l_r_num == 1 and P_r_num == 1:
                    # Get PIP information from dataframes - Family Feature Label and Feature Position
                    f1 = l_temp.loc[index[0],'l_family']
                    f2 = P_temp.loc[index[1],'P_family'] 
                    p1 = l_temp.loc[index[0],'l_fPos']
                    p2 = P_temp.loc[index[1],'P_fPos']
                else:
                    raise ValueError("Value for \'l_r_num\' and \'P_r_num\' not correct. %d and %d provided. Expected [1, 1]" % (l_r_num, P_r_num ))

                # Find the Cube Index from dict based on 3 PIP triangles.
                PIPfamPair = mysort(list((f1, f2)))
                
                try:
                    matrixIndex = fam_comb_Dict[tuple(PIPfamPair)]
                except KeyError as err:
                    print("ERROR: 2PIP Combination Not Found!\nCombination may not be sorted by 1st and 2nd columns.\nKey Provided: {0} \n\n".format(err))

                # Get Binning coordinates
                #
                # example: Acceptor-Donor
                # p1               p2
                # |------d1(x)---->|
                # A -------------- D 

                distance = eucDist(p1, p2)
                if verbose:
                    print ("\nPIP Pair Distance: %8.3f" % distance)          


                 # Round Distances for binning. This will be used as coordinates. Discretise the distance
                discreteDistance = getBinRound (distance, r)
                if verbose:
                    print ("PIP Pair Distance Rounded: %8.3f" % discreteDistance)

                #Find binning Index using discretised distance
                if r == 1:
                    binIndex = discreteDistance
                elif r == 0.5:
                    binIndex = discreteDistance*2
                        
                # Get Coordinates for Feature Matrix
                x, y = matrixIndex, binIndex

                # should be step above.
                # Filter by max distance of 20
                if y > PL.binSize:
                    print ("Max distance reached. Ignore value")
                    combIgnored += 1
                    continue # Skip Combination (Do not add to hypercube since out of range)

                if verbose:
                    print ("PIP Family Sequence:", PIPfamPair)
                    print ("Matrix Index for Family Sequence:", matrixIndex)
                    print ("Matrix Coordinates ->  x:%.1f  y:%.1f \n" % (x, y))

                # Bin PIP Pair sample in the correct matrix bin. Increment the bin count
                PL.featureMatrix[int(x), int(y)] += 1

                   
                #print ("****Binning Count: %d" % PL.featureMatrix[x, y, z])                       
        
        if verbose:
            print ("Feature Cube:\n", PL.featureMatrix)

        # Add the LigityScore Feature to the features dataframe. The row index return an np.array. np.asscalar is used to convert to int.
        df_Lig_Features_index = df_Lig_Features['features'][df_Lig_Features['pdbCode'] == plcomplex].index.values
        print ("index:", df_Lig_Features_index)
        #print (df_Lig_Features.head(4))
        #print ("\n\n")
        #print (type(df_Lig_Features_index))
        #print (df_Lig_Features_index)
        print ("\ndf_Lig_Features index: %10d" % df_Lig_Features_index.item())
        df_Lig_Features.loc[ df_Lig_Features_index.item(), "dataAvail"] = True
        #print ("+++++++++", df_Lig_Features.loc[ df_Lig_Features_index.item(), "features"] )
        #print (type(PL.featureMatrix))
        #print (type([PL.featureMatrix]))
        #df_Lig_Features.loc[ df_Lig_Features_index.item(), "features"]  = PL.featureMatrix
        df_Lig_Features.loc[ df_Lig_Features_index.item(), "features"]  = [PL.featureMatrix]
        #print (type(df_Lig_Features.loc[ df_Lig_Features_index.item(), "features"]))
        #print ((df_Lig_Features.loc[ df_Lig_Features_index.item(), "features"]))
        
        
        if combIgnored != 0:
            print ("\nPL Complex \'%s\' had %d points outside the coordinate space.\n" % (plcomplex, combIgnored))
            combIgnored = 0

        c += 1
        #if verbose:
        print ("\nIteration: %10d/%d" % (c, totalCodes))
        print ("Memory Usage: %12.2f Mb\n" % (df_Lig_Features['features'].memory_usage(index = True, deep=True)/(1024*1024) ) )

        #watch -n 1 free -m
        if c == args.iterations:
            break

    
    return df_Lig_Features



####################################################
####################################################
# Main Program
# --------------------------------------------------

# Variable Definition 
# --------------------------------------------------
verbose = args.verbose

cubeMaxDist = args.maxDist                # 3-PIP max distances. Distances greater than cubeMaxDist will be discarded.
r = args.resolution                       # Resolutions for binning the distances. Option are of 0.5 and 1.0

allowed_family = ["Hydrophobe", "Acceptor", "Donor", "PosIonizable", "NegIonizable", "Aromatic"] #"LumpedHydrophobe"

if args.LigDimension == 1:
    numPIPs = 2                     # Number of PIPs to use for each group. 2 implies linear structure.
    PL_comb_list = [[1, 1]]         # With two features only option is one from Ligand and one from Protein
elif args.LigDimension == 3:
    numPIPs = 3                     # Number of PIPs to use for each group. 3 implies triangular structure.
    PL_comb_list = [[1,2],[2,1]]    # Each element in list is a list that represent the number of 
                                    # Ligand and Protein features to use to create the 3-PIP triangles.

pd.set_option('display.max_columns', 20)

# Load DataFrames
# --------------------------------------------------

directory = args.input_dir_df
filename  = args.df_in_dataset_filname

# Load DataFrame for PDBBind dataset details
df_Lig_Features = loadDataFrame (directory, filename, c="zip")

df_Lig_Features['dataAvail'] = None
df_Lig_Features['features'] = None

df_Lig_Features['year'].astype('category')
df_Lig_Features['pdbSplit'].astype('category')
df_Lig_Features['split'].astype('category')
df_Lig_Features['dataAvail'].astype('category')
df_Lig_Features['realAffinity'].astype('float')


if verbose:
    print (df_Lig_Features.head(5))
    print (df_Lig_Features.shape)


# Load DataFrame for PDBfeature Pairs - Hotpoints
filename  = args.df_in_hotpoint_filename

PDBfeaturePairs = loadDataFrame (directory, filename, c="zip")

if verbose:
    print (PDBfeaturePairs.head(5))
    print ("\nNumber of Uniques PDBCodes in PDBfeaturePairs: %8d" % len(PDBfeaturePairs['l_PDBCode'].unique()))
    print ("Number of entries in df_Lig_Feature: %17d" %  len(df_Lig_Features))
    print ("Number of Uniques PDBCodes in df_Lig_Feature: %8d\n\n" % len(df_Lig_Features['pdbCode'].unique()))

# Remove any PDBCodes from df_Lig_Features that are not in PDBfeaturePairs (hotpoints)
codes = PDBfeaturePairs['P_PDBCode'].unique()
df_Lig_Features = df_Lig_Features[df_Lig_Features['pdbCode'].isin(codes)]

"""
if verbose:
    ids = df_Lig_Features["pdbCode"]
    print ("\nDuplicate PDB Codes in \'df_Lig_Features\'.")
    print(df_Lig_Features[ids.isin(ids[ids.duplicated()])].sort_values("pdbCode"))

# Remove duplicates
df_Lig_Features.drop_duplicates(subset='pdbCode', keep="last", inplace=True)
if verbose:
    print(df_Lig_Features.groupby('pdbSplit').count())
    print ("\nNumber of Uniques PDBCodes in PDBfeaturePairs: %8d" % len(PDBfeaturePairs['l_PDBCode'].unique()))
    print ("Number of entries in df_Lig_Features: %17d" % len(df_Lig_Features))
    print ("Number of Uniques PDBCodes in df_Lig_Features: %8d\n\n" % len(df_Lig_Features['pdbCode'].unique()))
""" 
    

# Get LigityScore1D or LigityScore 3D HyperCube Features
# --------------------------------------------------


if args.LigDimension == 1:          # LigityScore 1D Features
    df_Lig_Features = GenerateLigFeatures1D(PDBfeaturePairs, PL_comb_list, allowed_family, cubeMaxDist, r, verbose)
    df_Lig_Features = df_Lig_Features.reset_index()

elif args.LigDimension == 3:        # LigityScore 3D Features
    df_Lig_Features = GenerateLigFeatures3D(PDBfeaturePairs, PL_comb_list, allowed_family, cubeMaxDist, r, verbose)
    df_Lig_Features = df_Lig_Features.reset_index()


print ("\n\nMemory Usage in Bytes:\n", (df_Lig_Features.memory_usage(index=True, deep=True)) )

print ("\n\nSummary of Features:\n", df_Lig_Features.groupby('dataAvail').count())

gc.collect()



# Save DataFrames
# --------------------------------------------------
saveDataFrame (df_Lig_Features, args.output_dir, args.df_out_features_filename, c="zip")
#saveDataFrameFeather (df_Lig_Features, args.output_dir, args.df_out_features_filename)


#python3 pdb-LigFeatures.py --df_in_hotpoint_filename PDB2016Hotpoints-50samples
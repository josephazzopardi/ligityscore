"""
# Course: MSc AI
# Date: 02/02/2020
# Author: Joseph Azzopardi
# Credits:
# File: pdb-training-ligityScore1D.py
# Version: 1.0
# Description: Train CNN Model based on the LigityScore featues using pair distances to create a feature Matrix.
"""

from torch.utils.tensorboard import SummaryWriter
writer = SummaryWriter()

import pandas as pd
import numpy  as np
import re
import os
import errno
import pybel
import tfbio.data
import warnings
import argparse
import time

from math import ceil
from sklearn.linear_model import LinearRegression
#from IPython.display import Image

import torch
from torch import nn
import torch.nn.functional as F
import torch.optim as optim

from torch.utils.data import Dataset, DataLoader

from torchsummary import summary

# Import from Personal Module
from sbvscnnmsc.sbvsCNN  import myNet
from sbvscnnmsc.sbvsCNN  import init_weights
from sbvscnnmsc.sbvsData import datasets
from sbvscnnmsc.sbvsData import PDBdatasetLigity1D
from sbvscnnmsc.sbvsHelper import loadDataFrame
from sbvscnnmsc.sbvsHelper import cnnResults


#https://pytorch.org/docs/stable/notes/randomness.html
np.random.seed(123)
torch.manual_seed(123)

# Ignore warnings
warnings.filterwarnings("ignore")

# python3 pdb-training-Score.py --train_set "Train" --epoch 2 --new_shape 80 80 70

# Script Arguments
# -------------------------------
parser = argparse.ArgumentParser(description='SBVS Affinity Prediction based on CNN.',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

io_group = parser.add_argument_group('Input & Output Files-Directories')
io_group.add_argument('--input_dir', '-i', default='./LigityScore_Output/Ligity1D',
                       help='Directory with training, validation and test sets')
io_group.add_argument('--log_dir', '-l', default='./tensorboard-runs/Ligity1D',
                       help='Main directory to store tensorboard summaries')
io_group.add_argument('--log_testname', '-ltn', default='run-Score-1D-1', 
                       help='sub-directory to store tensorboard summaries')

train_group = parser.add_argument_group('Neural Network General Training Parameters')
train_group.add_argument('--train_set', '-ts', default='Train',
                          help='Parameter from datasets Class to use for CNN Training. Default is Train Set (datasets.Train) ')
train_group.add_argument('--val_set', '-vs', default='Validation',
                          help='Parameter from datasets Class to use for CNN Validation. Default is Validation Set (datasets.Validation) ')
#train_group.add_argument('--new_shape', '-ns', nargs='+', type=int, default=(160,140,20),
#                          help='Parameter to reshape dataset feature in this shape from (1120, 20, 20) ')
train_group.add_argument('--epoch', '-e', type=int, default=30,
                          help='Training epochs to iterate over train-ser.')
train_group.add_argument('--epochSave', '-eS', type=int, default=10,
                          help='Epoch Frequency to save model to disk.')
train_group.add_argument('--checkpoints', '-c', type=int, default=10,
                          help='Number of pytorch models to save to disk.')
train_group.add_argument('--epochValidate', '-eV', type=int, default=20,
                          help='Epoch Frequency to Validate model and output on Tensorboard.')
train_group.add_argument('--batchSize', '-b', type=int, default=20,
                          help='Batch Size to use for Training.')
train_group.add_argument('--rotations', '-r', type=int, default=1,
                          help='Rotations used to rotate feature Matrix. Default is one which indicates no rotation. Allowed values 4 and 8.')
train_group.add_argument('--learningRate', '-lr', type=float, default=0.00001,
                          help='Learning Rate used for back propagation (Optimizer).')
train_group.add_argument('--regl2', '-Rl2', type=float, default=0.001,
                          help='Optimiser L2 Regularisation parameter for weight decay.')
train_group.add_argument('--dataWorkers', '-dw', type=int, default=4,
                          help='Number of worker thread to use in dataloader')
train_group.add_argument('--weightInit', '-wI', type=int, default=1,
                          help='Enable custom Weight Initialisation as pwer script. Allowed values 0 and 1.')
train_group.add_argument('--earlyStop', '-eSt', type=bool, default=False,
                          help='The number of epochs of consistent worst validation results before stopping training. ')
train_group.add_argument('--earlyStopFactor', '-eSF', type=float, default=0.05,
                          help='The number of epochs of consistent worst validation results before stopping training. ')
train_group.add_argument('--min_epochs', '-mE', type=int, default=30,
                          help='The number of epochs before Early Stopping comes into play. Therefore this at worst is the minimum number of epoch used for training. ')
                    
conv_group = parser.add_argument_group('Convolutions Training Parameters')                                                
conv_group.add_argument('--convModule', '-cM', type=int, default=2,
                         help='Parameter to define if convolution should use nn.Conv2d or nn.Conv3d. Allowed values are "2" and "3". ')
conv_group.add_argument('--convKSize', '-cKS', type=int, default=5,
                         help='Filter Size to use in convolution.')
conv_group.add_argument('--convStride', '-cS', type=int, default=1,
                         help='Stride value to use in convolution.')
conv_group.add_argument('--convPad', '-cP', type=int, default=2,
                         help='Padding value to use in convolution.')
conv_group.add_argument('--convDim','-cD', nargs='+', type=int, default=[64, 128, 256], 
                         help='List of sizes of Convolutional Filters.')
conv_group.add_argument('--normalizationFn','-nF', default='batch', 
                         help='Normalization function to apply after convolution. Allowed values \'batch\' or \'instance\'.')
conv_group.add_argument('--normalizationList','-nL', nargs='+', type=int, default=[0, 0, 0, 0], 
                         help='List of sizes of Convolutional Filters.')
conv_group.add_argument('--convDropout','-cDrP', nargs='+', type=float, default=[0.0, 0.0, 0.0, 0.0], 
                         help='Dropout Percentage to use in Convolution Layers. Probability of an element to be zeroed.')
conv_group.add_argument('--convDropoutList','-cDr', nargs='+', type=int, default=[0, 0, 0, 0], 
                         help='List of "1" or "0" to represent if dropout2d-3d should be enabled or disabled respectively at each of the Convolution Layers. Must of of size equal to the number of layers.')
conv_group.add_argument('--in_channels', '-iCh', type=int, default=1,
                         help='C_in for Convolution Layers.')


maxpool_group = parser.add_argument_group('Max Pooling Layers Training Parameters') 
maxpool_group.add_argument('--mpoolWindow', '-mpW', type=int, default=2,
                            help='MaxPool Windows to use in maxpooling.')
maxpool_group.add_argument('--mpoolStride', '-mpS', type=int, default=2,
                            help='MaxPool Stride to use in maxpooling. Default is 2 and mpoolWindow of 2 and mpoolStride reduces dimenstion to half.')
maxpool_group.add_argument('--mpoolPad', '-mpP', type=int, default=0,
                            help='MaxPool Padding to use in maxpooling.')
maxpool_group.add_argument('--mpool','-mpL', nargs='+', type=int, default=[0, 1, 1, 1], 
                            help='List of "1" or "0" to represent if max pooling function should be enabled or disabled respectively after each of the Convolution Layers. Must of of size equal to the number of Conv layers. \nDefault all layers include MaxPooling function.')

fc_group = parser.add_argument_group('Convolution Training Parameters')
fc_group.add_argument('--activation', '-a', default="relu",
                       help='Activation Function to use for Fully Connected Layers. Default Activation Function is "relu".')
fc_group.add_argument('--activationList','-aL', nargs='+', type=int, default=[1, 1, 1, 1], 
                       help='List of "1" or "0" to represent if activation function should be enabled or disabled respectively at each of the Fully Connected Layers. Must of of size equal to the number of layers. \nDefault all layers include activation function.')
fc_group.add_argument('--FCdim','-fcD', nargs='+', type=int, default=[1000, 500, 200, 1], 
                       help='List of sizes of fully connected layers.')
fc_group.add_argument('--dropout', '-d', nargs='+', type=float, default=[0.5, 0.5, 0.5, 0],
                       help='Dropout Percentage to use in Fully Connected Layers. Probability of an element to be zeroed.')
fc_group.add_argument('--dropoutList','-dL', nargs='+', type=int, default=[ 1, 1, 1, 0], 
                       help='List of "1" or "0" to represent if dropout should be enabled or disabled respectively at each of the Fully Connected Layers. Must of of size equal to the number of layers.')


args = parser.parse_args()

# Script Arguments Validation
# -------------------------------

#Size of ActivateList, Dropout List, and FC dim should be the same
if not (len(args.activationList) == len(args.FCdim) and len(args.FCdim) == len(args.dropoutList)):
    raise ValueError('Incorrect numbers of elements in either activationList, FCdim, or dropout list.  Values must be equal. Values Provided:\n \
\t\t-> ActivationList: %s \n\t\t-> FCdim: %s \n\t\t-> dropoutList %s\n\n' % (len(args.activationList), len(args.FCdim), len(args.dropoutList) ) )

# dropout between 0 and 1.
for j in range (len(args.dropout), 0):
    if not (args.dropout[j] >= 0 and args.dropout[j] <= 1.0):
        raise ValueError('Dropout Rate must be between 0 and 1.0. 0 implies not dropout. Value prodived "%.3f".\n\n' % args.dropout )

# activation from available list.
if not (args.activation == 'relu' or args.activation == 'lrelu' ):
    raise ValueError('Allowed Values are "relu" or "lrelu". Value prodived "%s".\n\n' % args.activation )

# Normalisation Function from available list.
if not (args.normalizationFn == 'batch' or args.normalizationFn == 'instance' ):
    raise ValueError('Allowed Values are "batch" or "instance". Value prodived "%s".\n\n' % args.normalizationFn )

# Size of conv layers and max pool activations
if (len(args.mpool) != len(args.convDim)+1 ):
    raise ValueError('\n%d Pooling Layers and %d Convolution Layers provided. \nConvolution layers dimension (including input) \
must be of same size as pooling layers. Start with 0, example [0, 1, 1, 1] \
to mark input with no pooling and pooling on other 3 conv layers [64, 128, 256].\n\n' % (len(args.mpool), len(args.convDim)))

# Size of conv layers and normalization activations
if (len(args.normalizationList) != len(args.convDim)+1 ):
    raise ValueError('\n%d Normalization activation Layers and %d Convolution Layers provided. \nConvolution layers dimension (including input) \
must be of same size as normalization activation layers. Start with 0, example [0, 1, 1, 1] \
to mark input with no normalization and normalization on other 3 conv layers [64, 128, 256].\n\n' % (len(args.mpool), len(args.convDim)))

# selection of pytorch conv module
if not (args.convModule == 2 or args.convModule == 3 ):
    raise ValueError('\nconvModule must be either "2" to use nn.Conv2d or "3" to use nn.Conv3d. Value "%d" provided.\n\n' % args.convModule)

# check if directories exist.
if not os.path.isdir(args.input_dir):
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.input_dir)

if not os.path.isdir(args.log_dir):
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.log_dir)

# Classes or functions

class evalMetrics:
    # Class to store the prediction results for a particular model

    def __init__(self):
        self.rmse = 0   # Root Mean Square Error
        self.mae = 0
        self.R = 0
        self.sd = 0

def pdbPredict (net, inputSet="Validation"):

    """
    Function to iterate through datalader dataset to predict binding affinity using CNN model at the particular epoch.
    During Training only the Training and Validation set are predicted.  Best results are taken based on the best RMSE for the validation set.

    parameters:
        inputSet (String) : string value to define dataset to use for prediction. Default is 'Validation'
        net (Pytorch nn.Module): CNN model
    returns:
        p_metrics  (evalMetrics  Class): loss class containing values for RMSE, MAE, R
    
    """

    inputSetAllowed = ["Train", "Validation", "Test"]

    if (inputSet not in inputSetAllowed): #or inputSet != "Test" :
        raise ValueError('Incorrect Value for \'inputSet\'. Provided %s but expected either \'Train\', \'Validation\', \'Test\'. \n' % inputSet)
    
    try: 
        eval("dataset.%s" % inputSet) 
    except NameError: 
        print("'dataset' is not defined!\n")

    p_metrics = evalMetrics()   # parameters initialised to 0

    data = PDBdatasetLigity1D (eval("dataset.%s" % inputSet ), angle = 0, R=R) # Rotation set to 0 for validation
    dataloaderPredict = DataLoader(data, batch_size=batchSize, shuffle=True, num_workers=args.dataWorkers) #Load Data using pytorch dataloader
    exec ("dataset.%s['predAffinity'] = None" % inputSet) # add empty predAffinity column
    print ("\nPredicting %s dataset with %d batches .....\n" % (inputSet, len(dataloaderPredict )))

    exec ("dataset.%s['predAffinity'] = None" % inputSet) # add empty predAffinity column
    
    with torch.no_grad():

        net = net.eval() # switch model to Evaluation Mode

        for i, batch in enumerate(dataloaderPredict, 0):

            # get the inputs; data is a list of [inputs, labels, pdbCode]
            batch[1] = [(lambda x: float(x))(x) for x in batch[1]]
            batch[1] = torch.tensor(batch[1])
            batch[1] = batch[1].view(-1, 1)  # convert targets to match output of CNN

            # Prepare Inputs to CNN
            p_inputs, p_real_aff = batch[0].to(device), batch[1].to(device)

            # Get pdbCodes
            p_pdbid = batch[2]  # PDBCodes used to update dataset

            # Predict by passing FF through network
            p_pred_aff = net(p_inputs)

            # Compute Loss - Defines as MSE, add .sqrt fnc to convert to RMSE
            p_loss_RMSE = torch.sqrt(criterionMSE(p_pred_aff, p_real_aff))
            p_loss_MAE = criterionMAE(p_pred_aff, p_real_aff)
            
            # Compute Running Loss to Return
            p_metrics.rmse += p_loss_RMSE.item()
            p_metrics.mae  += p_loss_MAE.item()

            # Update Test Dataset with Predicted Affinity for Batch
            for pdbid, pred in zip(p_pdbid, p_pred_aff):
                #print (pdbid, pred.item())
                exec("dataset.%s['predAffinity'][dataset.%s['pdbCode'] == pdbid] = pred.item()" % (inputSet, inputSet) )

        # Compute Pearson Correlation 
        exec("dataset.%s['realAffinity'] = [(lambda x: float(x))(x) for x in dataset.%s['realAffinity']]" % (inputSet, inputSet))
        exec("dataset.%s['predAffinity'] = [(lambda x: float(x))(x) for x in dataset.%s['predAffinity']]" % (inputSet, inputSet))

        exec("affs%s = dataset.%s[['realAffinity','predAffinity']]" % (inputSet, inputSet))
        exec("pearsoncorr%s = affs%s.corr(method='pearson')" % (inputSet, inputSet))
        
        print (eval("pearsoncorr%s" % inputSet))

        p_metrics.R = eval( "pearsoncorr%s.iloc[0,1]" % inputSet )

        # Compute SD - Standard Deviation in Regression
        y =  eval("dataset.%s['realAffinity']" % (inputSet) )
        lr = LinearRegression()

        # Fit regression line into y=mx+c
        exec("lr.fit( dataset.%s['predAffinity'].to_numpy().reshape(-1, 1), dataset.%s['realAffinity'].to_numpy())" % (inputSet, inputSet) )
        y_ = eval("lr.predict( dataset.%s['predAffinity'].to_numpy().reshape(-1, 1) )" % (inputSet) )
        
        N = eval("len(dataset.%s['realAffinity'])" % (inputSet) )

        p_metrics.sd = (np.sum((y - y_)**2) / (N - 1)) ** 0.5         
        

        return p_metrics 


def save_checkpoint (model_dir, filename, max_models, net):
    
    """
    Function to Save Pytorch model to 'model_dir'. Most Recent 'max_models' are kept in dir.
    
    Arguments:
        model_dir (string): Directory of where model is saved.
        filename (string):  Filename of model to be saved.
        max_models (int):   Number of model checkpoints to save and keep to disk. Oldest files will be deleted to save better models.
        net (nn): Pytorch NN model
    
    Returns: None
    """
    
    # Check if 'models' directory is created
    if not (os.path.exists(model_dir)):
        print ("\nDirectory to save models does not exist.")
        print ("Creating 'models' directory in %s\n" % tensorboard_path)
    
        try:
            os.mkdir(model_dir)
        except OSError:
            print ("Creation of the directory %s failed" % model_dir)
        else:
            print ("Successfully created the directory %s \n" % model_dir)
        
    
    # Get all files in dir, and add full paths
    list_of_files = os.listdir(model_dir)
    full_path = [model_dir+("/{0}".format(x)) for x in list_of_files]
    
    # Delete last file
    if len(list_of_files) >= max_models:
        oldest_file = min(full_path, key=os.path.getctime)
        print ("Oldest file to remove is: %s.\n" % oldest_file)

        try:
            os.remove(oldest_file)
        except OSError:
            print ("Deletion of file %s failed" % oldest_file)
        else:
            print ("Successfully deleted file %s \n" % oldest_file)
    
    # Get all files in dir, and add full paths
    list_of_files = os.listdir(model_dir)
    full_path = [model_dir+("/{0}".format(x)) for x in list_of_files]
    
    # Save Model to disk
    if len(list_of_files) <= max_models:
        try:
            modelSavePath = os.path.join (model_dir, filename)
            torch.save(net.state_dict(), modelSavePath)
        except OSError:
            print ("\nSave of Model failed at %s.\n" % modelSavePath)
        else:
            print ("\nSuccessfully Saved Model at %s \n" % modelSavePath)
    


# GLOBAL variables Definition
# -------------------------------
"""
mean_w_init = 0.0
std_conv = 0.001
b_conv = 0.1
b_fc = 1.0
"""

# Variables Definition - Network Parameters
# -------------------------------

#Convolution
#newShape = tuple(args.new_shape)
in_channels = args.in_channels          # Cin (Z dimention )
#in_channels = newShape[2]              # Cin (Z dimention )
convModuleDim = args.convModule         # var to define if Conv2D or Conv3D is used.
conv_dim = args.convDim
conv_layers = len(conv_dim)
convKernelSize = args.convKSize
convStride = args.convStride
convPad = args.convPad
normalizationFn = args.normalizationFn
normalizationList = args.normalizationList
convDropout = args.convDropout
convDropoutList = args.convDropoutList

#Max Pooling
pooling = args.mpool
maxPoolWindow = args.mpoolWindow
maxPoolStride = args.mpoolStride
maxPoolPad = args.mpoolPad

#Fully Connected 
fc_dim = args.FCdim
fc_layers = len(fc_dim)

# Training Parameters
batchSize = args.batchSize
activation = args.activationList
activation_fn = args.activation
epochs = args.epoch
learningRate = args.learningRate
reg_l2 = args.regl2
dropout = args.dropoutList
dropoutProb = args.dropout
num_rotations = args.rotations


# Variables Definition 
# -------------------------------
splitSets   = {"tr":"Train", "v":"Validation", "t":"Test"}


best_val_R = -1
best_val_error = 1000
best_epoch_error = None
best_epoch_R = None
max_models = args.checkpoints

bestResults_R  = cnnResults()
bestResults_Er = cnnResults()

s_f = args.earlyStopFactor    # factor used to compute number of epoch to assess for early stopping
s = ceil(s_f * epochs)
s_count = s

angle_rotations = {}

if num_rotations == 4:
    for i, angle in enumerate(list(range(0,359,90)), 0):
        # key:value - The key is the family combination. Value is the index.
        angle_rotations.update( { i : angle} )
    R = True
elif num_rotations == 8:
    for i, angle in enumerate(list(range(0,359,45)), 0):
        # key:value - The key is the family combination. Value is the index.
        angle_rotations.update( { i : angle} )
    R = True
else:
    angle_rotations.update( { 0 : 0} )
    R = False


print("\n\nSBVS Affinity Prediction based of LigityScore Features.")
print("---------------------------------------------------\n\n")


# GPU definition
# -------------------------------
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print("\n\nDevice for training is:", device)
print(torch.__config__.show())

# Print Definitions
print("\n\n*********** CNN Network Definitions.\n" % ())
#print("Reshape input features: %15s" % str(newShape))
print("Input Channels: %23d" % (in_channels))
print("CNN Module: \t\t Conv%dd" % (convModuleDim))
print("Convolution Layers: %18d" % (conv_layers))
print("Convolution Layers Input Dimensions: %s" % (str(conv_dim)))
print("Convolution Kernel Size: %13d" % (convKernelSize))
print("Convolution Stride: %18d" % (convStride))
print("Convolution Padding: %17d" % (convPad))
print("Normalization Function: %15s" % (normalizationFn))
print("Normalization Layer Activation: %17s" % (str(normalizationList)))
print("Convolution Dropout Percentage: %15s" % str(convDropout))
print("Convolution Dropout Layer Activation: %17s\n" % (str(convDropoutList)))

print("Max Pool Layers Enabled (first is input layer so needs to be 0): %s" % (str(pooling)))
print("Max Pool Kernel Size: %10d" % (maxPoolWindow))
print("Max Pool Stride: %15d" % (maxPoolStride))
print("Max Pool Padding: %9d\n" % (maxPoolPad))

print("FC Layers Input Dimensions (%d): %20s\n" % (fc_layers, str(fc_dim)))

print("CNN Training Parameters.\n" % ())
print ("Batch Size: %17d" % (batchSize))
print ("Activation Function: %10s" % (activation_fn))
print ("Activation Layers: %20s" % activation)
print ("Epochs: %21d" % (epochs))
print ("Learning Rate: %19.5f" % (learningRate))
print ("L2 Regularization Decay: %8.4f" % (reg_l2))
print ("Dropout Layers: %23s" % dropout)
print ("Dropout Rate: %17s" % str(dropoutProb))
print ("Training Rotations: %9d" % (num_rotations))
print ("Angles for Rotations: %9s" % (str(angle_rotations.items())))
print ("Weight Initialisation: %6d\n" % (args.weightInit))

print ("Number of Model Checkpoints: %6d\n" % (max_models))
print ("Early Stopping: %16d epochs" % s)

# load datasets
dataset = datasets()
print ("\n\n*********** Load Datasets ...\n")
for i, Set in enumerate(splitSets):
    file = splitSets[Set] + "-pkl"
    setattr(dataset, splitSets[Set], loadDataFrame(args.input_dir, file) ) # load dataframe. #use setattr from class.

print ("\n\n*********** Datasets Summary:\n")
print ("Train Dataset Size: %d \nTrain Batches: %d \n" % (len(dataset.Train), ceil(len(dataset.Train)/batchSize)))
print ("Test Dataset Size: %d  \nTest Batches: %d \n" % (len(dataset.Test), ceil(len(dataset.Test)/batchSize)))
print ("Validation Dataset Size: %d  \nValidation Batches: %d \n" % (len(dataset.Validation), ceil(len(dataset.Validation)/batchSize)))

#print(dataset.Train['features'].iloc[0].dtype)
if dataset.Train['features'].iloc[0].dtype == 'float64':
    #print (dataset.Test['features'].iloc[0])
    dataset.Train['features'] = dataset.Train['features'].apply(lambda x: x.astype('float32'))
    dataset.Test['features'] = dataset.Test['features'].apply(lambda x: x.astype('float32'))
    dataset.Validation['features'] = dataset.Validation['features'].apply(lambda x: x.astype('float32'))



# Load Data using pytorch dataloader
cnnInput = PDBdatasetLigity1D (eval("dataset.%s" % args.train_set), angle = 0, R=R)
dataloader = DataLoader(cnnInput, batch_size=batchSize, shuffle=True, num_workers=args.dataWorkers) #set num_workers to 4 https://pytorch.org/docs/stable/data.html


# Get size of Gridresults
d = PDBdatasetLigity1D (dataset.Validation, angle = 0, R=R)
sample = d[0]
gridSize = list(sample[0].shape[0:]) 
print ("Grid Size is: %s \n" % gridSize)
#sample = sample[0,:,:,:,:]
#print (sample.shape)
print ("\n*********** Dataset Samples:\n")


# Get Example from dataloader by Looping through DataLoader
print("iter | Feature Dimension\t\t | Target Dimension  | PDBCode ")
print("---------------------------------------------------------------------- ")
for i_batch, sample_batched in enumerate(dataloader):
    # sample_batched is a list that includes the features (sample_batched[0]) and the labels (sample_batched[1])
    sample_batched[1] = [(lambda x: float(x))(x) for x in sample_batched[1]]
    sample_batched[1] = torch.tensor(sample_batched[1])
    print("%4d %34s %19d %10d" % (i_batch, str(sample_batched[0].size()), len(sample_batched[1]), len(sample_batched[2])))
    
    if i_batch == 1:
        h = sample_batched[0]
        print ("\nPDB Real Affinity from DataLoader (targets):\n %s" % str(sample_batched[1]))
        print ("\nPDB Codes from DataLoader:\n %s" % str(sample_batched[2]))
        break


#Initialize Network
# -------------------------------

net = myNet(in_channels, gridSize, convModuleDim, conv_layers, conv_dim, convKernelSize, convStride, convPad, 
            activation, activation_fn, pooling, maxPoolWindow, maxPoolStride, maxPoolPad, 
            fc_layers, fc_dim, dropout, dropoutProb, normalizationFn, normalizationList, convDropoutList, convDropout )

print ("\n\n", net)


# Initialise Tensorboard
print ("\n\n*********** Initialise Tensorboard:\n")
tensorboard_path = os.path.join(args.log_dir , args.log_testname)
print("Tensorboard log path: %s" % tensorboard_path)
writer = SummaryWriter(tensorboard_path)
writer.add_graph(net, sample_batched[0])

# Print Network summary
print ("\n\n*********** Network Summary:\n")
print(summary(net, sample_batched[0]))


print ("\n\n*********** CNN Network Definition:\n")
net.to(device)
print(net)
print ("\n")


# Initialize weights
# -------------------------------
if args.weightInit == 1:
    print ("*********** Initialize CNN Network Weights and Parameters Values:\n")
    net.apply(init_weights)     # global variables set in sbvsCNN.py


# Define Loss Function and Back-Prog method
# -------------------------------

# Define loss function to use. 
# In this case 'Mean Square Error' is used for training however 'MAE' error is also computed for tracking.
criterionMSE = nn.MSELoss()   #tracked after every batch
criterionMAE = nn.L1Loss()    #tracked after each epoch
print ("\n\nLoss Function Used:\n %s\n" % criterionMSE)


# Define the weight update method after computing gradients.
optimizer = optim.Adam(net.parameters(), lr=learningRate, weight_decay=reg_l2, amsgrad=False)
print ("Optimizer Parameters Used for Back Propagation:\n %s\n\n" % optimizer)
# torch.optim.Adam(params, lr=0.001, betas=(0.9, 0.999), eps=1e-08, weight_decay=0, amsgrad=False)
# https://pytorch.org/docs/stable/optim.html



# Start Training
# -------------------------------

start_time = time.time()
print ("\n\n*********** Training Started on:  %s.\n" % str(time.ctime(start_time)))

validationSet = args.val_set
trainSet = args.train_set

for epoch in range(0, epochs+1, 1):  # loop over the dataset multiple times

    net = net.train() # switch model to Train Mode
    
    epoch_loss_RMSE = 0     # loss computed after every epoch
    epoch_loss_MAE  = 0     

    print ("\n\n>>>>>>  Epoch: %d Start\n" % (epoch))

    for rotation in range(num_rotations):  # loop over all the rotations
        #x = PDBdatasetLigity1D (dataset.Validation, angle = 0)  
        print ("\n\nRotation Number %d." % rotation)
        
        cnnInput = PDBdatasetLigity1D (eval("dataset.%s" % args.train_set ), angle=angle_rotations[rotation], R=R) 
        dataloaderTrain = DataLoader(cnnInput, batch_size=batchSize, shuffle=True, num_workers=args.dataWorkers)

        running_loss = 0.0    # loss accumulator for 'batchesPrint' batches
        batchesPrint = 100    # Print loss after so many batches
        c = 0                 # counter used to print loss

        for i, batch in enumerate(dataloaderTrain, 1):   # enumerate starts from 1
            
            #print ("Batch %d of size %s" % (i, len(batch[0]) ))

            # get the inputs; data is a list of [inputs, labels, pdbCode]
            batch[1] = [(lambda x: float(x))(x) for x in batch[1]]
            batch[1] = torch.tensor(batch[1])
            batch[1] = batch[1].view(-1, 1)  # convert targets to match output of CNN
            #print (batch[1].shape)

            # inputs, labels = data
            inputs, labels = batch[0].to(device=device), batch[1].to(device=device)

            # zero the parameter gradients
            optimizer.zero_grad()

            # Forward Pass + backward + optimize
            outputs = net(inputs)
            #print (i, outputs)
            #print ("\n")

            # Compute Loss - Defines as MSE, add .sqrt fnc to convert to RMSE
            loss = torch.sqrt(criterionMSE(outputs, labels))
            loss_MAE = criterionMAE(outputs, labels)

            # Backpropagation pass
            loss.backward()

            # Update weights using ADAM
            optimizer.step()

            # print statistics (denominator for stats represents "total number of batches accumulated".)
            running_loss += loss.item()             #variables accumulated every 'batchesPrint'
            epoch_loss_RMSE += loss.item()
            epoch_loss_MAE  += loss_MAE.item()

            if i % batchesPrint == 0:    # print every 100 mini-batches
                writer.add_scalar('training-batch-loss', running_loss / batchesPrint, (epoch*len(dataloaderTrain)*num_rotations + rotation*len(dataloaderTrain) + i)) # ...log the running loss
                print('[E:%d, R:%d, %5d] loss: %.3f' % (epoch, rotation, i, running_loss / batchesPrint))
                running_loss = 0.0 #initialize running_loss
                c += 1

        if i % batchesPrint != 0:
            b = i - (c*batchesPrint)
            writer.add_scalar('training-batch-loss', running_loss / b, (epoch*len(dataloaderTrain)*num_rotations + rotation*len(dataloaderTrain) + i)) # ...log the running loss
            print('[E:%d, R:%d, %5d] loss: %.3f' % (epoch, rotation, i, running_loss / b))

    
    # epoch iteration do
    print('\n[E:%d] Training Epoch RMSE loss : %.3f' % (epoch, epoch_loss_RMSE / (eval("len(dataset.%s)/batchSize" % args.train_set)*num_rotations)))
    print('[E:%d] Training Epoch MAE  loss : %.3f' % (epoch, epoch_loss_MAE  / (eval("len(dataset.%s)/batchSize" % args.train_set)*num_rotations)))

    if epoch % args.epochValidate == 0:

        # Validate Training
        trainPredict = pdbPredict (net, args.train_set)
        validationPredict = pdbPredict (net, args.val_set)

        # Save EPOCH validation/training loss to tensorboard
        #writer.add_scalar('RMSE/training-epoch-loss', epoch_loss_RMSE / len(dataloaderTrain)*num_rotations, ((epoch+1)*len(dataloaderTrain)*num_rotations) ) # ...log the running loss
        writer.add_scalars('epoch/RMSE', {'Training'  :(trainPredict.rmse / eval("len(dataset.%s)/batchSize" % args.train_set)),
                                          'Validation':(validationPredict.rmse / eval("len(dataset.%s)/batchSize" % args.val_set) ) }, (epoch) )
        
        writer.add_scalars('epoch/R-Score', {'Training'  :(trainPredict.R ),
                                             'Validation':(validationPredict.R) }, (epoch) )

        print('\n[E:%d] Predict Training Epoch RMSE loss: %10.3f' % (epoch, trainPredict.rmse / eval("len(dataset.%s)/batchSize" % args.train_set) )) #number of batches
        print('[E:%d] Predict Training Epoch MAE  loss: %10.3f'   % (epoch, trainPredict.mae  / eval("len(dataset.%s)/batchSize" % args.train_set) ))
        print('[E:%d] Predict Training Epoch SD       : %10.3f' % (epoch, trainPredict.sd))
        print('[E:%d] Predict Training Epoch R        : %10.3f\n' % (epoch, trainPredict.R))

        print('[E:%d] Predict Validation Epoch RMSE loss: %8.3f' % (epoch, validationPredict.rmse / eval("len(dataset.%s)/batchSize" % args.val_set) )) #number of batches
        print('[E:%d] Predict Validation Epoch MAE  loss: %8.3f' % (epoch, validationPredict.mae  / eval("len(dataset.%s)/batchSize" % args.val_set) ))
        print('[E:%d] Predict Training Epoch SD         : %8.3f' % (epoch, validationPredict.sd))
        print('[E:%d] Predict Validation Epoch R        : %8.3f' % (epoch, validationPredict.R ))

        
        
        # keep track of best epoch for R value for validation set, and save it to disk
        if validationPredict.R > best_val_R:
            
            best_val_R = validationPredict.R

            # Store Full Results in Class
            bestResults_R.e = epoch
            
            bestResults_R.RMSE_tr = trainPredict.rmse / (len(dataset.Train)/batchSize)
            bestResults_R.MAE_tr  = trainPredict.mae  / (len(dataset.Train)/batchSize)
            bestResults_R.STD_tr  = trainPredict.sd
            bestResults_R.R_tr    = trainPredict.R
            
            bestResults_R.RMSE_v = validationPredict.rmse / (len(dataset.Validation)/batchSize)
            bestResults_R.MAE_v  = validationPredict.mae  / (len(dataset.Validation)/batchSize)
            bestResults_R.STD_v  = validationPredict.sd
            bestResults_R.R_v    = validationPredict.R

            modelSaveFilename = "model-cnn-epochR"+str(epoch)
            model_dir = os.path.join(tensorboard_path, "models")

            save_checkpoint (model_dir, modelSaveFilename, max_models, net)


        #keep track of best epoch (RMSE) for validation set, and save it to disk
        if (validationPredict.rmse / (len(dataset.Validation)/batchSize)) < best_val_error:
            
            best_val_error = validationPredict.rmse / (len(dataset.Validation)/batchSize)

            s_count = s     # re-initialise value for s, since a better Validation Error was found.
            
            # Store Full Results in Class
            bestResults_Er.e = epoch

            bestResults_Er.RMSE_tr = trainPredict.rmse / (len(dataset.Train)/batchSize)
            bestResults_Er.MAE_tr  = trainPredict.mae  / (len(dataset.Train)/batchSize)
            bestResults_Er.STD_tr  = trainPredict.sd
            bestResults_Er.R_tr    = trainPredict.R

            bestResults_Er.RMSE_v = validationPredict.rmse / (len(dataset.Validation)/batchSize)
            bestResults_Er.MAE_v  = validationPredict.mae  / (len(dataset.Validation)/batchSize)
            bestResults_Er.STD_v  = validationPredict.sd
            bestResults_Er.R_v    = validationPredict.R
            
            # Always save model when we have a better Validation error
            modelSaveFilename = "model-cnn-epoch"+str(epoch)
            model_dir = os.path.join(tensorboard_path, "models")

            save_checkpoint (model_dir, modelSaveFilename, max_models, net)

        else:
            
            if args.earlyStop:

                if epoch > args.min_epochs: # skip first 30 iterations to decide when to start tracking Early Stopping.
                    # Decrement Early Stopping Counter
                    s_count -= 1

                    if s_count == 0:
                        # Stop Training
                        print("Early stopping!. Validation Results have not improved for the last %d epochs" % s )
                        break           # Exit Training Loop
                

writer.close()

end_time = time.time()
total_time = (end_time - start_time)/60

print("\n\n*********** Training Complete on  %s !\n\n" % str(time.ctime(end_time)))
print ("\n\nTotal training time %.2f minutes.\n" % total_time )

print ("Best Results obtained in Epoch %d with %5.3f epoch loss.\nBest Model file name is: %20s.\n\n" % (bestResults_Er.e, bestResults_Er.RMSE_v, "model-cnn-epoch"+str(bestResults_Er.e) ) )

print ("Best Validation RMSE: %6.2f" % bestResults_Er.RMSE_v)
print ("Best Validation MAE: %6.2f"  % bestResults_Er.MAE_v)
print ("Best Validation STD: %6.2f"  % bestResults_Er.STD_v)
print ("Best Validation R: %6.2f\n"    % bestResults_Er.R_v  )

print ("Best Training RMSE: %6.2f" % bestResults_Er.RMSE_tr)
print ("Best Training MAE: %6.2f"  % bestResults_Er.MAE_tr )
print ("Best Training STD: %6.2f"  % bestResults_Er.STD_tr )
print ("Best Training R: %6.2f\n\n"    % bestResults_Er.R_tr )

print ("Best Results obtained in Epoch %d with %5.3f Pearson Coefficient (R) for Validation Set.\nBest Model file name is: %20s.\n\n" % (bestResults_Er.e, bestResults_Er.R_v, "model-cnn-epoch"+str(bestResults_Er.e) ) )


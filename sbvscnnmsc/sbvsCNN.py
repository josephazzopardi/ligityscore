"""
# Course: MSc AI
# Date: 02/02/2020
# Author: Joseph Azzopardi
# Credits:
# File: sbvsCNN.py
# Version: 1.0
# Description: Pytorch Neural Network class definition. MyNet is main class to create a CNN Network.
"""

import numpy as np

from math import ceil

# Pytorch General Modules
import torch
import torch.nn.functional as F
import torch.optim as optim

from torch import nn

# Pytorch Datasets 
from torch.utils.data import Dataset, DataLoader

"""
torch.utils.data.Dataset is an abstract class representing a dataset.Your custom dataset should inherit Dataset 
and override the following methods:
    __len__ so that len(dataset) returns the size of the dataset.
    __getitem__ to support the indexing such that dataset[i] can be used to get ith sample
"""


###############################################
#    Neural Network Pytorch Model
###############################################

# Convolution Layers
def conv_module(in_c, out_c, convModule, conv_kSize, conv_s, conv_p, activation_fn, pooling, maxPoolWindow, maxPoolStride, maxPoolPad, norm_fn, normalization, conv_dropout, prob_C_drop):
    
    """
    Function to define one layer of the CNN Network for the CONVOLUTION network block:
    
    arguments:
        in_c (int): Conv Layer Input channels 
        out_c (int): Conv Layer Output channels 
        convModule (int): Conv Module Dimensions. option is 2 for Conv2D Convolution Layers, or 3 for Conv3D Convolution Layers.
        activation_fn (string): activation function to use after convolution. Default RELU.
        conv_kSize (int): Convolution filter dimension
        conv_s (int): Convolution Stride
        conv_p (int): Convolution Padding
        pooling (int): Boolean variable to define if pooling layers should be added after conv layer.
        maxPoolWindow (int): Pooling 'filter' size
        maxPoolStride (int): Pooling Stride
        maxPoolPad (int): Pooling Padding
        norm_fn (String): Algorithm to use for normalisation
        normalization (int): Boolean variable to define if Normalization fucntion should be added after conv layer.
        conv_dropout  (int): Boolean variable to define if dropout should to the FC layer.
        prob_C_drop (float): nn.Dropout2d or nn.Dropout2d probability of an element to be zeroed.

    returns:
        Pytorch Sequential object defining ONE CNN layer.
    """
    
    ## Layer Sequence Definitions:
    #Conv Layer      -- 1) Convolution                 2) Non-Linear Activation  3) Pooling
    #With Batch Norm -- 1) Convolution  2) Batch Norm  3) Non-Linear Activation  4) Pooling
    #With Dropout    -- 1) Convolution  2) Batch Norm  3) Non-Linear Activation  4) Pooling  5) Dropout

    # Define Pytorch Module Dictionaries
    activations = nn.ModuleDict([
                ['lrelu', nn.LeakyReLU()],
                ['relu', nn.ReLU()]
    ])
    
    normalization2D = nn.ModuleDict([
                ['batch', nn.BatchNorm2d(out_c, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) ],
                ['instance', nn.InstanceNorm2d(out_c, eps=1e-05, momentum=0.1, affine=False, track_running_stats=False) ]
    ])

    normalization3D = nn.ModuleDict([
                ['batch',  nn.BatchNorm3d(out_c, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) ],
                ['instance', nn.InstanceNorm3d(out_c, eps=1e-05, momentum=0.1, affine=False, track_running_stats=False) ]
    ])


    if convModule == 2:    # use Conv2d and MaxPool2d for CNN Layers
        
        # Select if Pooling and Batch Normalisation are included
        if pooling == 0 and normalization == 0: 
            
            l = [] #define layer
            l.extend([nn.Conv2d(in_c, out_c, kernel_size=conv_kSize, stride=conv_s, padding=conv_p),  # add modules to layers
                      activations[activation_fn]]
            )    
            if conv_dropout == 1:
                l.extend([nn.Dropout2d(p=prob_C_drop, inplace=False)])
       
            return nn.Sequential(*l) # put modules in sequential

        elif pooling == 0 and normalization == 1: 
            
            l = [] 
            l.extend([nn.Conv2d(in_c, out_c, kernel_size=conv_kSize, stride=conv_s, padding=conv_p),
                      normalization2D[norm_fn],
                      activations[activation_fn]]
            )
            if conv_dropout == 1:
                l.extend([nn.Dropout2d(p=prob_C_drop, inplace=False)])

            return nn.Sequential(*l) 

        elif pooling == 1 and normalization == 0: 
            
            l = []
            l.extend([nn.Conv2d(in_c, out_c, kernel_size=conv_kSize, stride=conv_s, padding=conv_p),
                      activations[activation_fn],
                      nn.MaxPool2d(kernel_size=maxPoolWindow, stride=maxPoolStride, padding=maxPoolPad, dilation=1, return_indices=False, ceil_mode=True)]
            )
            if conv_dropout == 1:
                l.extend([nn.Dropout2d(p=prob_C_drop, inplace=False)])

            return nn.Sequential(*l)

        elif pooling == 1 and normalization == 1: 
            
            l = []
            l.extend([nn.Conv2d(in_c, out_c, kernel_size=conv_kSize, stride=conv_s, padding=conv_p),
                      normalization2D[norm_fn],
                      activations[activation_fn],
                      nn.MaxPool2d(kernel_size=maxPoolWindow, stride=maxPoolStride, padding=maxPoolPad, dilation=1, return_indices=False, ceil_mode=True)]
            )
            if conv_dropout == 1:
                l.extend([nn.Dropout2d(p=prob_C_drop, inplace=False)])

            return nn.Sequential(*l)
         
            
    elif convModule == 3: # use Conv3d and MaxPool3d for CNN Layers

        # Select if Pooling and Batch Normalisation are included
        if pooling == 0 and normalization == 0: 
            
            l = []
            l.extend([nn.Conv3d(in_c, out_c, kernel_size=conv_kSize, stride=conv_s, padding=conv_p),
                      activations[activation_fn]]
            )
            if conv_dropout == 1:
                l.extend([nn.Dropout3d(p=prob_C_drop, inplace=False)])
            
            return nn.Sequential(*l)
            
        elif pooling == 0 and normalization == 1: 
            
            l = []
            l.extend([nn.Conv3d(in_c, out_c, kernel_size=conv_kSize, stride=conv_s, padding=conv_p),
                      normalization3D[norm_fn],
                      activations[activation_fn]]
            )
            if conv_dropout == 1:
                l.extend([nn.Dropout3d(p=prob_C_drop, inplace=False)])
            
            return nn.Sequential(*l)

            
        elif pooling == 1 and normalization == 0: 
            
            l = []
            l.extend([nn.Conv3d(in_c, out_c, kernel_size=conv_kSize, stride=conv_s, padding=conv_p),
                      activations[activation_fn],
                      nn.MaxPool3d(kernel_size=maxPoolWindow, stride=maxPoolStride, padding=maxPoolPad, dilation=1, return_indices=False, ceil_mode=True)]
            )
            if conv_dropout == 1:
                l.extend([nn.Dropout3d(p=prob_C_drop, inplace=False)])
            
            return nn.Sequential(*l)

        elif pooling == 1 and normalization == 1: 
            
            l = []
            l.extend([nn.Conv3d(in_c, out_c, kernel_size=conv_kSize, stride=conv_s, padding=conv_p),
                      normalization3D[norm_fn],
                      activations[activation_fn],
                      nn.MaxPool3d(kernel_size=maxPoolWindow, stride=maxPoolStride, padding=maxPoolPad, dilation=1, return_indices=False, ceil_mode=True)]
            )
            if conv_dropout == 1:
                l.extend([nn.Dropout3d(p=prob_C_drop, inplace=False)])
            
            return nn.Sequential(*l)


class conv_Modules(nn.Module):
    
    def __init__(self, convModule, conv_dim, conv_kSize, conv_s, conv_p, activation_fn, pooling, maxPoolWindow, maxPoolStride, maxPoolPad, norm_fn, normalization, conv_dropout, prob_C_drop):

        """
        Class based on nn.Module to define CNN convolution block.
        
        arguments: 
            convModule (int): Conv Module Dimensions. option is 2 for Conv2D Convolution Layers, or 3 for Conv3D Convolution Layers.
            conv_dim(list): Conv Layer List of number of channels [ 64, 128, 256] 
            conv_kSize (int): Convolution filter dimension
            conv_s (int): Convolution Stride
            conv_p (int): Convolution Padding
            pooling (list): List of Boolean variable [0, 1, 1, 1] to define if pooling layers should be added after conv layer (including input).
            maxPoolWindow (int): Pooling 'filter' size
            maxPoolStride (int): Pooling Stride
            maxPoolPad (int): Pooling Padding
            norm_fn (String): Algorithm to use for normalisation
            normalization (List): List of Boolean variable [0, 1, 1, 1] to define if Normalization layers should be added after conv layer (including input).
            conv_dropout  (List): List of Boolean variable [0, 1, 1, 1] to define if dropout should be added at end of layer (including input).
            prob_C_drop (List): List of Dropout probability of an element to be zeroed. Should be size of 'conv_dropout' list.
        
        returns:
            Pytorch Sequential object defining multiple CNN layers.
        """
            
        super(conv_Modules, self).__init__()
      
        self.conv_layers = nn.Sequential(*[conv_module(in_c, out_c, convModule, conv_kSize, conv_s, conv_p, activation_fn, p, maxPoolWindow, maxPoolStride, maxPoolPad, norm_fn, n, conv_d, conv_d_prob) 
                                           for in_c, out_c, p, n, conv_d, conv_d_prob in zip(conv_dim, conv_dim[1:], pooling[1:], normalization[1:], conv_dropout[1:], prob_C_drop[1:])])
                   
            
    def forward(self, x):
    
        return self.conv_layers(x)


# Fully Connected Layers
def fc_module(in_fc, out_fc, dropout, p_drop, activation, activation_fn):
    
    """
    Function to define one layer of the CNN Network for the FULLY CONNECTED network block:
    
    arguments:
        in_fc      (int): Number of input neurons in a FC layer.
        out_fc     (int): Number of output neurons in a FC layer.
        dropout    (bool):  Boolean variable to define if dropout should to the FC layer.
        p_drop     (float): Dropout probability of an element to be zeroed.
        activation (bool):  Boolean variable to define if layer has an activation function.
        activation_fn (sting): Activation function to use from ModuleDict

    returns:
        PyTorch Sequential object defining ONE CNN layer.
    """
    
    activations = nn.ModuleDict([
                ['lrelu', nn.LeakyReLU()],
                ['relu', nn.ReLU()]
    ])
       
    if dropout == 1 and activation == 1:
        return nn.Sequential(
                #self.fc1 = nn.Linear(256*3*3*3, 1000) 
                #x = F.relu(self.fc1(x))
                nn.Linear(in_fc, out_fc), 
                activations[activation_fn],
                nn.Dropout(p=p_drop)            
            )
    elif dropout == 1 and activation == 0: # dropout only
        print ("++Warning: No Activation Selection++")
        return nn.Sequential(
                nn.Linear(in_fc, out_fc), 
                nn.Dropout(p=p_drop)        
            )
    elif dropout == 0 and activation == 1: # no dropout
        return nn.Sequential(
                nn.Linear(in_fc, out_fc), 
                activations[activation_fn]          
            )
    elif dropout == 0 and activation == 0: # no dropout no activation
                return nn.Sequential(
                nn.Linear(in_fc, out_fc)       
            )


class fc_Modules(nn.Module):
    
    def __init__(self, fc_dim, dropout, p_drop, activation, activation_fn):

        """
        Class based on nn.Module to define CNN Fully Connected block.
        
        arguments:
            fc_dim  (List): List of fully connected dimensions, including input dimension [6912, 1000, 500, 200, 1]
            dropout (List): List of Boolean variable [1, 1, 1, 0] to define if dropout should to the FC layer.
            p_drop  (List): Dropout probability of an element to be zeroed. List of floats correcsponding to each FC layer.
            activation (List): List of Boolean variables [1, 1, 1, 0] to define if activation should be active at a FC layer.
            activation_fn (string): activation fucntion name to use in ModuleDict 
            
        returns:
            Pytorch Sequential object defining multiple fully connected layers.
        """
        
        super(fc_Modules, self).__init__()
               
        self.fc_layers = nn.Sequential(*[fc_module(in_fc, out_fc, d, p_d, a, activation_fn) 
                                          for in_fc, out_fc, d, p_d, a in zip(fc_dim, fc_dim[1:], dropout, p_drop, activation)] )
        
         
    def forward(self, x):
    
        return self.fc_layers(x)


            
# Combine 'Convolution' and 'Fully Connected' layers to build network.
class myNet(nn.Module):

    """
    Class based on nn.Module to define CNN.
        
    arguments:
        c_in (int): Conv Layer Input channels 
        grid_size (list): List of input dimentions TO FC layers. For a 3D object (x, y, cin), (x_bar, y_bar) is returned where x_bar and y_bar are the size of the dimesions after conv and pooling layers.
        convModule (int): Conv Module Dimensions. option is 2 for Conv2D Convolution Layers, or 3 for Conv3D Convolution Layers.
        convModule (int): Conv Module Dimensions. option is 2 for Conv2D Convolution Layers, or 3 for Conv3D Convolution Layers.
        conv_dim(list): Conv Layer List of number of channels [64, 128, 256] 
        conv_kSize (int): Convolution filter dimension
        conv_s (int): Convolution Stride
        conv_p (int): Convolution Padding
        activation (book List): List of Boolean variables [1, 1, 1, 0] to define if activation should be active at a FC layer.
        activation_fn (string): activation fucntion name to use in ModuleDict 
        pooling (list): List of Boolean variable [0, 1, 1, 1] to define if pooling layers should be added after conv layer (including input).
        maxPoolWindow (int): Pooling 'filter' size
        maxPoolStride (int): Pooling Stride
        maxPoolPad (int): Pooling Padding
        fc_layers (int): Number of Fully Connected layers.
        fc_dim (List):  List of fully connected dimensions, including input dimension [6912, 1000, 500, 200, 1]
        dropout (List): List of Boolean variable [1, 1, 1, 0] to define if dropout should to the FC layer.
        p_drop (List): List of Dropout probability of an element to be zeroed. Should be size of 'dropout' list.
        conv_dropout  (List): List of Boolean variable [0, 1, 1, 1] to define if dropout should be added at end of layer (including input).
        prob_C_drop (List): List of Dropout probability of an element to be zeroed. Should be size of 'conv_dropout' list.
        
    returns:
        Pytorch Sequential object defining multiple fully connected layers.
    """


    def __init__(self, c_in, grid_size, convModule = 3, conv_layers=3, conv_dim=[64, 128, 256], conv_kSize=5, conv_s=1, conv_p=1, activation=[1, 1, 1, 1],
        activation_fn='relu', pooling = [0, 1, 1 ,1], maxPoolWindow=2, maxPoolStride=2, maxPoolPad=0, fc_layers=4, fc_dim=[1000, 500, 200, 1], 
        dropout=[1, 1, 1, 0], p_drop=[0.5, 0.5, 0.5, 0], norm_fn='batch', normalization=[0, 0, 0, 0], conv_dropout=[0,0,0,0], prob_C_drop=[0.0, 0.0, 0.0, 0.0]):     

                
        super(myNet, self).__init__()
        
        # define variables
        self.c_in = c_in
        self.grid_size = grid_size

        self.convModule = convModule       
        self.conv_layers = conv_layers
        self.conv_dim = conv_dim
        self.conv_kSize = conv_kSize
        self.conv_s = conv_s
        self.conv_p = conv_p
        
        self.activation = activation
        self.activation_fn = activation_fn
        self.pooling = pooling
        self.maxPoolWindow = maxPoolWindow
        self.maxPoolStride = maxPoolStride
        self.maxPoolPad = maxPoolPad
        
        self.fc_layers = fc_layers
        self.fc_dim = fc_dim
        self.dropout = dropout
        self.p_drop = p_drop
        self.norm_fn = norm_fn
        self.normalization = normalization
        self.conv_dropout = conv_dropout
        self.prob_C_drop = prob_C_drop

        # prepend number of channels of the input.
        self.conv_dim.insert(0, self.c_in)
        
        # check pooling size
        # validation check. pooling size must be same as number of conv layers.
        if (len(self.pooling) != len(self.conv_dim)):
            raise ValueError('%d Pooling Layers and %d Convolution Layers.\nConvolution layers dimenstion (inc input) \
                             must be of same size as pooling layers. Start with 0, example [0, 1, 1, 1] \
                             to mark input with no pooling and pooling on other \
                             and 3 conv layers.' % (len(self.pooling), len(self.conv_dim)))

        if not (self.convModule == 2 or self.convModule == 3):
            raise ValueError("convModule must be either of value '2' for Conv2D CNN layers, or '3' for Conv3D CNN Layers. Input %d was provided" % convModule)
            

        if not (len(self.dropout) == len(self.p_drop) and (len(self.dropout) == self.fc_layers)):
            raise ValueError("Dropout activation and dropout probability must be the same, and equal to number of FC layers. Dropout activation has length %d, whilst dropout probability has length %d." % (len(self.dropout), len(self.p_drop)))

        if not (len(self.conv_dropout) == len(self.prob_C_drop) and (len(self.conv_dropout) == (self.conv_layers + 1))):
            raise ValueError("Convolution Dropout activation and convolution dropout probability must be the same, and equal to number of Convolution layers and Input. Conv Layers are %d Conv Dropout activation has length %d, whilst Conv dropout probability has length %d." % (self.conv_layers, len(self.conv_dropout), len(self.prob_C_drop)))

        fc_in_grid = self.fc_input_features(verbose=1)
        fc_in_dim = np.int(self.conv_dim[-1]) * np.int(np.prod(fc_in_grid))
        self.fc_dim.insert(0, fc_in_dim)
            
        
        # Call Convolution Class to define and Initialise Convolution Layers
        self.conv_Layers = conv_Modules(self.convModule, self.conv_dim, self.conv_kSize, self.conv_s, self.conv_p, self.activation_fn,
                                        self.pooling, self.maxPoolWindow, self.maxPoolStride, self.maxPoolPad, self.norm_fn, self.normalization, self.conv_dropout, self.prob_C_drop)
        
        # Call Fully Connected Class to define and Initialise FF layers
        self.fc_Layers  = fc_Modules(self.fc_dim, self.dropout, self.p_drop, self.activation, self.activation_fn)
        


    def forward(self, x):
        """
        Forward Pass in Network
        """

        x = self.conv_Layers(x)
        
        x = x.view(-1, self.num_flat_features(x)) # flat
        
        x = self.fc_Layers(x)
        
        return x

  
    def num_flat_features(self, x):
        '''
        Function to flatten the number features for input to FC layers.
        '''
        size = x.shape[1:]  # consider all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features
    
    
    def check_grid_size(self):
        '''    
            Check if input grid has the same dimensions (W = L = H)
            If occurence count of first element in list is equal to length of list.
            Then it means all elements in List are equal.
            
            parameters:
                self
            returns:
                'False' if grid has different dimensions across axes.
                'True' if grid has same dimensions across axes.
            
        '''
        result = False
        
        if len(self.grid_size) > 0 :
            result = self.grid_size.count(self.grid_size[0]) == len(self.grid_size)
    
        return result
    
    
    """
    def fc_input_features(self):
        '''
        Function to to determine the size at the input of the FC layer in a CNN network.
        parameters:
            self: uses
        '''
        
        if (self.check_grid_size()):
            grid_size = self.grid_size[0]
        else:
            raise ValueError('Input Grid has different dimenstions of shape %s.\n' % self.grid_size.shape)
        
        layers = self.conv_layers
        
        f = self.conv_kSize
        p = self.conv_p
        s = self.conv_s       
        
        p_mpool = self.maxPoolPad
        s_mpool = self.maxPoolStride
        f_mpool = self.maxPoolWindow

        fc_in = grid_size
        
        for i in range(layers):
            #print (fc_in)
            fc_in = (fc_in + (2*p) - f)/s + 1

            if self.pooling[i+1] == 1:
                fc_in = ceil((fc_in + (2*p_mpool) - f_mpool)/s_mpool + 1)

        #print ("\nGrid Size: %d" % fc_in)     

        return int(fc_in)
    """

    def fc_input_features(self, verbose=0):

        """
        Function to get input dimension of Fully Connected Layer after passing through Conv and Max Pool Layers in a CNN network.
        parameters:
            self:
            verbose (int): Print output 
        returns: output_grid_size (list): output size for last convolution dimension.

        """

        layers = self.conv_layers
        
        f = self.conv_kSize
        p = self.conv_p
        s = self.conv_s       
        
        p_mpool = self.maxPoolPad
        s_mpool = self.maxPoolStride
        f_mpool = self.maxPoolWindow

        input_dim = self.grid_size

        output_grid_size = []

        for dim in input_dim[1:]: # skip first element. First element is Cin (channels) in nn.Conv2d and nn.Conv3d
            if verbose:
                print ("\nOutput Dimensions by Layer:")
                print ("Start: %12d" % dim)
            
            for i in range(1, layers+1):
                #print (i)
                
                if i == 1:
                    output_dim = (dim + (2*p) - f)/s + 1
                    if verbose:
                        print ("Conv Layer%d: %6d" % (i, output_dim))

                    if self.pooling[i] == 1:
                        output_dim = ceil((output_dim + (2*p_mpool) - f_mpool)/s_mpool + 1)
                        if verbose:
                            print ("Pool Layer%d: %6d" % (i, output_dim))

                else:
                    output_dim = (output_dim + (2*p) - f)/s + 1
                    if verbose:
                        print ("Conv Layer%d: %6d" % (i, output_dim))

                    if self.pooling[i] == 1:
                        output_dim = ceil((output_dim + (2*p_mpool) - f_mpool)/s_mpool + 1)
                        if verbose:
                            print ("Pool Layer%d: %6d" % (i, output_dim))
                            
            if verbose:
                print ("End: %14d\n\n" % output_dim)
            
            if output_dim < 1:
                raise ValueError('Input Dimension is less than 1 for Convolution Layers and Parameters provided. \nOriginal size: %6d \nNew Size: %6d' % (dim, output_dim))
    

            output_grid_size.append(output_dim)
            if verbose:
                print ("Convolution Output Size: ", output_grid_size)

        return output_grid_size



###############################################
#    Function to initialise Weights
###############################################

# GLOBAL variables Definition
# -------------------------------

mean_w_init = 0.0
std_conv = 0.001
b_conv = 0.1
b_fc = 1.0

def init_weights(m):

    """
    Function to be used by net.apply function where net is the NN definition to initialize weights.
    Parameters:
        m: pytorch model
        b_conv: Global value for bias for conv layers
        b_fc: Global value for bias for fc layers
    Returns:
        none
    """
    
    #torch.nn.init.ones_(tensor)
    #torch.nn.init.constant_(tensor, val)
    #torch.nn.init.normal_(tensor, mean=0.0, std=1.0)
    #torch.nn.init.xavier_uniform(m.weight)
    
    #mean_w_init = 0.0
    
    if type(m) == nn.Conv3d or type(m) == nn.Conv2d:
        # In nn.Linear m.weight.shape[0] is the size of the output channels (ex 64, 256, 512)
        # print (m.weight.shape[0])

        try:
            torch.nn.init.normal_(m.weight, mean_w_init, std_conv)
        except NameError:
            print ("\nConv std Global Value not defined. Setting to 0.001 ..... ")
            std_conv = 0.001
            torch.nn.init.normal_(m.weight, mean_w_init, std_conv)
        
        print("Initialized Weights for layer %s with mean=%.3f, std=%.3f." % (m, mean_w_init, std_conv))

        try:
            m.bias.data.fill_(b_conv)
        except NameError:
            print ("Conv Bias Global Value not defined. Setting to 0.1 ..... ")
            b_conv = 0.1
            m.bias.data.fill_(b_conv)

        print("Initialized Bias for layer %s with value %.3f." % (m, b_conv))
    
    if type(m) == nn.Linear:
        # In nn.Linear m.weight.shape[1] is the size of the input.
        # print (m.weight.shape[1])
        std = 1/m.weight.shape[1]**0.5 
        torch.nn.init.normal_(m.weight, mean_w_init, std)
        print("\nInitialize weights for layer %s with mean=%.3f, std=%.3f." % (m, mean_w_init, std))
        try:
            m.bias.data.fill_(b_fc)
        except NameError:
            b_fc = 1.0
            print ("FC Bias Global Value not defined. Setting to 1.0 ..... ")
            m.bias.data.fill_(b_fc)





"""
# Course: MSc AI
# Date: 02/02/2020
# Author: Joseph Azzopardi
# Credits:
# File: sbvsHelper.py
# Version: 1.0
# Description: Helper functions to be used for Structure Based Virtual Screening (SBVS) for Binding Affinity Prediction (LigityScore).
"""

import pandas as pd
import numpy  as np
import re
import os
import errno
import cv2

from math import ceil
from PIL import Image



def list_files(startpath):
    """
    Function to print files and folders starting from startpath.
    """

    for root, dirs, files in os.walk(startpath):
        level = root.replace(startpath, '').count(os.sep)
        indent = ' ' * 4 * (level)
        print('{}{}/'.format(indent, os.path.basename(root)))
        subindent = ' ' * 4 * (level + 1)
        for f in files:
            print('{}{}'.format(subindent, f))


def saveDataFrame (df, directory, filename, c="zip"):
    """
    Function to save a dataframe disk as a compressed pickle file
    Parameters:
        df: dataframe object to save
        directory: path to save to local disk
        filename:  filename
        c: compression method
    Return: None
    """
    path = os.path.join(directory, (filename+"."+c))
    
    try:
        df.to_pickle(path, compression=c, protocol=4)
        print ("\nDataFrame saved at %s.\n" % path)
    except Exception as error:
        print ("\n%s\n" % error)
        print ("Save of Features DataFrame FAILED at %s.\n" % path)
        
    return None
    #df_final.to_hdf('./CNN_AS_output/data.h5', key='df', mode='w')


def saveDataFrameFeather (df, directory, filename, suffix="feather"):
    """
    Function to save a dataframe disk as a 'Feather' file
    Parameters:
        df: dataframe object to save
        directory: path to save to local disk
        filename:  filename
        suffix: filename suffix
    Return: None
    """
    path = os.path.join(directory, (filename+"."+suffix))
    
    try:
        df.to_feather(path)
        print ("\nDataFrame saved at %s.\n" % path)
    except Exception as error:
        print ("\n%s\n" % error)
        print ("Save of Features DataFrame FAILED at %s.\n" % path)
        
    return None


def loadDataFrame (directory, filename, c="zip"):
    """
    Function to load a dataframe disk as a compressed pickle file
    Parameters:
        directory: path to load from local disk
        filename:  filename
        c: compression method
    Return:
        df: dataframe object read from disk
    """
    path = os.path.join(directory, (filename+"."+c))
    
    try:
        df = pd.read_pickle(path, compression=c)
        print ("\nDataFrame loaded from %s.\n" % path)
        
        return df
        
    except Exception as error:
        print ("\n%s\n" % error)
        print ("Loading of Features DataFrame FAILED at %s.\n" % path)



def pad_with(vector, pad_width, iaxis, kwargs):
     pad_value = kwargs.get('padder', 10)
     vector[:pad_width[0]] = pad_value
     vector[-pad_width[1]:] = pad_value


def myrotate(m, angle, verbose=0):
    """
    Function to rotate and equal sided matrix by 45 degree intervals.
    
    Parameters:
        m (np.array): Feature matrix of size (N,N). Only square shapes are supported
        angle (int) : Angle in degrees to rotate the matrix. Allowed valures
        verbose (int): Print verbose information. Default is 0
    Return:
        m_r (np.array): Rotated m matrix by angle degrees.
    """
    
    angles360 = []
    angles360.extend(range(0,361,45))
    
    angles_allowed = []
    angles_allowed.extend(range(0,361,45))
    angles_allowed.extend(range(0,-361,-45))
    
    if angle not in angles_allowed:
        raise ValueError("Angle Value not Allowed. %f given but expected from %s." % (angle, str(angles_allowed)))
    
    
    # grab the dimensions of the input featuers.
    (h, w) = m.shape[:2]
    
    if h != w:
        raise ValueError("Features Width and Height must be the same. \n\tWidth: %d \n\tHeight: %d" % (w, h))
    
    if verbose:
        print ("\nOriginal Width and Height: %d  %d\n" % (w, h))
    
    #determine the center of features
    #(cX, cY) = (ceil(w / 2), ceil(h / 2))
    (cX, cY) = (w // 2, h // 2)
    if verbose:
        print ("Centre for image - Original: %d  %d.\n" % (cX, cY))
        print (m)
    
    # Find the rotation matrix for the angle that contains most values outside the original NxN frame.
    # For a rectangle the max angle is 45 degrees.
    angle_max = 45
    M_max = cv2.getRotationMatrix2D((cX, cY), -angle_max, 1.0)
    
    # Find padding required so that all rotated matrix fits in frame
    cos = np.abs(M_max[0, 0])
    sin = np.abs(M_max[0, 1])
    maxW = int((h * sin) + (w * cos))
    maxH = int((h * cos) + (w * sin))
    
    pad = maxW - w
    
    # Find new matrix Width and Height
    nW = maxW + pad
    nH = maxH + pad
    
    if verbose:
        print ("\nNew Width and Height: %d  %d\n" % (nW, nH))
      
    # Add Padding to image
    m = np.pad(m, pad, pad_with, padder=0)
    
    #(cX, cY) = (ceil(nW / 2), ceil(nH / 2))
    (cX, cY) = (nW // 2, nH // 2)
    if verbose:
        print ("Centre for image - Padded: %d  %d.\n" % (cX, cY))
        
    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])
      
    if verbose:
        print ("Padded feature matrix:")
        print (m, "\n\n")
      
    # perform the actual rotation and return the image - CV2 method
    #m_r = np.around((cv2.warpAffine(m, M, (nW, nH))), 1)
    #m_r = np.rint((cv2.warpAffine(m, M, (nW, nH))))
    #m_r = ((cv2.warpAffine(m, M, (nW, nH), borderMode=cv2.BORDER_TRANSPARENT )))
    
    # perform the actual rotation and return the image - PILLOW method
    m = Image.fromarray(m)
    m_r = m.rotate(angle, expand=0) #rotate image
    m_r = np.array(m_r)
        
    if verbose:
        print ("Rotated feature matrix:")
        print (m_r, "\n\n")
    
    return m_r


def myrotate3D(x, index):
    
    """
    Function to Rotate 3D Matrix by 90 degrees. Only certain positions are allowed as the Matrix has 
    two sides equal, but the third is of different size. Example 98x98x54.
    
    Rotations along the X axis: 0, 90, 180, 270 (0 is the starting point)
    Rotations along the Y axis: 180
    Rotations along the Z axis: 270
    
    Arguments:
        x (np.array): 3D input matrix
        index (int) : Integer to specify rotation
                            0 --> X, 0   degrees
                            1 --> X, 90  degrees
                            2 --> X, 180 degrees
                            3 --> X, 270 degrees
                            4 --> Y, 180 degress
                            5 --> Z, 270 degrees
    Return:
        x_r: rotated matrix based on index
    
    """
    
    x_rot = [0, 1, 2, 3]   # rotations along the X axis: 0, 90, 180, 270 degrees
    y_rot = [2]            # rotations along the Y axis: 180 degrees
    z_rot = [2]            # rotations along the Z axis: 180 degrees
       
    # Create List of possible 90 degree rotations
    rot_index = x_rot + y_rot + z_rot
    
    # Create List of axis labels for possible 90 degree rotations
    rot_axis = list("x"*len(x_rot)) + list("y"*len(y_rot)) + list("z"*len(z_rot))
    
    # Define Dictionary for plane
    rot_dict = {"x":(0,1), "y":(0,2), "z":(1,2)}
    
    # Rotate by 90 degrees based on index provided
    #x_r = np.rot90(x, i, (0,1))
    #x_r = np.rot90(x, i, (0,2))
    #x_r = np.rot90(x, i, (1,2))
    x_r = np.rot90(x, rot_index[index], rot_dict[rot_axis[index]]).copy()  # ndarray.copy() will allocate new memory for numpy array which makes it normal by removal of negative stride.
    #print (x_r)
        
    return x_r
    


class evalMetrics:
    # class used to store epoch results during training
    def __init__(self):
        self.rmse = 0   # Root Mean Square Error
        self.mae = 0
        self.R = 0
        self.sd = 0


# results class for predictions script
class predictResults:
    # class used to store prediction results for each test set.

    def __init__(self):
        
        self.RMSE_tr = None
        self.MAE_tr  = None
        self.STD_tr  = None
        self.R_tr  = None

        self.RMSE_v = None
        self.MAE_v  = None
        self.STD_v  = None
        self.R_v  = None

        self.RMSE_t = None
        self.MAE_t  = None
        self.STD_t  = None
        self.R_t  = None

        self.RMSE_t3 = None
        self.MAE_t3  = None
        self.STD_t3  = None
        self.R_t3  = None
        

# results class for training and validation
class cnnResults:
    # class used to store results from one experiment (during training), to return class to master script to store results of experiments.

    def __init__(self):
        self.e  = None
        self.time = None
        
        self.RMSE_tr = None
        self.MAE_tr  = None
        self.STD_tr  = None
        self.R_tr  = None

        self.RMSE_v = None
        self.MAE_v  = None
        self.STD_v  = None
        self.R_v  = None
        
# Parameters class to launch CNN training      
class cnnParams:
    # Class to store parameters to initialise sbvsCNN class. Used to define parameters for one experiment in batch training.

    def __init__(self):
       self.TestName = "Sample Test"
       self.Comments = "Sample Test"
       self.LigityVersion = 1
       self.input_dir = "./Ligity_Output/Ligity1D"
       self.log_dir = "./tensorboard-runs/Ligity1D"
       self.log_testname = "run-ligity-1D"+self.TestName
       self.train_set = "Train"
       self.val_set = "Validation"
       self.epoch = 140
       self.epochSave = 15
       self.checkpoints = 15
       self.epochValidate = 1
       self.earlyStop = False
       self.earlyStopFactor = 0.1
       self.batchSize = 20
       self.rotations = 1
       self.learningRate = 0.00001
       self.regl2 = 0.001
       self.dataWorkers = 4
       self.weightInit = 0
       self.convModule = 2
       self.convKSize = 5
       self.convStride = 1
       self.convPad = 2
       self.convDim = [64, 128, 256]
       self.normalizationFn = "instance"
       self.normalizationList = [0, 1, 1, 1]
       self.convDropout = [0, 0.1, 0.1, 0.1]
       self.convDropoutList = [0, 1, 1, 1]
       self.in_channels = 1
       self.mpoolWindow = 2
       self.mpoolStride = 2
       self.mpoolPad = 0
       self.mpool = [0, 1, 1, 1]
       self.activation = "relu"
       self.activationList = [1, 1, 1, 1]
       self.FCdim = [1000, 500, 200, 1]
       self.dropout = [0.5, 0.5, 0.5, 0]
       self.dropoutList = [1, 1, 1, 0]

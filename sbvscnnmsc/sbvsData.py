"""
# Course: MSc AI
# Date: 02/02/2020
# Author: Joseph Azzopardi
# Credits:
# File: sbvsDdata.py
# Version: 1.0
# Description: Helper functions to be used for Structure Based Virtual Screening (SBVS) for Binding Affinity Prediction.
"""

import pandas as pd
import numpy  as np

from tfbio.data import Featurizer
from tfbio.data import make_grid, rotate

from torch.utils.data import Dataset, DataLoader

from sbvscnnmsc.sbvsHelper import myrotate
from sbvscnnmsc.sbvsHelper import myrotate3D

###############################################
#    Pytorch Dataset Definition - Pafnucy
###############################################
class PDBdataset(Dataset):
    """PDB Bind Dataset"""

    def __init__(self, pdb_df, dataColumn="features", targetColumn="realAffinity", pdbIDColumn="pdbCode", grids=True, grid_resolution=1.0, max_dist = 10.0, R=0):
        """
        Arguments:
            pdb_df (DataFrame): Pandas DataFrame Object containing PDB Features.
            dataColumn (string): Pandas Series column index that containes a list element, that in turn includes matrix of features
                      to represent features for all atoms in P-L complex.
            targetColumn (string):
            pdbIDColumn (string):
        return:
            r (np.array): A feature sample from the dataset.
        """
        self.grids = grids
        self.grid_resolution = grid_resolution
        self.max_dist = max_dist
        self.data = pdb_df[dataColumn]      # data is a pandas series object
        self.targets = pdb_df[targetColumn]
        self.pdbID = pdb_df[pdbIDColumn]
        self.R = R

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        
        # initialize return value
        r = []
        
        # convert list in dataframe to numpy array
        sample = iter(self.data.iloc[idx])
        sample = next(sample)
        t = self.targets.iloc[idx]
        ID = self.pdbID.iloc[idx]
        
        if self.grids:
            # compute grid of sample
            sampleCoord = sample[:,0:3]             # the first 3 columns are coords
            sampleFeatures = sample[:,3:]           # rest are atom features (19 in total)
            
            # compute rotations
            sampleCoord = rotate(sampleCoord, self.R)
                       
            sample = make_grid(sampleCoord, sampleFeatures, self.grid_resolution, self.max_dist) # returns 5 dimensions
            sample = sample[0,:,:,:,:]        # drop batch dimension as this is added by dataloader class
                                              # Sample with Dimension (X, Y, Z, F), where F is the feature dimension
            
            # reorder dimension for Conv3D. Input size for Conv3D is (N,Cin,D,H,W)
            sample = np.swapaxes(sample, 2, 3)  # (X, Y, Z, F) -> Swap Z with F 
            sample = np.swapaxes(sample, 1, 2)  # (X, Y, F, Z) -> Swap Y with F 
            sample = np.swapaxes(sample, 0, 1)  # (X, F, Y, Z) -> Swap X with F
            
            r.append(sample)
            r.append (t)
            r.append (ID)
            
        return r


###############################################
#    Class to store dataset split Train-Validation-Test
###############################################

# Store DataSet types
class datasets:
    # Class to load dataset classes

    def __init__(self):
        self.Train = None
        self.Validation = None
        self.Test = None
        self.Test2013 = None
        
        

def minmax(x, verbose=0):

    """
    Function to standardize input using min_max function:
    Arguments:
        x (np.array) : Input 
    return:
        xx: input after min_max transformation.
    """

    x_max = np.max(x)
    x_min = np.min(x)
    
    if verbose:
        print ("Max Value: %f" % x_max)
        print ("Min Value: %f" % x_min)
        
    if x_max == x_min:
        if verbose:
            print ("Divide by 0")
        xx = x
        return xx

    xx = (x - x_min) / (x_max - x_min)
    
    return xx


def standardNorm(x, verbose=0):

    """
    Function to normalize input using standardNorm function:
    Arguments:
        x (np.array) : Input 
    return:
        xx: input after standardNorm transformation.
    """

    x_mean = np.mean(x.ravel())
    x_std = np.std(x.ravel())
    
    if verbose:
        print ("Mean Value: %f" % x_mean)
        print ("Sdt  Value: %f" % x_std)
        
    if x_std == 0:
        if verbose:
            print ("Divide by 0")
        xx = x
        return xx

    xx = (x - x_mean) / (x_std)
    
    return xx


###############################################
#    Pytorch Dataset Definition - Ligity 1D
###############################################


class PDBdatasetLigity1D(Dataset):
    """PDB Bind Dataset"""

    def __init__(self, pdb_df, dataColumn="features", targetColumn="realAffinity", pdbIDColumn="pdbCode", angle=0, R=False):
        # grids=True, grid_resolution=1.0, max_dist = 10.0, R=0):
        
        """
        Arguments:
            pdb_df  (DataFrame): Pandas DataFrame Object containing PDB Features.
            dataColumn (string): Pandas Series column index that containes a list element, that in turn includes matrix of features
                                 to represent features for all atoms in P-L complex.
            targetColumn (string): Pandas DataFrame Column storing the real affinity values for PL complexes.
            pdbIDColumn  (string): Pandas DataFrame Column storing the PDB Codes.
            angle (int) : Angle in degrees to rotate feature matrix.
            R (bool) : Boolean variable to determine if rotations are enabled.
        Returns:
            r (np.array): A feature sample from the dataset.
        """
  
        self.targets = pdb_df[targetColumn]
        self.data  = pdb_df[dataColumn]         # data is a pandas series object
        self.pdbID = pdb_df[pdbIDColumn]
        self.angle = angle
        self.R = R

    def __len__(self):
        return len(self.data)


    def __getitem__(self, idx):
        
        # initialize return value
        r = []
              
        sample = self.data.iloc[idx]      # numpy array
        t = self.targets.iloc[idx]
        ID = self.pdbID.iloc[idx]
        
        # Check for Any NaN values
        if np.isnan(sample).any():
            print ("NaN Detected")
            raise ValueError("Data input contains NaN values.")

        # Rotate Matrix
        if self.R: # Rotations are enabled. num_rotations == 4 or 8
            sample = myrotate(sample, self.angle, verbose=0)
               
        # Min-Max Scalar
        sample = minmax(sample, verbose=0)
  
        # added Cin dimension to sample
        sample = sample[np.newaxis, :, :]
        
        r.append(sample)
        r.append (t)
        r.append (ID)
            
        return r


###############################################
#    Pytorch Dataset Definition - Ligity 3D
###############################################


class PDBdatasetLigity(Dataset):
    """PDB Bind Dataset"""

    def __init__(self, pdb_df, newShape = (160, 140, 20), dataColumn="features", targetColumn="realAffinity", pdbIDColumn="pdbCode", convModule=2, R=False, rot_index = 0):
        # grids=True, grid_resolution=1.0, max_dist = 10.0, R=0):
        
        """
        Arguments:
            pdb_df  (DataFrame): Pandas DataFrame Object containing PDB Features.
            dataColumn (string): Pandas Series column index that containes a list element, that in turn includes matrix of features
                      to represent features for all atoms in P-L complex.
            targetColumn (string): Pandas DataFrame Column storing the real affinity values for PL complexes.
            pdbIDColumn  (string): Pandas DataFrame Column storing the PDB Codes.
            convModule (int): integer (2 or 3) to define in Conv2D or Conv3D is used.
            R (bool) : Boolean variable to determine if rotations are enabled.
            rot_index (int): Rotation transformation to use in myrotate3D() function. allowed values 1 to 6.
        Returns:
            r (np.array): A feature sample from the dataset.
        """
        #self.grids = grids
        #self.grid_resolution = grid_resolution
        #self.max_dist = max_dist   
        self.targets = pdb_df[targetColumn]
        self.data  = pdb_df[dataColumn]         # data is a pandas series object
        self.pdbID = pdb_df[pdbIDColumn]
        self.newShape = newShape
        self.convModule = convModule
        self.R = R
        self.rot_index = rot_index


    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
               
        r = []      # initialize return value
        
        sample = self.data.iloc[idx]     # numpy array
        t = self.targets.iloc[idx]
        ID = self.pdbID.iloc[idx]   

        # Check for Any NaN values
        if np.isnan(sample).any():
            print ("NaN Detected")
            raise ValueError("Data input contains NaN values.")

        # Min-Max Scalar
        sample = minmax(sample, verbose=0)

        # Reshape to make it image-like
        sample = np.reshape(sample, self.newShape, order='C')

        # Rotate Matrix
        if self.R: # Rotations are enabled. num_rotations == 6
            sample = myrotate3D(sample, self.rot_index)

        # reorder dimension for Conv3D. Input size for Conv3D is (N,Cin,D,H,W)
                                            # (X, Y, Cin) -> Start Position
        sample = np.swapaxes(sample, 0, 1)  # (Y, X, Cin) -> Swap Cin with Y 
        sample = np.swapaxes(sample, 1, 2)  # (Y, Cin, X) -> Swap Cin with X
        sample = np.swapaxes(sample, 0, 1)  # (Cin, Y, X) -> Swap Cin with X 

        if self.convModule == 3:     # add Cin for nn.Conv3d module. 4D Tensor is required
            sample = sample[np.newaxis, ...]


        r.append(sample)
        r.append(t)
        r.append(ID)
            
        return r
"""
# Course: MSc AI
# Date: 02/02/2020
# Author: Joseph Azzopardi
# Credits:
# File: sbvsHelper.py
# Version: 1.0
# Description: Helper functions to be used for Structure Based Virtual Screening (SBVS) for Binding Affinity Prediction (Pafnucy).
"""

import pandas as pd
import numpy  as np
import re
import os
import errno
import pybel
import tfbio.data

from pymol import cmd
from math import ceil

from tfbio.data import Featurizer
from sklearn import preprocessing


def processProteinPyMol(path, filename, outputfile=None, f="mol2", overwrite=False ):

    #https://pymolwiki.org/index.php/H_Add
    #https://pymol.org/dokuwiki/doku.php?id=api:cmd:h_add
    """
    Function to process molecule using PyMol: remove hetatm, remove HOH, add hydrogens, change file format
    
    parameters:
        path (string): folder path of input molecule
        filename (string): filename of input molecule, example molecule.pdb
        outputfile (string): name of outputfile. By default same filename prefix will be used. 
                            file extension will be f
        f(string): file extension. By default this is mol2
        overwrite(Bool): Default False to disable overwriting files with the same outputfile name
    return:
        No return values.
    """

    filepath = os.path.join(path, filename)
    
    if not (os.path.isfile(filepath)):
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), filepath)
    
    if outputfile == None:
        tmp_name, tmp_extension = os.path.splitext(filename)
        outputfile = tmp_name 

    try:
        outputfilePath = os.path.join(path, (outputfile + "." + f))
        
        if not (overwrite): 
            if os.path.isfile(outputfilePath):
                raise FileExistsError(errno.EEXIST, os.strerror(errno.EEXIST), outputfilePath)
                
        cmd.load(filepath, object="myprotein")
        cmd.remove("hetatm")
        cmd.remove("resn hoh")
        cmd.h_add("all")
        
        # CHARGES ARE NOT COMPUTED BY PYMOL. Even if files are converted to mol2
        cmd.save(outputfilePath, format=f)
        
        if not (os.path.isfile(outputfilePath)): #if file is not saved
            cmd.delete("myprotein")
            print ("PyMol file %s not saved.\n" % outputfilePath)
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), filename)
            
        cmd.delete("myprotein")
        print ("File %s created." % outputfilePath )

    except Exception as error:
        print ("%s\n" % error)
        print ("PyMol Processing on file %s failed.\n" % filepath)
    
    
    return None



def processProteinPybel(path, filename, outputfile=None, f="mol2", overwrite=False ):

    """
    Function to process molecule using Pybel: remove HOH, add hydrogens, add charges, change file format
    
    parameters:
        path (string): folder path of input molecule
        filename (string): filename of input molecule, example molecule.pdb
        outputfile (string): name of outputfile. By default same filename prefix will be used. 
                            file extension will be f
        f(string): file extension. By default this is mol2
        overwrite(Bool): Default False to disable overwriting files with the same outputfile name
    return:
        No return values.
    """
    
    filepath = os.path.join(path, filename)
    print (filepath)
    
    input_name, input_extension = os.path.splitext(filename)
    
    if not (os.path.isfile(filepath)):
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), filepath)
    
    if outputfile == None:
        #tmp_name, tmp_extension = os.path.splitext(filename)
        outputfile = input_name 

    try:
        outputfilePath = os.path.join(path, (outputfile + "." + f))
        
        if not (overwrite): 
            if os.path.isfile(outputfilePath):
                raise FileExistsError(errno.EEXIST, os.strerror(errno.EEXIST), outputfilePath)
        
        # read pybel molecule. Returns Iterator
        mol = pybel.readfile(input_extension[1:], filepath)
        # extract Molecule from Iterator
        mol = next(mol)
        
        # add hydrogens
        mol.addh()
        
        # Find Water Molecule using SMARTS
        smarts = pybel.Smarts("[OH2]") # Matches a water molecule
        water = smarts.findall(mol)    # Returns a list of tuples with water molecules location
        #print (water)
                
        for w in reversed(water):      # Start in reversed order to preserve location
            #print (*w)
            m = mol.OBMol.GetAtom(*w) 
            mol.OBMol.DeleteAtom(m)
            #print (mol)
        
        # save file in mol2 format to ADD CHARGES
        mol.write(format=f, filename=outputfilePath, overwrite=True)
        
        if not (os.path.isfile(outputfilePath)): #if file is not saved
            print ("PyMol file %s not saved.\n" % outputfilePath)
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), filename)
            
        print ("File %s created." % outputfilePath )

    except Exception as error:
        print ("%s\n" % error)
        print ("Pybel Processing on file %s failed.\n" % filepath)
    
    return None

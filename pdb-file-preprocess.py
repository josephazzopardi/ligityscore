"""
# Course: MSc AI
# Date: 02/02/2020
# Author: Joseph Azzopardi
# Credits:
# File: sbvsLigHotpoints.py
# Version: 1.0
# Description: Program to analyse PDBbind dataset and extract DataFrame containing information on each complex available in PDBbind and check 
#              that their corresponding molecular files are available
# 
# Sample below

        pdbCode 	year 	realAffinity 	pdbSplit 	pdbPath 	                                        dataAvail 	features 	split
    0 	3zzf 	    2012 	0.40 	        general 	./pdbbind/v2016/data/general-set-except-refine... 	True 	    NaN 	    NaN
    1 	3gww 	    2009 	0.45 	        general 	./pdbbind/v2016/data/general-set-except-refine... 	True 	    NaN 	    NaN
    2 	1w8l 	    2004 	0.49 	        general 	./pdbbind/v2016/data/general-set-except-refine... 	True 	    NaN 	    NaN
    3 	3fqa 	    2009 	0.49 	        general 	./pdbbind/v2016/data/general-set-except-refine... 	True 	    NaN 	    NaN
    4 	1zsb 	    1996 	0.60 	        general 	./pdbbind/v2016/data/general-set-except-refine... 	True 	    NaN 	    NaN

"""

import pandas as pd
import numpy  as np
import re
import os
import sys
import errno
import math
import argparse
import rdkit
import shutil

from pymol      import cmd
from rdkit      import Chem
from rdkit      import RDConfig
from rdkit.Chem import Lipinski
from rdkit.Chem import ChemicalFeatures
from rdkit.Chem import Crippen
from rdkit.Chem import Descriptors
from rdkit.Chem import rdmolfiles

from sbvscnnmsc.sbvsHelper import saveDataFrame

# Results Commands Final
# ----
#python3 pdb-file-preprocess.py --input_dir="./pdbbind/v2016_F" --output_Filename='PDB2016dataframe' --pymolFetch | tee msc-results-preprocess-console-output-pdb-2016.txt 
#python3 pdb-file-preprocess.py --input_dir="./pdbbind/v2018_F" --general_IndexFile='INDEX_general_PL_data.2018' --refined_IndexFile='INDEX_refined_data.2018' --output_Filename='PDB2018dataframe' --pymolFetch | tee msc-results-preprocess-console-output-pdb-2018.txt 
# ----

#Script attributes
# -------------------------------
parser = argparse.ArgumentParser(description='SBVS Affinity Prediction based on CNN.',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

io_group = parser.add_argument_group('Input & Output Files-Directories')
io_group.add_argument('--input_dir', '-i', default='./pdbbind/v2016/',
                       help='Directory containing the folders of the PDBBind dataset')
io_group.add_argument('--general_IndexFile', '-g', default='INDEX_general_PL_data.2016',
                       help='Filename for the Index File of the General set of PDBBind')
io_group.add_argument('--refined_IndexFile', '-r', default='INDEX_refined_data.2016',
                       help='Filename for the Index File of the Refined set of PDBBind')
io_group.add_argument('--core_IndexFile', '-c', default='CoreSet.dat',
                       help='Filename for the Index File of the Core set.')
io_group.add_argument('--core2013_IndexFile', '-c3', default='CoreSet2013.dat',
                       help='Filename for the Index File of the Core 2013 set.')

io_group.add_argument('--output_dir', '-o', default='./LigityScore_Output',
                       help='Directory to save output DataFrame')
io_group.add_argument('--output_Filename', '-oF', default='PDBdataframe',
                       help='Filename of output DataFrame.')


runtime_group = parser.add_argument_group('Runtime Variables')
runtime_group.add_argument('--verbose', '-v', default=0, type=int,
                            help='Enable Script Print output.')
runtime_group.add_argument('--pymolFetch', action='store_true', default=False,
                           help='Boolean to enable/disable fetch of ligand from pymol if RDKit cannot load original molecule.')

                            

args = parser.parse_args()


# Variable Definition
# -------------------------------

pd.set_option('display.max_columns', 20)

verbose = args.verbose

dataPath    = args.input_dir
dataFolders = ['data/', 'data/refined-set/' ]

generalFile = args.general_IndexFile
refinedFile = args.refined_IndexFile
coreFile    = args.core_IndexFile
coreFile2013    =  args.core2013_IndexFile


generalFullPath = os.path.join(dataPath, generalFile)
refinedFullPath = os.path.join(dataPath, refinedFile)
coreFullPath    = os.path.join(dataPath, coreFile)
coreFullPath2013    = os.path.join(dataPath, coreFile2013)

#pdbbindSets = {"g":"general", "r":"refined", "c":"core"}
pdbbindSets = {"g":"general", "r":"refined", "c":"core", "c3":"core2013"}
splitSets   = {"tr":"training", "v":"validation", "t":"test"}


columnNames = [
    "pdbCode",
    "year",
    "realAffinity",
    "pdbSplit",
    "ligCode"
]


def getPDBbind(fileFullPath, columnNames, setName=None):
    
    """
    Funtion to convert PDBbind textfile to a dataframe:
    
    Parameters:
        fileFullPath (string): location of textfile
        columnNames(list): list of features to include in dataframe 
        setName(string): set from PDBbind (general, refined, or core)
        
    Return:
        df: Dataframe object with columns 'pdbCodes', 'year', 'realAffinity'
    """
    
    pdbCodes = []
    year = []
    realAffinity = []
    ligCodes = []
    
    df = pd.DataFrame(columns=columnNames)
    
    if (os.path.isfile(fileFullPath)):    #check if path exists 
        print (">>>> Processing File: %s. \n" % fileFullPath )

        c1 = 0
        c2 = 0

        with open (fileFullPath, 'rt') as pdbbindSet:
            for line in pdbbindSet:
                if not line.startswith("#"):
                    columns = line.split()
                    pdbCodes.append(columns[0])
                    year.append(columns[2])
                    realAffinity.append(columns[3]) 
                    
                    try:
                        lig_code = columns[7]

                        lig_code = lig_code.replace('(', '').replace(')', '') #modify string
                        if len(lig_code) == 3:
                            c1 += 1
                        if (len(lig_code) != 3):
                            c2 += 1
                            #print(columns[0], lig_code)
                        
                        ligCodes.append(lig_code)
                    except:
                        pass

        print ("File has %d Ligand names with length of 3." % c1)
        print ("File has %d Ligand names with length no equal to 3." % c2)    


    else:
        raise(FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), fileFullPath))


    df["pdbCode"] = pdbCodes
    df["year"] = year
    df["realAffinity"] = realAffinity
    df["pdbSplit"] = setName

    if c1 != 0:
        df["ligCode"] = ligCodes 
    else:
        df["ligCode"] = None 
    
    print (df.head())
    
    print ("\nTotal Complexes in %s Set: %6d \n" % (setName, df.shape[0]) )
    
    return df


def mergePDBbind(df1, df2, df1_set, df2_set, columnNames ):
    """
    Merge Dataframes and label merger DataFrame. df2 is a subset of df1
    
    Parameters:
        df1 (pandas DataFrame): one of two dataframes requiring merging
        df2 (pandas DataFrame): second of two dataframes requiring merging
        df1_set (string): label of first dataframe
        df2_set (string): label of second dataframe
    Return:
        df1: Merged df1 with df2 and labelled with df1_set, df2_set.
    """
    
    # Confirm all refined set is in general, and that there are no extra pdbCodes outside general set

    # check df1, df2 types
    
    # Use list l to merge dataframes
    l = ["pdbCode", "year", "realAffinity", "ligCode"]

    # merge refined with df1
    df1 = df1.merge(df2, on=l, indicator=True, how='outer')
    merged = df1.groupby('_merge').count() #merge is a df provideing counts. _merge column is created.
    #print (df1.head())

    # List pdbCodes that ARE NOT in refined set (left_only) after merging
    G = merged.loc['left_only', 'pdbCode']

    # List pdbCodes that ARE in refined set and general set (left_only) after merging
    R = merged.loc['both', 'pdbCode']

    # List pdbCodes that ARE ONLY in refined set (right_only) after merging
    R_only = merged.loc['right_only', 'pdbCode']
    
    #df1['split'] = df1['split_x']

    if (R_only != 0):
        #print  ("Found %d pdbCodes from 'Refined Set' that are not in 'General Set' " % R_only)
        raise ValueError ("Found %d pdbCodes from 'Refined Set' that are not in 'General Set' " % R_only)

    if (df2['pdbCode'].count() == R):
        print ("All refined set molecules are in general set.\n")

        #label df1 pdbcodes as refined
        df1['pdbSplit'] = df1['_merge'].apply(lambda x: df2_set if x == 'both' else df1_set) 
        #print (df1.head())

        # Drop Extra Columns
        # Get list of extra columns created. Negate Intersection (union - intersection)
        dropColumns = list((set(df1.columns).union(set(columnNames))) - set(df1.columns).intersection(set(columnNames)))
        #print (">>>>>>>>>>>>>>>>>>>>", dropColumns)
        df1 = df1.drop(columns=dropColumns)
        print (df1.head())
        
    return df1


def mergeCore(df):
    """
    Function that takes dataframe containing General and Refined sets to merge with Core set and label the correct PDBbind set.

    Arguments:
        df (DataFrame):
    Return:
        df_pdb (DataFrame): DataFrame including Core Set details.
    """
    
    # Merge and Label Datasets

    # Use list l to merge dataframes
    l = ["pdbCode", "year", "realAffinity"]
    #l = ["pdbCode", "year"]

    # merge refined with df1
    df_pdb = df.merge(df_c, on=l, indicator=True, how='outer')
    merged = df_pdb.groupby('_merge').count() #merge is a df provideing counts. _merge column is created.
    #df_pdb.groupby('_merge').count()
    #print (df_pdb.head())

    # List pdbCodes that ARE NOT in core set (left_only) after merging
    G_R = merged.loc['left_only', 'pdbCode']

    # List pdbCodes that ARE in refined set and core set (left_only) after merging
    C = merged.loc['both', 'pdbCode']

    # List pdbCodes that ARE ONLY in refined set (right_only) after merging
    C_only = merged.loc['right_only', 'pdbCode']

    df_pdb['pdbSplit'] = df_pdb['pdbSplit_x']

    print ("Found %d pdbCodes from 'Core Set' that ARE in 'Refined Set'.\n" % C)

    if (C_only > 0):
        print ("Found %d pdbCodes from 'Core Set' that did not match all 3 fields (PDBCode, Year, Real Affinity).\n" % C_only)
        print ("Possible Duplicates Found.\n")
        
    df_pdb.loc[df_pdb._merge == "right_only", 'pdbSplit'] = pdbbindSets["c"]
    df_pdb.loc[df_pdb._merge == "both"      , 'pdbSplit'] = pdbbindSets["c"]
    #df_pdb['split'] = df_pdb['_merge'].apply(lambda x: pdbbindSets["c"] if x == 'both' else pass ) 

    #print (df_pdb.head())

    # Drop Extra Columns
    # Get list of extra columns created. Negate Intersection (union - intersection)
    dropColumns = list((set(df_pdb.columns).union(set(columnNames))) - set(df_pdb.columns).intersection(set(columnNames)))
    df_pdb = df_pdb.drop(columns=dropColumns)

    return df_pdb


def checkDropDuplicates(df_pdb):

    """
    Function to check for any duplicates in the dataframe. This can occur when core set is merged since the CoreFile is not actually part of the PDBBind Set.
    These could occurs since merge of dataframes was done on PDBCode, Year, and Affinity Values. Any duplicates are dropped. 

    Arguments: df_pdb (DataFrame):
    Returns: df_pdb (DataFrame):
    """

    duplicateCodesIndex = []

    duplicateRowsDF = df_pdb[df_pdb.duplicated('pdbCode')]
    duplicateCodes = duplicateRowsDF['pdbCode']
    duplicatePDBSplit = duplicateRowsDF['pdbSplit']

    for Code, split in zip(duplicateCodes, duplicatePDBSplit):

        print (Code, split)
        if (split == "core"): 

            print("Checking for Duplicates for %s in Refined Set ..." % (Code))
            
            if not (df_pdb[(df_pdb['pdbCode'] == Code) & (df_pdb['pdbSplit'] == "refined")].empty):
                print("Duplicate Found!\n")
                print(df_pdb[(df_pdb['pdbCode'] == Code) & (df_pdb['pdbSplit'] == "refined") ])
                duplicateCodesIndex.append(df_pdb[(df_pdb['pdbCode'] == Code) & (df_pdb['pdbSplit'] == "refined") ].index)
                print("Added Index to List.\n")
                
                # Core set to not have ligand Codes. Therefore ligand Codes need to be copied from refined record to core record.
                c_index = df_pdb[(df_pdb['pdbCode'] == Code) & (df_pdb['pdbSplit'] == "core")].index
                r_index = df_pdb[(df_pdb['pdbCode'] == Code) & (df_pdb['pdbSplit'] == "refined")].index
                #print ("##############", df_pdb.loc[r_index, "ligCode"])
                df_pdb.loc[c_index, "ligCode"] = df_pdb.loc[r_index, "ligCode"].item()
                #print ("#######", df_pdb.loc[c_index, "ligCode"])


            print("Checking for Duplicates for %s in General Set ..." % (Code))
            
            if not (df_pdb[(df_pdb['pdbCode'] == Code) & (df_pdb['pdbSplit'] == "general")].empty):
                print("Duplicate Found.\n")
                print(df_pdb[(df_pdb['pdbCode'] == Code) & (df_pdb['pdbSplit'] == "general") ])
                duplicateCodesIndex.append(df_pdb[(df_pdb['pdbCode'] == Code) & (df_pdb['pdbSplit'] == "general") ].index)
                print("Added Index to List.\n")
    print ("\n\n")

    for codeIndex in duplicateCodesIndex:
        print ("Dropping Index %d. DataFrame Entry is:\n %s \n" % (codeIndex.item(), str(df_pdb.iloc[codeIndex])))
        df_pdb.drop(df_pdb.index[codeIndex], inplace=True)

    return df_pdb

def searchPDBFolders(startPath):

    """
    Function to search for folders within the given path and collect this information to build a new dataset with pdbCodes and their location.

    Parameters:
        startPath (string): Full path of directory to pdb-bind molecule folders.
    Returns:
        df (DataFrame): Created dataframe from this information.
    """

    maxLevel = 0
    pdbCodeFolders = []
    pdbCodePaths  = []
    setFolders = []
    split = []

    for subdirFullPath, dirs, files in os.walk(startPath):
        level = subdirFullPath.replace(startPath, '').count(os.sep)
        if level > maxLevel:
            maxLevel = level
            
    maxLevel = maxLevel + 1

    if (maxLevel > 2):
        raise ValueError ("Expected 2 subdirectory (refined, general) in dataset path. Got %d !" % (maxLevel))
    else:
        print ("Found %d Directory Levels" % maxLevel)
        
    setFolders = []    

    for subdirFullPath, dirs, files in os.walk(startPath):
        level = subdirFullPath.replace(startPath, '').count(os.sep)
        if subdirFullPath == startPath:
            next
        elif level == 0:
            setFolders.append (subdirFullPath.replace(startPath, ''))
            print (subdirFullPath)
        elif level == 1:   
            if (re.search( "/....$", subdirFullPath)):
                pdbCodePaths.append (subdirFullPath)
                pdbCodeFolders.append (subdirFullPath[-4:])
                if (re.search( "refined-set", subdirFullPath)):
                    split.append ( pdbbindSets["r"] )
                elif (re.search( "general-set", subdirFullPath)):
                    split.append ( pdbbindSets["g"] )
                #pdbCodeFolders.append (subdirFullPath.replace(os.path.join(startPath, setFolders[level-1]), ''))

    print ("\nFound %d pdbCode Folders.\n" % len(pdbCodeFolders))  

    # create dataset based on collected information
    columnNames = ["pdbCode", "pdbSplit", "pdbPath", "dataAvail", "features", "split"]

    df = pd.DataFrame(columns=columnNames)
    df['pdbCode']  = pdbCodeFolders
    df['pdbSplit'] = split
    df['pdbPath']  = pdbCodePaths

    return df


def checkPDBFolders(df):
    """
    Function to check if each of the protein folders contains 4 files (or 6 if files are already processed).
    Names of the files are not checked.

    Parameters:
        df (DataFrame): Full path of directory to pdb-bind molecule folders.
    Returns:
        df (DataFrame): Modified dataframe with information if protein-ligand files are available..
    """
    i=0
    include = []

    for path in df['pdbPath']:
        files = os.listdir(path) #list contect of directory, returns list of all files
        #print (files)
        file_count = 0
        for file in files:
            if (os.path.isfile(os.path.join(path, file))):
                file_count += 1
        #subdirFullPath, dirs, files = next(os.walk(path))
        if not ( len(files) >= 4 or len(files) <= 6 ):
            print ("Files in path %s Not Found.\n %d Files Found." % (path, len(files)))
            print ("Verify Files at %s.\n" % path)
            include.append(False)
        else:
            include.append(True)
            i += 1
            
    print ("%d out of %d files found.\n" % (i, len(df['pdbPath'])))
    df['dataAvail'] = include

    return df


def mergeDF(df_index, df_mol):
    """
    Function to compare datasets obtained from PDBBind INDEX files with molecular file data available in the general and refined sets. The two datasets are
    joined to check if there are any Protein-Ligand affinity figures with no data files, or vice versa.
    
    Arguments:
        df_index (DataFrame): Dataframe built from the PDBBind INDEX files
        df_mol (DataFrame): Dataframe built from the PDBBind INDEX files
    Results:
        df_final (DataFrame): Merged DataFrame of df_index and df_mol.
    """


    df_mol = df_mol.drop(columns='pdbSplit')

    print (">>>>> Merging the 2 DataFrames. Sample of each below:\n\n")
    print("Molecule Dataframe:\n", df_mol.head())
    print("\n\nIndex Dataframe:\n", df_index.head())

    # Use list l to merge dataframes
    l = ["pdbCode"]

    # merge refined with df1
    df_final = df_index.merge(df_mol, on=l, indicator=True, how='outer')
    
    mergeResults = df_final.groupby('_merge').count()

    if (mergeResults.loc['left_only', 'pdbCode'] > 0):
        
        print ("%d P-L Complexes have no data files.\n" % mergeResults.loc['left_only', 'pdbCode'] )
        df_final = df_final.drop(columns='_merge')
        
        print ("Removing P-L complexes with no data files from dataset ...")
        df_final = df_final[df_final.dataAvail.notna()]
    

    elif ((mergeResults.loc['right_only', 'pdbCode'] > 0)):
        
        print ( "%d Data files have no Affinity Values.\n" % mergeResults.loc['right_only', 'pdbCode'] )
        df_final = df_final.drop(columns='_merge')
        
        print ("Removing P-L complexes with no Affinity from dataset ...")
        df_final = df_final[df_final.realAffinity.notna()]


    print ("%d Data folders with molecule files and corresponding binding Affinity Values available.\n" % mergeResults.loc['both', 'pdbCode'])

    print("Merge Results:\n")
    print(mergeResults.head())

    return df_final




def checkLigandFile(df):

    """
    Function to load ligands using RDKit. If RDKit returns Null it means that the molecule has an incorrect format.
    In the case 'mol2' return NULL, the molecure is tried again using SDF file format. If also SDF format is incorrect,
    this functions triggers another function to try to solve issue by 'fetching' the ligand directly from pymol.
    """

    l_suff = "_ligand.mol2"
    #l_suff = "_ligand.sdf"

    l_mol2Count = 0         # Track number of incorrect mol2 files
    l_mol2toSDFCount = 0    # Track number of successful sdf files read after mol2 were unsuccessful directly from dataset
    i = 0                   # Track number of molecules that could not be loaded in mol2 or sdf from PDBbind
    PL_noneCount = 0        # Track number of Real number of incorrect files after trying to import from Pymol
    j = 0                   # track iteration count
    discarded = []          # Track discarded status of molecule

    # iterate over the dataframe
    for index, row in df.iterrows():
        j += 1 
        
        # get molecule paths
        ligand = row[3]
        P, pl_path = row[0], row[6]
        l_file = os.path.join(pl_path, P+l_suff)
        if verbose:
            print ("Ligand File:   %s" % (l_file))
        
        d = False

        # load molecule file - MOL2
        l_mol = Chem.rdmolfiles.MolFromMol2File(molFileName=l_file)
       
        # Load Molecule in RDKit
        if l_mol is None: # Check if ligand molecule was loaded correctly. If not try with SDF file type
            l_mol2Count += 1
            if verbose:
                print ("Molecule %s  in %s format is not valid. Trying with SDF format ... " % (P, l_suff))
            
            d = True

            l_file = os.path.join(pl_path, P+"_ligand.sdf")

            # load molecule file - SDF    
            suppl = Chem.SDMolSupplier(l_file)
            for mol in suppl:
                l_mol = mol
            
            # Re-check rdkit molecule loading
            if l_mol is not None:
                print ("Molecule successfully loaded using SDF.")
                l_mol2toSDFCount += 1
                d = False

                discarded.append(d)
                
            else:
                PL_noneCount += 1 # track Null count After pymol fix
                i += 1 # track Null Count Before pymol fix
                
                #if verbose:
                #    print ("Null Count: %17d" % (i+1))
                #    print ("Null Count After Fix: %8d" % PL_noneCount)
                d = True

                if args.pymolFetch:
                    # Since l-mol is incorrect, try to Download directly from pymol
                    # Call Pymol Script if ligand code is correct
                    if len(ligand) == 3:
                        pymolImportLigand(ligand, l_file )

                        suppl = Chem.SDMolSupplier(l_file)
                        for mol in suppl:
                            l_mol = mol

                        # load Molecule
                        if l_mol is not None:
                            print ("Molecule %s is now Valid." % P)
                            PL_noneCount -= 1
                            d = False
                            #if verbose:
                            #    print ("Null Count: %17d" % (i+1))
                            #    print ("Null Count After Fix: %8d" % PL_noneCount)
                        else:
                            print ("Molecule is still an issue.")
                            d = True
                    else:
                        print ("Molecule is still an issue.")
                        d = True
                
                discarded.append(d)
                        
            #raise ValueError('Invalid Molecules. Could not load ligand at %s.\n' % l_file)
        else:
            d = False
            discarded.append(d)
            print ("Iteration: %18d/%d" % (j , len(df.index)))
            print ("Mol2 Incorrect files: %8d" % l_mol2Count)
            print ("SDF Saved: %18d" % l_mol2toSDFCount)
            print ("Null Count: %17d" % i) # Null values from PDBbind files
            print ("Null Count After Fix: %8d \n" % PL_noneCount) # Null values after trying to fetch from pymol
            continue

        #discarded.append(d)    

        print ("Iteration: %18d/%d" % (j , len(df.index)))
        print ("Mol2 Incorrect files: %8d" % l_mol2Count)
        print ("SDF Saved: %18d" % l_mol2toSDFCount)
        print ("Null Count: %17d" % i) # Null values from PDB files
        print ("Null Count After Fix: %8d \n" % PL_noneCount) # Null values after trying to fetch from pymol
                 
        if j == -1: #change limit to break
            break


    df["discarded"] = discarded

    return None



def pymolImportLigand(ligand, ligandPath, f="sdf"):
    """
    Function to take a copy of the original ligand file and download the ligand file again using pymol.
    """

    # mv original file to .original
    #os.rename(ligandPath, ligandPath+".original" )
    shutil.copy(ligandPath, ligandPath+".original")
    overwrite = True

    print (ligand)

    try:
        
        if not (overwrite): 
            if os.path.isfile(ligandPath):
                raise FileExistsError(errno.EEXIST, os.strerror(errno.EEXIST), ligandPath)
                
        #cmd.load(filepath, object="myprotein")
        #cmd.remove("hetatm")
        #cmd.remove("resn hoh")
        #cmd.h_add("all")
        
        cmd.fetch(ligand, path="../pymoltempdir")

        # CHARGES ARE NOT COMPUTED BY PYMOL. Even if files are converted to mol2
        cmd.save(ligandPath, format=f)
        
        if not (os.path.isfile(ligandPath)): #if file is not saved
            cmd.delete(ligand)
            print ("PyMol file %s NOT saved.\n" % ligandPath)
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), ligandPath)

        print ("PyMol file %s Saved.\n" % ligandPath)

        cmd.delete(ligand)
        print ("File %s created." % ligandPath )
   
    except Exception as error:
        print ("%s\n" % error)
        print ("PyMol Processing on file %s failed.\n" % ligandPath)
       



# Main Program
# -------------------------------

print ("Program to process the PDBBIND dataset.\n----------------------------------------------------\n\n")
print ("+++++.Part 1 - Index Files\n--------------\n\n")

#########  Get Dataset from Data Files (Index Files) for General, Refined, and Core sets

# Read Index_General File
df_g = getPDBbind(generalFullPath, columnNames, setName=pdbbindSets["g"])

# Read Index_Refined File
df_r = getPDBbind(refinedFullPath, columnNames, setName=pdbbindSets["r"])

# Merge and Label Datasets
df_pdb = mergePDBbind(df_g, df_r, pdbbindSets["g"], pdbbindSets["r"], columnNames )

print ("\n\n----- DataFrame Grouped by pdbSplit:\n")
print(df_pdb.groupby('pdbSplit').count(), "\n\n")



# Read Core Dataset
df_c = getPDBbind(coreFullPath, columnNames, setName=pdbbindSets["c"])
df_c = df_c.drop(columns='ligCode')

print(df_c.head())

# Merge Core Dataset, and Label Datasets
df_pdb = mergeCore(df_pdb)

print ("\n\n----- DataFrame including General, Refined, and Core Sets.:\n")
print(df_pdb.info(), "\n\n")

# Print Data Set Split
counts = df_pdb.groupby('pdbSplit').count()
print ("----- Split Summary:")
print ( "Total Number of General Complex: %5d" % counts.loc['general', 'pdbCode'])
print ( "Total Number of Refined Complex: %5d" % counts.loc['refined', 'pdbCode'])
print ( "Total Number of Core    Complex: %5d\n\n" % counts.loc['core', 'pdbCode'])

# Check for any duplicates as matching was done on Code, 
df_pdb = checkDropDuplicates(df_pdb)

# Print Data Set Split
print ("----- Split Summary after Drop Duplicates:")
counts = df_pdb.groupby('pdbSplit').count()
print ( "Total Number of General Complex: %5d" % counts.loc['general', 'pdbCode'])
print ( "Total Number of Refined Complex: %5d" % counts.loc['refined', 'pdbCode'])
print ( "Total Number of Core    Complex: %5d" % counts.loc['core', 'pdbCode'])

print ("\n\n----- DataFrame including General, Refined, and Core Sets.:\n")
print(df_pdb.info(), "\n\n")

# check for NAN values
if (df_pdb.isna().sum().any()):
    print ("----- No NAN Values Found")
assert not (df_pdb.isna().sum().any()), "Compiled Dataset included NAN values"

####  Include Core2013 dataset

# Read Core Dataset
df_c3 = getPDBbind(coreFullPath2013, columnNames, setName=pdbbindSets["c3"])
df_c3.drop(columns=['ligCode'], inplace=True)


# Merge datasets
l = ["pdbCode", "year", "realAffinity"] # merge columns to use
df_pdb = df_pdb.merge(df_c3, on=l, indicator=True, how='outer')

print ("DataFrame after merge with core2013")
print(df_pdb.head())

# Remove Merge Column. Change names of columns. The pdbSplit_y column is rename the C2013 and marks only those that are part of the core2013 set.
df_pdb.drop(columns=['_merge'], inplace=True)
df_pdb.columns = ["pdbCode", "year", "realAffinity", "ligCode", "pdbSplit",  "C2013"]

print ("\nDataFrame after adding core2013 column.")
print(df_pdb.head())

print ("\nDataFrame split by group after core2013 addition.")
print(df_pdb.groupby('pdbSplit').count())

# Assign Core2013 molecules that are in general set or refined set to core2013 so that these are excluding from validation or training.
df_pdb['pdbSplit'][(df_pdb['pdbSplit'] == "refined") & (df_pdb['C2013'] == "core2013")] = "core2013"
df_pdb['pdbSplit'][(df_pdb['pdbSplit'] == "general") & (df_pdb['C2013'] == "core2013")] = "core2013"

print ("\nDataFrame Summary after 2013 addition.")
print(df_pdb.groupby('pdbSplit').count())


#########  Get Dataset from Molecule Files (Protein and Ligand)


print ("\n\n\n+++++.Part 2 - Molecule Files\n--------------\n\n\n")
startPath = os.path.join(dataPath, dataFolders[0])
print (">>>>> The proteins and ligands files from %s will be processed to extract features.\n\n" % startPath)

# Search all available folders and create initial DF
df_molFiles = searchPDBFolders(startPath)

print ("Split Summary:")
print(df_molFiles.tail())

print ("Split Summary:")
print(df_molFiles.groupby('pdbSplit').count())

# Confirm if folders are not empty
print (">>>>> Check if proteins and ligands files are available.")
df_molFiles = checkPDBFolders(df_molFiles)


print ("\n\n+++++.Part 3 - Merge Index Dataframe and Molecules DataFrame\n--------------\n\n\n")

# Merge DataFrames to create one dataset with pdbCodes that have files available with a corresponding affinity value.
dfFinal = mergeDF(df_pdb, df_molFiles)

print("\n\nFinal Dataframe:\n", dfFinal.head())
print ("\n\nFinal Dataframe split summary:\n", dfFinal.groupby('pdbSplit').count())

# Check if RDkit returns None for any of the molecules.
checkLigandFile(dfFinal)

print (">>>>> Save Final DataFrame")
saveDataFrame (dfFinal, args.output_dir, args.output_Filename, c="zip")

print ("\nSplit Summary:")
print(dfFinal.groupby('pdbSplit').count())

print ("\nSplit Summary:")
print(dfFinal.groupby('discarded').count())

print("\nDiscarded from Refined Set:")
print((dfFinal[(dfFinal['pdbSplit'] == "refined") & (dfFinal['discarded'] == False)]).count())
print("\nDiscarded from General Set:")
print((dfFinal[(dfFinal['pdbSplit'] == "general") & (dfFinal['discarded'] == False)]).count())
print("\nDiscarded from Core Set:")
print((dfFinal[(dfFinal['pdbSplit'] == "core") & (dfFinal['discarded'] == False)]).count())

print("\nDiscarded from Core2013 Set:")
print((dfFinal[(dfFinal['C2013'] == "core2013") & (dfFinal['discarded'] == False)]).count())

print("\nDiscarded from Core2013 Set coming from general and refined:")
print((dfFinal[(dfFinal['pdbSplit'] == "core2013") & (dfFinal['discarded'] == False)]).count())


            




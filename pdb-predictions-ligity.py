"""
# Course: MSc AI
# Date: 02/02/2020
# Author: Joseph Azzopardi
# Credits:
# File: pdb-predictions-ligity.py
# Version: 1.0
# Description: Load a previously trained LigityScore CNN Model, and run predictions for Training, Validation, CASF-2013, and CASF-2016.
"""

#from SBVSCNNMSC import sbvscnn
import pandas as pd
import numpy  as np
import seaborn as sns
import matplotlib.pyplot as plt
import pybel
import re
import os
import errno
import tfbio.data
import warnings
import argparse

from math import ceil
from pymol import cmd
from IPython.display import Image
#from tfbio.data import Featurizer
#from tfbio.data import make_grid, rotate
from sklearn.linear_model import LinearRegression

import torch
from torch import nn
import torch.nn.functional as F
import torch.optim as optim

#from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader

# Import from Personal Module
from sbvscnnmsc.sbvsCNN  import myNet
from sbvscnnmsc.sbvsCNN  import init_weights
from sbvscnnmsc.sbvsData import datasets
from sbvscnnmsc.sbvsData import PDBdataset
from sbvscnnmsc.sbvsData import PDBdatasetLigity
from sbvscnnmsc.sbvsData import PDBdatasetLigity1D
from sbvscnnmsc.sbvsHelper import loadDataFrame
from sbvscnnmsc.sbvsHelper import evalMetrics
from sbvscnnmsc.sbvsHelper import predictResults

#https://pytorch.org/docs/stable/notes/randomness.html

np.random.seed(123)
torch.manual_seed(123)

# Ignore warnings
warnings.filterwarnings("ignore")

# LigityScore3D
# python3 pdb-predictions-ligity.py --input_dir "./LigityScore_Output/split-uint8" --model_dir "./tensorboard-runs/run-ligity-gpu-6" --model_file "model-cnn-epoch60" --new_shape 160 140 20 --verbose 0

# LigityScore1D
# python3 pdb-predictions-ligity.py --input_dir "./LigityScore_Output/LigityScore1D" --model_dir "./tensorboard-runs/LigityScore1D/run-ligity1D-gpu-9" --model_file "model-cnn-epoch32" --in_channels 1 --LigDimension 1 --convModule 2 --Convdim 64 128 256 --normalizationFn "instance" --normalizationList 0 1 1 1 --convDropout 0.0 0.1 0.2 0.2 --convDropoutList 0 1 1 1 --mpool 0 1 1 1 --FCdim  1000 500 200 1 --activationList 1 1 1 1  --dropout 0.5 0.5 0.5 0 --dropoutList 1 1 1 0 --verbose 0



# Script Arguments
# -------------------------------
parser = argparse.ArgumentParser(description='SBVS Affinity Prediction based on CNN.',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

io_group = parser.add_argument_group('Input & Output Files-Directories')
io_group.add_argument('--input_dir', '-i', default='./LigityScore_Output/split-uint8',
                      help='directory with training, validation and test sets')
io_group.add_argument('--model_dir', default='./tensorboard-runs/run-ligity-gpu-5',
                      help='main directory to store previously trained models')
io_group.add_argument('--model_file', default='model-cnn-epoch0', 
                      help='Pre-trained Model File name')
io_group.add_argument('--dataset_name', default='Test', 
                      help='Dataset to be used for prediction results')

runtime_group = parser.add_argument_group('Runtime Script Parameters.')
runtime_group.add_argument('--verbose', '-v', type=int, default=0,
                           help='Print Verbose Details.')
runtime_group.add_argument('--LigDimension', '-ld', default=3, type=int,
                           help='Number of features to group to find distance.  \'1\' of \'3\' are allowed values. \
                           A \'1\' implies that features will be grouped in pairs producing one distance (1D). \
                           A \'3\' implies that 3 features will be grouped forming a traingle and producing 3 distance (3D).')

train_group = parser.add_argument_group('Neural Network General Training Parameters')
train_group.add_argument('--train_set', '-ts', default='Train',
                          help='Parameter from datasets Class to use for CNN Training. Default is Train Set (datasets.Train) ')
train_group.add_argument('--val_set', '-vs', default='Validation',
                          help='Parameter from datasets Class to use for CNN Validation. Default is Validation Set (datasets.Validation) ')
train_group.add_argument('--new_shape', '-ns', nargs='+', type=int, default=(80,80,70),
                          help='Parameter to reshape dataset feature in this shape from (1120, 20, 20) ')
train_group.add_argument('--batchSize', '-b', type=int, default=20,
                          help='Batch Size to use for Training.')
train_group.add_argument('--rotations', '-r', type=int, default=1,
                          help='Rotations used to rotate feature Matrix. Default is one which indicates no rotation. Allowed values 4 and 8.')
train_group.add_argument('--learningRate', '-lr', type=float, default=0.00001,
                          help='Learning Rate used for back propagation (Optimizer).')
train_group.add_argument('--regl2', '-Rl2', type=float, default=0.001,
                          help='Optimiser L2 Regularisation parameter for weight decay.')
train_group.add_argument('--dataWorkers', '-dw', type=int, default=4,
                          help='Number of worker thread to use in dataloader')

conv_group = parser.add_argument_group('Convolutions Training Parameters')                                                
conv_group.add_argument('--convModule', '-cM', type=int, default=2,
                         help='Parameter to define if convolution should use nn.Conv2d or nn.Conv3d. Allowed values are "2" and "3". ')
conv_group.add_argument('--convKSize', '-cKS', type=int, default=5,
                         help='Filter Size to use in convolution.')
conv_group.add_argument('--convStride', '-cS', type=int, default=1,
                         help='Stride value to use in convolution.')
conv_group.add_argument('--convPad', '-cP', type=int, default=2,
                         help='Padding value to use in convolution.')
conv_group.add_argument('--Convdim','-cD', nargs='+', type=int, default=[64, 128, 256, 512], 
                         help='List of sizes of Convolutional Filters.')
conv_group.add_argument('--normalizationFn','-nF', default='batch', 
                         help='Normalization function to apply after convolution. Allowed values \'batch\' or \'instance\'.')
conv_group.add_argument('--normalizationList','-nL', nargs='+', type=int, default=[0, 0, 0, 0], 
                         help='List of sizes of Convolutional Filters.')
conv_group.add_argument('--convDropout','-cDrP', nargs='+', type=float, default=[0.0, 0.0, 0.0, 0.0], 
                         help='Dropout Percentage to use in Convolution Layers. Probability of an element to be zeroed.')
conv_group.add_argument('--convDropoutList','-cDr', nargs='+', type=int, default=[0, 0, 0, 0], 
                         help='List of "1" or "0" to represent if dropout2d-3d should be enabled or disabled respectively at each of the Convolution Layers. Must of of size equal to the number of layers.')
conv_group.add_argument('--in_channels', '-iCh', type=int, default=1,
                         help='C_in for Convolution Layers.')

maxpool_group = parser.add_argument_group('Max Pooling Layers Training Parameters') 
maxpool_group.add_argument('--mpoolWindow', '-mpW', type=int, default=2,
                            help='MaxPool Windows to use in maxpooling.')
maxpool_group.add_argument('--mpoolStride', '-mpS', type=int, default=2,
                            help='MaxPool Stride to use in maxpooling. Default is 2 and mpoolWindow of 2 and mpoolStride reduces dimenstion to half.')
maxpool_group.add_argument('--mpoolPad', '-mpP', type=int, default=0,
                            help='MaxPool Padding to use in maxpooling.')
maxpool_group.add_argument('--mpool','-mpL', nargs='+', type=int, default=[0, 1, 1, 1, 1], 
                            help='List of "1" or "0" to represent if max pooling function should be enabled or disabled respectively after each of the Convolution Layers. Must of of size equal to the number of Conv layers. \nDefault all layers include MaxPooling function.')

fc_group = parser.add_argument_group('Convolution Training Parameters')
fc_group.add_argument('--activation', '-a', default="relu",
                       help='Activation Function to use for Fully Connected Layers. Default Activation Function is "relu".')
fc_group.add_argument('--activationList','-aL', nargs='+', type=int, default=[1, 1, 1, 1, 1], 
                       help='List of "1" or "0" to represent if activation function should be enabled or disabled respectively at each of the Fully Connected Layers. Must of of size equal to the number of layers. \nDefault all layers include activation function.')
fc_group.add_argument('--FCdim','-fcD', nargs='+', type=int, default=[6000, 1000, 500, 200, 1], 
                       help='List of sizes of fully connected layers.')
fc_group.add_argument('--dropout', '-d', nargs='+', type=float, default=[0.5, 0.5, 0.5, 0],
                       help='Dropout Percentage to use in Fully Connected Layers. Probability of an element to be zeroed.')
fc_group.add_argument('--dropoutList','-dL', nargs='+', type=int, default=[1, 1, 1, 1, 0], 
                       help='List of "1" or "0" to represent if dropout should be enabled or disabled respectively at each of the Fully Connected Layers. Must of of size equal to the number of layers.')


args = parser.parse_args()


# SNS Visualisation Parameters
# -------------------------------

colourSets   = {"tr":"g", "v":"b", "t":"r", "t3":"m"}
sns.set_palette("deep")
sns.set_style("whitegrid") #sns.set_style("dark")
sns.set_context("poster", font_scale = 0.5, rc={"grid.linewidth": 0.8, "axes.titlesize": 18})

# Addtion plot properties
"""
{'axes.labelsize': 17.6,
 'axes.titlesize': 19.200000000000003,
 'font.size': 19.200000000000003,
 'grid.linewidth': 1.6,
 'legend.fontsize': 16.0,
 'lines.linewidth': 2.8000000000000003,
 'lines.markeredgewidth': 0.0,
 'lines.markersize': 11.200000000000001,
 'patch.linewidth': 0.48,
 'xtick.labelsize': 16.0,
 'xtick.major.pad': 11.200000000000001,
 'xtick.major.width': 1.6,
 'xtick.minor.width': 0.8,
 'ytick.labelsize': 16.0,
 'ytick.major.pad': 11.200000000000001,
 'ytick.major.width': 1.6,
 'ytick.minor.width': 0.8}
"""


# Classes and Function Definition
# -------------------------------


def savePlot(f, plotFileName, plotPath="./", DPI=600):
    """
    Function to save SNS plot.
    Parameters:
        f (plot): plot object
        plotFileName (string): Name to use to save plot to disk
        plotPath (string): Location of where to save plot on disk
        DPI (int): Resolution of Plot
    Return: None
    """
    try:
        plotFullPath = os.path.join (plotPath, plotFileName)
        figure = f.get_figure()    
        figure.savefig(plotFullPath, dpi=DPI)
        print ("%s plot save at %s.\n" % (plotFileName, plotFullPath))
    except Exception as error:
        print ("\n%s\n" % error)
        print ("Saving of %s at %s FAILED.\n" % (plotFileName, plotFullPath))



# Variables Definition - Network Parameters
# -------------------------------

#Convolution
newShape = tuple(args.new_shape)
in_channels = args.in_channels          # Cin (Z dimention )
convModuleDim = args.convModule         # var to define if Conv2D or Conv3D is used.
conv_dim = args.Convdim
conv_layers = len(conv_dim)
convKernelSize = args.convKSize
convStride = args.convStride
convPad = args.convPad
normalizationFn = args.normalizationFn
normalizationList = args.normalizationList
convDropout = args.convDropout
convDropoutList = args.convDropoutList

#grid_size

#Max Pooling
pooling = args.mpool
maxPoolWindow = args.mpoolWindow
maxPoolStride = args.mpoolStride
maxPoolPad = args.mpoolPad

#Fully Connected 
fc_dim = args.FCdim
fc_layers = len(fc_dim)

# Training Parameters
batchSize = args.batchSize
activation = args.activationList
activation_fn = args.activation
learningRate = args.learningRate
reg_l2 = args.regl2
dropout = args.dropoutList
dropoutProb = args.dropout
num_rotations = args.rotations # This is required to set the size of the input if there is rotation. Rotation increase the size of matrix


# Variables Definition 
# -------------------------------
#splitSets   = {"tr":"Train", "v":"Validation", "t":"Test"}
splitSets   = { "v":"Validation", "t":"Test", "tr":"Train", "t3":"Test2013"}

verbose = args.verbose

angle_rotations = {}

if num_rotations == 4:
    for i, angle in enumerate(list(range(0,359,90)), 0):
        # key:value - The key is the family combination. Value is the index.
        angle_rotations.update( { i : angle} )
    R = True
elif num_rotations == 8:
    for i, angle in enumerate(list(range(0,359,45)), 0):
        # key:value - The key is the family combination. Value is the index.
        angle_rotations.update( { i : angle} )
    R = True
else:
    angle_rotations.update( { 0 : 0} )
    R = False


print("\n\nSBVS Affinity Prediction based of LigityScore Features.")
print("---------------------------------------------------\n\n")

# GPU definition
# -------------------------------
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print("\n\nDevice for training is:", device)
print(torch.__config__.show())

# Print Definitions
# Print Definitions
print("\n\n*********** CNN Network Definitions.\n" % ())
print("Input Channels: %23d" % (in_channels))
print("Convolution Layers: %18d" % (conv_layers))
print("Convolution Layers Input Dimensions: %s" % (str(conv_dim)))
print("Convolution Kernel Size: %13d" % (convKernelSize))
print("Convolution Stride: %18d" % (convStride))
print("Convolution Padding: %17d" % (convPad))
print("Normalization Function: %15s" % (normalizationFn))
print("Normalization Layer Activation: %17s" % (str(normalizationList)))
print("Convolution Dropout Percentage: %15s" % str(convDropout))
print("Convolution Dropout Layer Activation: %17s\n" % (str(convDropoutList)))

print("Max Pool Layers Enabled (first is input layer so needs to be 0): %s" % (str(pooling)))
print("Max Pool Kernel Size: %10d" % (maxPoolWindow))
print("Max Pool Stride: %15d" % (maxPoolStride))
print("Max Pool Padding: %14d\n" % (maxPoolPad))

print("FC Layers Input Dimensions (%d): %20s\n" % (fc_layers, str(fc_dim)))

print("CNN Training Parameters.\n" % ())
print ("Batch Size: %17d" % (batchSize))
print ("Activation Function: %10s" % (activation_fn))
print ("Activation Layers: %20s" % activation)
print ("Learning Rate: %19.5f" % (learningRate))
print ("L2 Regularization Decay: %8.4f" % (reg_l2))
print ("Dropout Layers: %23s" % dropout)
print ("Dropout Rate: %17s" % str(dropoutProb))


# load datasets
dataset = datasets()
print ("\n\n*********** Load Datasets ...\n")
for i, Set in enumerate(splitSets):
    file = splitSets[Set] + "-pkl"
    setattr(dataset, splitSets[Set], loadDataFrame(args.input_dir, file) ) # load dataframe. #use setattr from class.

print ("*********** Datasets Summary:\n")
print ("Train Dataset Size: %d \nTrain Batches: %d \n" % (len(dataset.Train), ceil(len(dataset.Train)/batchSize)))
print ("Test Dataset Size: %d  \nTest Batches: %d \n" % (len(dataset.Test), ceil(len(dataset.Test)/batchSize)))
print ("Test 2013 Dataset Size: %d  \nTest 2013 Batches: %d \n" % (len(dataset.Test2013), ceil(len(dataset.Test2013)/batchSize)))
print ("Validation Dataset Size: %d  \nValidation Batches: %d \n" % (len(dataset.Validation), ceil(len(dataset.Validation)/batchSize)))


# LigityScore 3D features are either 'uint8' or 'int16'. Change to 'float'
if dataset.Test['features'].iloc[0].dtype == 'uint8' or dataset.Test['features'].iloc[0].dtype == 'int16':
    dataset.Train['features'] = dataset.Train['features'].apply(lambda x: x.astype('float32'))
    dataset.Test['features'] = dataset.Test['features'].apply(lambda x: x.astype('float32'))
    dataset.Test2013['features'] = dataset.Test2013['features'].apply(lambda x: x.astype('float32'))
    dataset.Validation['features'] = dataset.Validation['features'].apply(lambda x: x.astype('float32'))
    if verbose == 1:
        print ("Sample from Dataset:/n", dataset.Test['features'].iloc[10])
        print ("Total Counts: %10.2f" % np.sum(dataset.Test['features'].iloc[10]))

# LigityScore 1D features are either 'float64'. Change to 'float'
if dataset.Test['features'].iloc[0].dtype == 'float64':
    #print (dataset.Test['features'].iloc[0])
    dataset.Train['features'] = dataset.Train['features'].apply(lambda x: x.astype('float32'))
    dataset.Test['features'] = dataset.Test['features'].apply(lambda x: x.astype('float32'))
    dataset.Test2013['features'] = dataset.Test2013['features'].apply(lambda x: x.astype('float32'))
    dataset.Validation['features'] = dataset.Validation['features'].apply(lambda x: x.astype('float32'))


# Get size of Gridresults
if args.LigDimension == 1:
    d = PDBdatasetLigity1D (dataset.Validation, angle=0, R=R)
elif args.LigDimension == 3:
    d = PDBdatasetLigity (dataset.Validation, newShape=newShape, convModule=convModuleDim)
sample = d[0]
gridSize = list(sample[0].shape[0:]) 
print ("Grid Size is: %s \n" % gridSize)


#Initialize Network
# -------------------------------

net = myNet(in_channels, gridSize, convModuleDim, conv_layers, conv_dim, convKernelSize, convStride, convPad, 
            activation, activation_fn, pooling, maxPoolWindow, maxPoolStride, maxPoolPad, 
            fc_layers, fc_dim, dropout, dropoutProb, normalizationFn, normalizationList, convDropoutList, convDropout )


modelSavePath = os.path.join(args.model_dir, args.model_file)
print ("Path to Saved Model: %s" % modelSavePath)

net.load_state_dict(torch.load(modelSavePath))
net.to(device)
print ("\n*********** CNN Network Definition:\n")
print ("\n\n", net)

criterionMSE = nn.MSELoss()   
criterionMAE = nn.L1Loss() 

# Predictions Routine 
# -------------------------------
all_test_metrics = predictResults()

for i, Set in enumerate(splitSets):
    print ("\n*********** %s" % splitSets[Set])
    if args.LigDimension == 1:
        data = PDBdatasetLigity1D (eval("dataset.%s" % splitSets[Set] ), angle=0, R=R) 
    elif args.LigDimension == 3:
        data = eval("PDBdatasetLigity (dataset.%s, newShape=newShape, convModule=convModuleDim)" % splitSets[Set] )
    exec (("dataloader%s = DataLoader(data, batch_size=batchSize, shuffle=True, num_workers=args.dataWorkers)" % (splitSets[Set]) )) #Load Data using pytorch dataloader
    exec ("dataset.%s['predAffinity'] = None" % splitSets[Set]) # add empty predAffinity column
    
    print ("\nPredicting %s dataset with in %d batches:\n" % (splitSets[Set], len(eval("dataloader%s" % splitSets[Set] ))))
    

    test_metrics = evalMetrics() 

    with torch.no_grad(): # disable autograd during evaluation for this block only.
        
        net = net.eval()  # switch model to Evaluation Mode

        for i, batch in enumerate(eval("dataloader%s" % splitSets[Set] ), 0):

            # get the inputs; data is a list of [inputs, labels, pdbCode]
            batch[1] = [(lambda x: float(x))(x) for x in batch[1]]
            batch[1] = torch.tensor(batch[1])
            batch[1] = batch[1].view(-1, 1)  # convert targets to match output of CNN
            if verbose == 1:
                print ("Target Shape: %15s" % str(batch[1].shape))

            # Prepare Inputs to CNN and real affinity values
            test_inputs, test_real_aff = batch[0].to(device), batch[1].to(device)

            # Get pdbCodes
            test_pdbid = batch[2]

            # Predict by passing FF through network
            test_pred_aff = net(test_inputs)

            # Compute Loss - Defines as MSE, add .sqrt fnc to convert to RMSE
            test_loss_RMSE = torch.sqrt(criterionMSE(test_pred_aff, test_real_aff))
            test_loss_MAE = criterionMAE(test_pred_aff, test_real_aff)
            
            # Compute Running Loss to Return
            test_metrics.rmse += test_loss_RMSE.item()
            test_metrics.mae  += test_loss_MAE.item()                  

            # Update Dataset Under 'test'
            if verbose == 1:
                print ("CNN Prediction Output:")

            for pdbid, real, pred in zip(test_pdbid, test_real_aff, test_pred_aff):
                if verbose == 1:
                    print ("PDB Code: %7s \t Real Affinity: %10.3f \t Predicted Affinity: %10.3f" % (pdbid, real.item(), pred.item()))
                exec("dataset.%s['predAffinity'][dataset.%s['pdbCode'] == pdbid] = pred.item()" % (splitSets[Set], splitSets[Set]) )

            if i == -1:
                break
    
    exec("dataset.%s['realAffinity'] = [(lambda x: float(x))(x) for x in dataset.%s['realAffinity']]" % (splitSets[Set], splitSets[Set]))
    exec("dataset.%s['predAffinity'] = [(lambda x: float(x))(x) for x in dataset.%s['predAffinity']]" % (splitSets[Set], splitSets[Set]))

    exec("affs%s = dataset.%s[['realAffinity','predAffinity']]" % (splitSets[Set], splitSets[Set]))
    exec("pearsoncorr%s = affs%s.corr(method='pearson')" % (splitSets[Set], splitSets[Set]))
    print(eval("pearsoncorr%s.head()" % splitSets[Set] ))
    test_metrics.R = eval( "pearsoncorr%s.iloc[0,1]" % splitSets[Set] )

    # Compute SD - Standard Deviation in Regression
    y =  eval("dataset.%s['realAffinity']" % (splitSets[Set]) )
    lr = LinearRegression()
    #exec("print(dataset.%s['predAffinity'].to_numpy())" % splitSets[Set])
    exec("lr.fit( dataset.%s['predAffinity'].to_numpy().reshape(-1, 1), dataset.%s['realAffinity'].to_numpy())" % (splitSets[Set], splitSets[Set]) )
    y_ = eval("lr.predict( dataset.%s['predAffinity'].to_numpy().reshape(-1, 1) )" % (splitSets[Set]) )
    
    N = eval("len(dataset.%s['realAffinity'])" % (splitSets[Set]) )

    test_metrics.sd = (np.sum((y - y_)**2) / (N - 1)) ** 0.5

    print ("\n%s RMSE: %10.3f" % (splitSets[Set], test_metrics.rmse / ( eval("len(dataset.%s)" % (splitSets[Set]))/batchSize) ))
    print ("%s MAE:  %10.3f" % (splitSets[Set], test_metrics.mae / ( eval("len(dataset.%s)" % (splitSets[Set]))/batchSize) ))
    print ("%s R:    %10.3f" % (splitSets[Set], test_metrics.R))
    print ("%s SD:   %10.3f\n" % (splitSets[Set], test_metrics.sd))



    # Store Results of Dataset in class to return to master script
    exec ("all_test_metrics.RMSE_%s = test_metrics.rmse / (len(dataset.%s)/batchSize)" % (Set, splitSets[Set] ) )
    exec ("all_test_metrics.MAE_%s = test_metrics.mae / (len(dataset.%s)/batchSize)" % (Set, splitSets[Set] ) )
    exec ("all_test_metrics.R_%s = test_metrics.R" % Set )
    exec ("all_test_metrics.STD_%s = test_metrics.sd" % Set )

# TODO - push output files to S3




# SNS Visualisations
# -------------------------------

print ("\n\n*********** Save Visualisation Plots\n")

axes_dict = {0:(0,0), 1:(0,1), 2:(1,0), 3:(1,1)}
splitSets   = {"tr":"Train", "v":"Validation", "t":"Test", "t3":"Test2013"}
# Real Affinity Distribution
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(22,14))

for i, Set in enumerate(splitSets):             
    #print (splitSets[Set]) 
    plot_real = sns.distplot(eval("dataset.%s['realAffinity']" % splitSets[Set]), ax=axes[axes_dict[i][0], axes_dict[i][1]], color=colourSets[Set] )
    plot_real.set(xlabel='Real Affinity', ylabel='Density', title='%s Real Affinity Distribution' % splitSets[Set] )
    plotName = 'real-affinity.png'

savePlot (plot_real, plotName, args.model_dir )

# Predicted Affinity Distribution
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(22,14))
                
for i, Set in enumerate(splitSets):             
    plot_real = sns.distplot(eval("dataset.%s['predAffinity']" % splitSets[Set]), ax=axes[axes_dict[i][0], axes_dict[i][1]], color=colourSets[Set] )
    plot_real.set(xlabel='Pred Affinity', ylabel='Density', title='%s Predicted Affinity Distribution' % splitSets[Set] )
    plotName = 'predicted-affinity.png'

savePlot (plot_real, plotName, args.model_dir )

# Predicted Affinity Vs Real Affinity
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(22,14))
                
for i, Set in enumerate(splitSets):             
    plot_real = sns.scatterplot(x="predAffinity", y="realAffinity", hue=eval("dataset.%s['pdbSplit']" % splitSets[Set]), data=eval("dataset.%s[['realAffinity','predAffinity']]" % splitSets[Set]), ax=axes[axes_dict[i][0], axes_dict[i][1]], color=colourSets[Set] )
    plotName = 'RealVsPredicted.png'

savePlot (plot_real, plotName, args.model_dir )

print ("Test results summary: \n\n ", all_test_metrics.__dict__)


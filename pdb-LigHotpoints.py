"""
# Course: MSc AI
# Date: 02/02/2020
# Author: Joseph Azzopardi
# Credits:
# File: sbvsLigHotpoints.py
# Version: 1.0
# Description: Program to determine the LigityScore Hotpoints in a PL complex. The program extract all the LigityScore Hotpoints from the PDBBind dataset
               and save them in a dataframe.
"""

import math
import os
import re
import sys
import argparse
import rdkit

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

from rdkit      import Chem
from rdkit      import RDConfig
from rdkit.Chem import Lipinski
from rdkit.Chem import ChemicalFeatures
from rdkit.Chem import Crippen
from rdkit.Chem import Descriptors
from rdkit.Chem import rdmolfiles

from sbvscnnmsc.sbvsHelper import loadDataFrame
from sbvscnnmsc.sbvsHelper import saveDataFrame

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


# Results Commands Final
# ----2016
#python3 pdb-LigHotpoints.py --input_filename './PDB2016dataframe' --it -1 --verbose 1 --output_filename_Hot "PDB2016Hotpoints1-0"  --pip_threshold 1.0 2>&1 | tee  msc-results-hotpoints-console-output-threshold-1-0-2016.txt 
#python3 pdb-LigHotpoints.py --input_filename './PDB2016dataframe' --it -1 --verbose 1 --output_filename_Hot "PDB2016Hotpoints1-1"  --pip_threshold 1.1 2>&1 | tee  msc-results-hotpoints-console-output-threshold-1-1-2016.txt 
#python3 pdb-LigHotpoints.py --input_filename './PDB2016dataframe' --it -1 --verbose 1 --output_filename_Hot "PDB2016Hotpoints1-25" --pip_threshold 1.25 2>&1 | tee msc-results-hotpoints-console-output-threshold-1-25-2016.txt 
#python3 pdb-LigHotpoints.py --input_filename './PDB2016dataframe' --it -1 --verbose 1 --output_filename_Hot "PDB2016Hotpoints1-4"  --pip_threshold 1.4 2>&1 | tee  msc-results-hotpoints-console-output-threshold-1-4-2016.txt 
#python3 pdb-LigHotpoints.py --input_filename './PDB2016dataframe' --it -1 --verbose 1 --output_filename_Hot "PDB2016Hotpoints1-5"  --pip_threshold 1.5 2>&1 | tee  msc-results-hotpoints-console-output-threshold-1-5-2016.txt 
#python3 pdb-LigHotpoints.py --input_filename './PDB2016dataframe' --it -1 --verbose 1 --output_filename_Hot "PDB2016Hotpoints1-6"  --pip_threshold 1.6 2>&1 | tee  msc-results-hotpoints-console-output-threshold-1-6-2016.txt 

#python3 pdb-LigHotpoints.py --input_filename './PDB2016dataframe' --it -1 --verbose 1 --output_filename_Hot "PDB2016Hotpoints1-4L" --pip_threshold 1.4 --lipinski 2>&1 | tee hotpoints-console-output-threshold-1-4L-2016.txt 
#python3 pdb-LigHotpoints.py --input_filename './PDB2016dataframe' --it -1 --verbose 1 --output_filename_Hot "PDB2016Hotpoints1-5L" --pip_threshold 1.5 --lipinski 2>&1 | tee hotpoints-console-output-threshold-1-5L-2016.txt 
# ----2018
#python3 pdb-LigHotpoints.py --input_filename './PDB2018dataframe' --it -1 --verbose 1 --output_filename_Hot "PDB2018Hotpoints1-0"  --pip_threshold 1.0  2>&1 | tee  msc-results-hotpoints-console-output-threshold-1-0-2018.txt 
#python3 pdb-LigHotpoints.py --input_filename './PDB2018dataframe' --it -1 --verbose 1 --output_filename_Hot "PDB2018Hotpoints1-1"  --pip_threshold 1.1  2>&1 | tee  msc-results-hotpoints-console-output-threshold-1-1-2018.txt 
#python3 pdb-LigHotpoints.py --input_filename './PDB2018dataframe' --it -1 --verbose 1 --output_filename_Hot "PDB2018Hotpoints1-25" --pip_threshold 1.25 2>&1 | tee msc-results-hotpoints-console-output-threshold-1-25-2018.txt 
#python3 pdb-LigHotpoints.py --input_filename './PDB2018dataframe' --it -1 --verbose 1 --output_filename_Hot "PDB2018Hotpoints1-4"  --pip_threshold 1.4  2>&1 | tee  msc-results-hotpoints-console-output-threshold-1-4-2018.txt 
#python3 pdb-LigHotpoints.py --input_filename './PDB2018dataframe' --it -1 --verbose 1 --output_filename_Hot "PDB2018Hotpoints1-5"  --pip_threshold 1.5  2>&1 | tee  msc-results-hotpoints-console-output-threshold-1-5-2018.txt 
#python3 pdb-LigHotpoints.py --input_filename './PDB2018dataframe' --it -1 --verbose 1 --output_filename_Hot "PDB2018Hotpoints1-6"  --pip_threshold 1.6  2>&1 | tee  msc-results-hotpoints-console-output-threshold-1-6-2018.txt

#python3 pdb-LigHotpoints.py --input_filename './PDB2018dataframe' --it -1 --verbose 1 --output_filename_Hot "PDB2018Hotpoints1-4L" --pip_threshold 1.4 --lipinski 2>&1 | tee msc-results-hotpoints-console-output-threshold-1-4L-2018.txt 
#python3 pdb-LigHotpoints.py --input_filename './PDB2018dataframe' --it -1 --verbose 1 --output_filename_Hot "PDB2018Hotpoints1-5L" --pip_threshold 1.5 --lipinski 2>&1 | tee msc-results-hotpoints-console-output-threshold-1-5L-2018.txt 
# ----


# Script Arguments
# -------------------------------
parser = argparse.ArgumentParser(description='LigityScore Hotpoint Extraction',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

io_group = parser.add_argument_group('Input & Output Files-Directories')
io_group.add_argument('--input_dir',  '-i', default='./LigityScore_Output',
                      help='Directory for location of PDBdataframe file obtained from data pre-processing part.')
io_group.add_argument('--input_filename',  '-iF', default='./PDBdataframe',
                      help='Directory for location of PDBdataframe file obtained from data pre-processing part.')                     
io_group.add_argument('--output_dir', '-o', default='./LigityScore_Output',
                      help='Directory to store LigityScore Hotpoint Dataframe')
io_group.add_argument('--output_filename_Hot', '-oFH', default='PDB2016Hotpoints',
                      help='Filename for saved dataframe.')
io_group.add_argument('--output_filename_Lip', '-oFL', default='PDBdataframeLipinski',
                      help='Filename for saved dataframe.')


feature_group = parser.add_argument_group('Features to Extract Hotpoints')
feature_group.add_argument('--pip_threshold',  '-pT', type=float, default=1.0,
                           help='Factor to change the LigityScore Query threshold.')
feature_group.add_argument('--lipinski', action='store_true', default=False,
                           help='Boolean to enable/disable Lipinski Rule of 5 Filtering.')

runtime_group = parser.add_argument_group('Runtime Variables')
runtime_group.add_argument('--iterations', '-it', default=-1, type=int,
                            help='Number of PDBCodes to process. To be used whilst debugging.')
runtime_group.add_argument('--verbose', '-v', default=0, type=int,
                            help='Enable Script Print output.')

args = parser.parse_args()


## TO DO
## Check if files exist


# Environment Variables
# -------------------------------

print (args.iterations)

pd.set_option('display.max_colwidth', 120)

colourSets  = {"HA":"g", "HD":"b", "LogP":"r", "MolW":"y"}
sns.set_palette("deep")
sns.set_style("whitegrid") #sns.set_style("dark")
sns.set_context("poster", font_scale = 0.6, rc={"grid.linewidth": 0.8, "axes.titlesize": 14, 'lines.markeredgewidth': 1.0})


# Variable Definition - General
# --------------------------------------------------

verbose = args.verbose


# Variable Definition - Extract Lipinski Featuress
# --------------------------------------------------
i = 0

P_noneCount = 0
l_noneCount = 0

Reject_Null_l = []
Reject_Null_P = []
Reject_Lip  = []
P_incorrect = []
l_incorrect = []

P_suff = "_pocket.pdb"
l_suff = "_ligand.mol2"

l_HA = []          # List for Hydrogen Acceptor counts
l_HD = []          # List for Hydrogen Donor counts
l_LogP = []        # List for LogP values
l_MolW = []        # List for Molecular Weight values

l_HA_lip = []      # List for Hydrogen Acceptor counts that follow Lipinski RuleOf5
l_HD_lip = []      # List for Hydrogen Donor counts that follow Lipinski RuleOf5
l_LogP_lip = []    # List for LogP values that follow Lipinski RuleOf5
l_MolW_lip = []    # List for Molecular Weight values that follow Lipinski RuleOf5

P_HA = []          # List for Hydrogen Acceptor counts
P_HD = []          # List for Hydrogen Donor counts
P_LogP = []        # List for LogP values
P_MolW = []        # List for Molecular Weight values


# Define Lipinski Rules
HAcceptor = 10 #12
HDonor = 5 #7
LogP = 5 #7
Mol_Weight_Max = 500 #600
Mol_Weight_Min = 0


# Analyze PDB files and build dataframe
# --------------------------------------------------

def createDF_1():

    dropColNames = ["dataAvail", "features", "split"]
    addColNames  = ["Reject_Null_l", "Reject_Null_P", "Reject_Lip"]

    # import dataframe
    df = loadDataFrame(args.input_dir, args.input_filename)

    # drop extra column
    df = df.drop(dropColNames, axis = 1) 

    # add new columns fore preprocessing
    df = pd.concat([df, pd.DataFrame(columns=addColNames)])

    if verbose == 1:
        print(df.head(4), "\n\n")

    return df


# Extract Features from RDKit
# --------------------------------------------------

def extractLipinski_2(df):
    """
    Function to process all ligand files in PDB and add Lipinski features to the Dataframe. 

    Arguments:
        df (DataFrame): Initial dataframe containing PDBbind complexes' information. This is generated from pdb-file-preprocess.py script.
    return:
        df (DataFrame): modified DataFrame containing Lipinski infromation for each Ligand (# of Donor, # of Acceptors, molWeight, and logP.)
    """


    # iterate over the dataframe
    i = 0
    global l_noneCount
    global P_noneCount

    print ("Extract Lipinksi Rule of 5 Features from compiled dataframe  ....\n")

    for index, row in df.iterrows():
        
        R_Null_l = False
        R_Null_P = False
        R_Lip  = True
        
        # get molecule paths
        P, pl_path = row[0], row[6]
        P_file = os.path.join(pl_path, P+P_suff)
        l_file = os.path.join(pl_path, P+l_suff)
        if verbose:
            print ("\nProtein File: %s \nLigand File: %s" % (P_file, l_file))
        
        
        # load molecule file - MOL
        P_mol = Chem.rdmolfiles.MolFromPDBFile(P_file)
        #P_mol = Chem.rdmolfiles.MolFromMol2File(P_file)
        l_mol = Chem.rdmolfiles.MolFromMol2File(molFileName=l_file)

        #suppl = Chem.SDMolSupplier(l_file)         # to use sdf files instead of mol2
        #l_mol = next(suppl)

        # Check if ligand molecule was loaded correctly. If not try with SDF file type
        if l_mol is None:
            if verbose:
                print ("Molecule in %s format is not valide. Trying with SDF format ... " % l_suff)
            l_file = os.path.join(pl_path, P+"_ligand.sdf")
            
            suppl = Chem.SDMolSupplier(l_file)
            for mol in suppl:
                l_mol = mol
            if l_mol is not None:
                print ("Molecule successfully loaded using SDF.")
        
        if P_mol is None:
            #raise ValueError('Invalid Molecule. Could not load Protein at %s.\n' % P_file)
            P_incorrect.append(P)
            P_noneCount += 1
            R_Null_P = True
            R_Lip = "NA"
            l_HA_count = None
            l_HD_count = None
            l_MolW_value = None
            l_LogP_value = None
            
        elif l_mol is None:
            #raise ValueError('Invalid Molecule. Could not load ligand at %s.\n' % l_file)
            l_incorrect.append(P)
            l_noneCount += 1
            R_Null_l = True
            R_Lip = "NA"
            l_HA_count = None
            l_HD_count = None
            l_MolW_value = None
            l_LogP_value = None
            
        else:
            # get ligand properties
            l_HA_count = Lipinski.NumHAcceptors(l_mol)          # Get Hydrogen Acceptor count
            l_HD_count = Lipinski.NumHDonors(l_mol)             # Get Hydrogen Donor count
            l_MolW_value = Chem.Descriptors.ExactMolWt(l_mol)   # Get LogP value
            l_LogP_value = Chem.Crippen.MolLogP(l_mol)          # Get Molecule weight value
            
            if verbose:
                print ("Ligand HA Count: %10d" % l_HA_count)
                print ("Ligand HD Count: %10d" % l_HD_count)
                print ("Ligand Mol Weight: %9.2f" % l_MolW_value)
                print ("Ligand LogP: %14.2f" %l_LogP_value)
                
            if l_HA_count < HAcceptor and l_HD_count < HDonor and l_MolW_value < Mol_Weight_Max \
            and l_MolW_value > Mol_Weight_Min and l_LogP_value < LogP:   # select ligand on custom cutoff.
                R_Lip = False
                l_HA_lip.append(l_HA_count)          # Get Hydrogen Acceptor count
                l_HD_lip.append(l_HD_count)          # Get Hydrogen Donor count
                l_LogP_lip.append(l_LogP_value)      # Get LogP value
                l_MolW_lip.append(l_MolW_value)      # Get Molecule weight value
        
        # append values to Lists
        Reject_Null_l.append(R_Null_l)       
        Reject_Null_P.append(R_Null_P)
        Reject_Lip.append(R_Lip)
        l_HA.append(l_HA_count)          
        l_HD.append(l_HD_count)          
        l_LogP.append(l_LogP_value)      
        l_MolW.append(l_MolW_value)      
        
        if verbose:
            print ("Ligand Reject: %12s" % R_Null_l )
            print ("Protein Reject: %11s" % R_Null_P )
            print ("Lipinski Reject: %10s" % R_Lip )
            print ("\n")

        i += 1
    
    df['Reject_Null_l'] = Reject_Null_l
    df['Reject_Null_P'] = Reject_Null_P
    df['Reject_Lip']    = Reject_Lip
    df['HA_l']     = l_HA
    df['HD_l']     = l_HD
    df['Weight_l'] = l_MolW
    df['LogP_l']   = l_LogP

    # Print Molecure with incorrect RDkit format
    print ("Protein Molecules with INCORRECT format: %6d / %d" % (P_noneCount, i) )
    print ("Ligand  Molecules with INCORRECT format: %6d / %d\n\n" % (l_noneCount, i) )

    # check which Ligand Protein Sets that overlap
    P_L_inter = list(set(P_incorrect) & set(l_incorrect))
    P_unique  = list(set(P_incorrect) - set(P_L_inter))
    l_unique  = list(set(l_incorrect) - set(P_L_inter))

    print ("Number of incorrect molecules of same PL-complex is: %29d" % len(P_L_inter))
    print ("Number of incorrect molecules of where only Protein has incorrect format: %8d" % len(P_unique))
    print ("Number of incorrect molecules of where only ligand has incorrect format:  %8d\n\n" % len(l_unique))


    # Plot histograms for Ligand features
    
    try:
        featureNames   = {"HA":"l_HA", "HD":"l_HD", "LogP":"l_LogP", "MolW":"l_MolW"}

        fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(22,15))
        axes = axes.flatten()

        for i, f in enumerate(featureNames):             
            #print (f, i)
            #print((eval(featureNames[f])[0]))
            plot_real = sns.distplot((eval(featureNames[f])), ax=axes[i % 4], color=colourSets[f]  )
            plot_real.set(xlabel='%s'% featureNames[f], ylabel='Density', title='%s Ligand Distribution' % featureNames[f] )
        

        fig.savefig(os.path.join(args.output_dir, "PDB_Lipinski_Distributions_Before_Filtering.png"))

    except:
        print("Error: Failed to Save plot")

    else:
        print("Plot saved at %s" % os.path.join(args.output_dir, "PDB_Lipinski_Distributions_Before_Filtering.png"))


    return df


# Filter Ligand by Lipinski Rule of 5
# --------------------------------------------------

def filterLipinski_3(df):

    """
    Function to filter out molecules that exceed the Lipinski Thresholds.
    arguments:
        df (DataFrame): DataFrame with PDBbind details
    returns:
        df (DataFrame): Filtered dataframe.
    """
    
    print("Filter PDBBind Dataset by Limpinski Rule of 5.\n")

    # Check Number of Ligands that do not conform with Lipinski Rules
    print ("Number of Ligands that do not conform with Lipinski Rules Summary:\n")
    print(df.groupby('Reject_Lip').count())

    # Remove 'True' and 'None' rows under 'Reject_Lip'
    df_Lip = df[df["Reject_Lip"] == False]  
    df_Lip.head()
    print (len(df_Lip['pdbCode']))

    return df_Lip



# RDKit Feature Factory 
# --------------------------------------------------

def eucDist(a, b):
    a = np.asarray(a)
    b = np.asarray(b)
    d = (a-b)**2
    d = (sum(d))**0.5
    return d

def getCoord(molecule):
    
    #numAtoms = molecule.GetNumAtoms(onlyExplicit=False) # returns count including hydrogens
    #print ((numAtoms))
    
    #n = molecule.GetConformer().GetNumAtoms()
    #print ((n))
    
    # get conformer excludes Hydrogen atoms
    coords = np.asarray(molecule.GetConformer().GetPositions())
    #print(coords)

    return coords



# --------------------------------------------------

def ligityHotpoints_4(df, l_suff, P_suff):

    """
    Function to extract LigityScore Hotpoints. Loads each molecules in RDkit and uses the RDKit Feature Factory to extract pharmacophoric features from the
    protein and ligand. Combinations of these pharmacophoric features are checked against a distance thresholds, and allowed family combination

    Parameters:
        df (DataFrame): DataFrame containing all the available complexes.
        l_suff (string): Ligand file suffix to use (.mol2)
        P_suff (string): Protein file suffix to use (.pdb)
    Returns:

    PDBfeaturePairs: Pairs of ligand-protein Hotspots that are conformintant with allowed family combinations and distance thresholds.
    """

    print ("\n\nCompiling LigityScore Hotpoints ...\n")

    i = 0
    PL_noneCount = 0
    no_hotpointCount = 0 
    l_mol2toSDFCount = 0

    allowed_family = ["LumpedHydrophobe", "Hydrophobe", "Acceptor", "Donor", "PosIonizable", "NegIonizable", "Aromatic"]

    # define Ligity thresholds
    max_dist_HH = 4.5 * args.pip_threshold
    max_dist_AD = 3.9 * args.pip_threshold
    max_dist_CA = 4.0 * args.pip_threshold
    max_dist_ArAr = 4.5 * args.pip_threshold
    max_dist_CAr  = 4.0 * args.pip_threshold

    # define Dataframes
    PDBfeaturePairs = pd.DataFrame()

    #feats = []  
    fdefName = os.path.join(RDConfig.RDDataDir, 'BaseFeatures.fdef')
    factory = ChemicalFeatures.BuildFeatureFactory(fdefName)

    #print (df.head(1))

    # iterate over the dataframe
    for index, row in df.iterrows():
        
        d_pairs = []
        
        # define Dataframes
        l_family_conf = pd.DataFrame(columns=["l_PDBCode", "l_atom", "l_family", "l_multiAtom", "l_CoF", "l_fPos"])
        P_family_conf = pd.DataFrame(columns=["P_PDBCode", "P_atom", "P_family", "P_multiAtom", "P_CoF", "P_fPos"])
        PL_family_conf = pd.DataFrame()
        
        
        # get molecule paths
        P, pl_path = row[0], row[6]
        #P = "4ty8"                                                                            # Uncomment to find hotpoint of a particular PDBCode
        #pl_path = "./pdbbind/v2016/data/general-set-except-refined/4ty8/"
        P_file = os.path.join(pl_path, P+P_suff)
        l_file = os.path.join(pl_path, P+l_suff)
        if verbose:
            print ("Protein File:  %s \nLigand File:   %s" % (P_file, l_file))
        
        #suppl = Chem.SDMolSupplier(l_file)
        #for mol in suppl:
        #    l_mol = mol
        
        # load molecule file - MOL
        P_mol = Chem.rdmolfiles.MolFromPDBFile(P_file)
        #P_mol = Chem.rdmolfiles.MolFromMol2File(P_file)
        l_mol = Chem.rdmolfiles.MolFromMol2File(molFileName=l_file)

        # Check if ligand molecule was loaded correctly. If not try with SDF file type
        if l_mol is None:
            if verbose:
                print ("Molecule in %s format is not valid. Trying with SDF format ... " % l_suff)
            l_file = os.path.join(pl_path, P+"_ligand.sdf")
            
            suppl = Chem.SDMolSupplier(l_file)
            for mol in suppl:
                l_mol = mol
            if l_mol is not None:
                print ("Molecule successfully loaded using SDF.")
                l_mol2toSDFCount += 1

        # Get factory Features
        if l_mol is None or P_mol is None: 
            if verbose:
                print ("Molecule %s is not invalid." % P)
            PL_noneCount += 1
            if verbose:
                print ("Iteration: %9d\%d  \tHotpoints: %8d" % (i+1, len(df), len(PDBfeaturePairs['l_family'])))
                print ("Null Count: %7d" % PL_noneCount)
                print ("SDF Saved: %9d" % l_mol2toSDFCount)
                print ("No Hotpoints: %6d\n" % no_hotpointCount)
                
            i += 1
            continue             # skip iteration since either l_mol or P_mol are None
            #raise ValueError('Invalid Molecules. Could not load ligand at %s.\n' % l_file)
        else:
            # get ligand properties
            l_feats = factory.GetFeaturesForMol(l_mol)
            # get ligand properties
            P_feats = factory.GetFeaturesForMol(P_mol)
            
        
        # Find geometric centre of Ligand
        #l_coords = getCoord (l_mol) # Hydrogen Atoms are not included
        #centroid = l_coords.mean(axis=0)   

        # Filter Ligand Features by Family 
        # {LumpedHydrophobe, Hydrophobe, Acceptor, Donor, PosIonizable, NegIonizable, Aromatic }
        for f in (l_feats):
            family  = str(f.GetFamily())
            if family in allowed_family:
                # Check if feature has more than one atom
                mAtoms = False
                if len(f.GetAtomIds()) > 1:
                    # !! find centre of feature
                    mAtoms = True
                #l_fPos = np.asarray(list(f.GetPos()))
                l_fPos = tuple(f.GetPos())
                # Append atoms in df that match family
                l_family_conf = l_family_conf.append({'l_PDBCode' : P , "l_atom": f, "l_family": family, 
                                                    "l_multiAtom" : mAtoms, "l_fPos": l_fPos } , 
                                                    ignore_index=True)
                
        
        # Filter Protein Features by Family 
        # {LumpedHydrophobe Hydrophobe, Acceptor, Donor, PosIonizable, NegIonizable, Aromatic }   
        for f in (P_feats):
            family  = str(f.GetFamily())
            if family in allowed_family:
                mAtoms = False
                # Check if feature has more than one atom
                if len(f.GetAtomIds()) > 1:
                    mAtoms = True
                    # !! find centre of feature
                P_fPos = tuple(f.GetPos())
                # Append atoms in df that match family
                P_family_conf = P_family_conf.append({"P_PDBCode" : P , "P_atom": f, "P_family": family, 
                                                    "P_multiAtom" : mAtoms, "P_fPos": P_fPos } , 
                                                    ignore_index=True)

        
        l_family_conf['key'] = 0
        P_family_conf['key'] = 0
        
        # cross join dataframes and filter rows
        # group pairs of conformant families
        #PL_family_conf = l_family_conf.merge(P_family_conf, left_on='l_family', right_on="P_family", how='outer')
        PL_family_conf = l_family_conf.merge(P_family_conf, how='outer')
        
        # Drop extra Columns after Merge
        PL_family_conf = PL_family_conf.drop(columns=['key'])
            
            
        # Filter Protein-Ligand Familty Pair.
        # {{Hydrophobe, Hydrophobe}, {Hydrophobe, LumpedHydrophobe}, {LumpedHydrophobe, Hydrophobe}, {LumpedHydrophobe, LumpedHydrophobe}
        #  {acceptor, donor}, 
        #  {cation, anion}, 
        #  {aromatic, aromatic}, 
        #  {cation, aromatic}}
        
        # Finding the matching pairs. Negate to mark the non-matching pairs and drop them.
        indexDrop = PL_family_conf[~(
                                ((PL_family_conf['l_family'] == "Hydrophobe") & (PL_family_conf['P_family'] == "Hydrophobe"))   |
                                ((PL_family_conf['l_family'] == "LumpedHydrophobe") & (PL_family_conf['P_family'] == "Hydrophobe"))   |
                                ((PL_family_conf['l_family'] == "Hydrophobe") & (PL_family_conf['P_family'] == "LumpedHydrophobe"))   |
                                ((PL_family_conf['l_family'] == "LumpedHydrophobe") & (PL_family_conf['P_family'] == "LumpedHydrophobe"))   |
                                ((PL_family_conf['l_family'] == "Acceptor") & (PL_family_conf['P_family'] == "Donor"))            | 
                                ((PL_family_conf['l_family'] == "Donor") & (PL_family_conf['P_family'] == "Acceptor"))            |
                                ((PL_family_conf['l_family'] == "PosIonizable") & (PL_family_conf['P_family'] == "NegIonizable")) | 
                                ((PL_family_conf['l_family'] == "NegIonizable") & (PL_family_conf['P_family'] == "PosIonizable")) | 
                                ((PL_family_conf['l_family'] == "Aromatic") & (PL_family_conf['P_family'] == "Aromatic"))         | 
                                ((PL_family_conf['l_family'] == "PosIonizable") & (PL_family_conf['P_family'] == "Aromatic"))     | 
                                ((PL_family_conf['l_family'] == "Aromatic") & (PL_family_conf['P_family'] == "PosIonizable"))              
                            )].index
        PL_family_conf.drop(indexDrop  , inplace=True)
        #print (len(PL_family_conf['l_family']))
        
        
        #PDBfeaturePairs = PDBfeaturePairs.append(PL_family_conf, ignore_index = True)
        PL_family_conf["PLfeatureDist"] = None

        # Compare distances between Ligand and Protein
        
        for index, row in PL_family_conf.iterrows():
            # compute Euclidean Distance
            l_fPos = row[5]
            P_fPos = row[11]
            d = eucDist(l_fPos, P_fPos)
            d_pairs.append(d)
        
        PL_family_conf["PLfeatureDist"] = d_pairs
        
        #print ("+++++++++++++\n\n", d_pairs)       #Uncomment to check issue that Pl complex has no hotpoints
        #print (PL_family_conf.head())

        # Filter P-L pair by distance
        
        indexDrop = PL_family_conf[(
                                ((PL_family_conf['l_family'] == "Hydrophobe") & (PL_family_conf['P_family'] == "Hydrophobe") & (PL_family_conf['PLfeatureDist'] >  max_dist_HH))   |
                                ((PL_family_conf['l_family'] == "LumpedHydrophobe") & (PL_family_conf['P_family'] == "Hydrophobe") & (PL_family_conf['PLfeatureDist'] >  max_dist_HH))   |
                                ((PL_family_conf['l_family'] == "Hydrophobe") & (PL_family_conf['P_family'] == "LumpedHydrophobe") & (PL_family_conf['PLfeatureDist'] >  max_dist_HH))   |
                                ((PL_family_conf['l_family'] == "LumpedHydrophobe") & (PL_family_conf['P_family'] == "LumpedHydrophobe") & (PL_family_conf['PLfeatureDist'] >  max_dist_HH))   |
                                ((PL_family_conf['l_family'] == "Acceptor") & (PL_family_conf['P_family'] == "Donor") & (PL_family_conf['PLfeatureDist'] >  max_dist_AD))            | 
                                ((PL_family_conf['l_family'] == "Donor") & (PL_family_conf['P_family'] == "Acceptor") & (PL_family_conf['PLfeatureDist'] >  max_dist_AD))            |
                                ((PL_family_conf['l_family'] == "PosIonizable") & (PL_family_conf['P_family'] == "NegIonizable") & (PL_family_conf['PLfeatureDist'] >  max_dist_CA)) | 
                                ((PL_family_conf['l_family'] == "NegIonizable") & (PL_family_conf['P_family'] == "PosIonizable") & (PL_family_conf['PLfeatureDist'] >  max_dist_CA)) | 
                                ((PL_family_conf['l_family'] == "Aromatic") & (PL_family_conf['P_family'] == "Aromatic") & (PL_family_conf['PLfeatureDist'] >  max_dist_ArAr))         | 
                                ((PL_family_conf['l_family'] == "PosIonizable") & (PL_family_conf['P_family'] == "Aromatic") & (PL_family_conf['PLfeatureDist'] >  max_dist_CAr))     | 
                                ((PL_family_conf['l_family'] == "Aromatic") & (PL_family_conf['P_family'] == "PosIonizable") & (PL_family_conf['PLfeatureDist'] >  max_dist_CAr))              
                            )].index
        PL_family_conf.drop(indexDrop  , inplace=True)
        #print (len(PL_family_conf['l_family']))                           

        if verbose:
            print ("Found %5d Hotpoints." % len(PL_family_conf['l_family']))   
            if len(PL_family_conf['l_family']) == 0:
                print ("No Hotpoint Found.")
                no_hotpointCount += 1
                                
        # Collect Conformat P-L pairs
        PDBfeaturePairs = PDBfeaturePairs.append(PL_family_conf, ignore_index = True)
        
        if verbose:
            print ("Iteration: %9d\%d  \tHotpoints: %8d" % (i+1, len(df), len(PDBfeaturePairs['l_family'])))
            print ("Null Count: %8d" % PL_noneCount)
            print ("SDF Saved: %9d" % l_mol2toSDFCount)
            print ("No Hotpoints: %6d\n" % no_hotpointCount)
            #print("Total Proteins" % len(PDBfeaturePairs['P_PDBCode'].unique()) + PL_noneCount))

        # re-initialize feature variable
        l_feats = None
        P_feats = None

        i += 1
        if i == args.iterations:
            break

    return PDBfeaturePairs 





# Main Program
# -------------------------------

print ("Program to process extract Protein-Ligand Hotpoints.\n----------------------------------------------------\n\n")

df = createDF_1()

print ("\n\nAdd Lipinski Features to Dataset:\n\n")
df = extractLipinski_2(df)

# save Dataframe
print ("\nSave DataFrame with Lipinski Features:\n", df.head(10))
saveDataFrame (df, args.output_dir, args.output_filename_Lip, c="zip")

if args.lipinski:
    print ("Filtering using Lipinski Rule of 5:")
    print ("\tHAcceptor = %d" % HAcceptor) 
    print ("\tHDonor = %d" % HDonor) 
    print ("\tLogP = %d" % LogP)
    print ("\tMol_Weight_Max = %d" % Mol_Weight_Max) 
    print ("\tMol_Weight_Min = %d \n" % Mol_Weight_Min)
    df = filterLipinski_3(df)  # Uncomment to filter dataset by Lipinski Rules

    # Plot histograms for Ligand features after being filtered by Lipinski Rule
    
    try:
        featureNames   = {"HA":"df['HA_l']", "HD":"df['HD_l']", "LogP":"df['Weight_l']", "MolW":"df['LogP_l']"}

        fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(22,15))
        axes = axes.flatten()

        for i, f in enumerate(featureNames):             
            #print (f, i)
            #print((eval(featureNames[f])[0]))
            plot_real = sns.distplot((eval(featureNames[f])), ax=axes[i % 4], color=colourSets[f]  )
            plot_real.set(xlabel='%s'% featureNames[f], ylabel='Density', title='%s Ligand Distribution' % featureNames[f] )
        

        fig.savefig(os.path.join(args.output_dir, "PDB_Lipinski_Distributions_After_Filtering.png"))

    except:
        print("Error: Failed to Save plot")

    else:
        print("Plot saved at %s" % os.path.join(args.output_dir, "PDB_Lipinski_Distributions_After_Filtering.png"))




PDBfeaturePairs = ligityHotpoints_4(df, l_suff, P_suff)
print ("\n\nHotpoint DataFrame:\n", PDBfeaturePairs.head(20))

# Drop RDKit Feature Columns from DataFrame
PDBfeaturePairs.drop(['l_atom', 'P_atom'], axis=1, inplace=True)


# save Dataframe
saveDataFrame (PDBfeaturePairs, args.output_dir, args.output_filename_Hot, c="zip")
#saveDataFrameFeather (df, args.output_dir, args.output_filename_Hot, suffix="feather")





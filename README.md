# LigityScore: Convolutional Neural Network for Binding-affinity Predictions


>Full Literature published as part of the proceeding for the conference ***BIOINFORMATICS 2021 : 12th International Conference on Bioinformatics Models, Methods and Algorithms*** can be found at [this link](https://www.scitepress.org/Papers/2021/102283/102283.pdf).


LigityScore is a CNN based scoring function that predicts the binding affinity for protein-ligand complexes. LigityScore is trained on PDBbind v2016 or v2018 and tested on the CASF-2013 and CASF-2016 scoring power benchmarks. The PDBBind dataset can be downloaded from http://www.pdbbind.org.cn/download.php, whilst CASF dataset is available at http://www.pdbbind.org.cn/casf.php.

## Media Content 

**ligityscore**. This is the main directory that contains all source code, datasets, and folders used to store the various script outputs. This folder can be used to replicate LigityScore as detailed in User Guide below. The **ligityscore** directory contains the below folders:


1. **tensoboard-runs**. This directory contains the TensorBoard data files for all the logs saved during training. These include the loss every 100 mini-batches for the training set, and the RMSE and R-score values for the training and validation sets computed after each epoch.  Each experiment directory also contains a number of CNN model checkpoints to save the model parameters for the epoch with the lowest validation RMSE error. In this folder only the best performing LigityScore experiments are included.

2. **pdbbind**. This folder should contain the v2016 and v2018 PDBbind datasets. Each PDBbind versions includes the **INDEX_general_PL.2016** INDEX files that describes the protein-ligand complex properties, and molecular file for the general and refined sets in *sdf* and *mol2* formats. The Core-2013 (*CoreSet2013.dat*) and Core-2016 (*CoreSet.dat*) files are also included with each version of PDBbind. Please download dataset from http://www.pdbbind.org.cn.

3. **sbvscnnmsc**. Contains the source files for the *sbvscnnmsc* Python package used to construct the LigityScore SF. The following Python files are included:
    - *sbvsCNN.py*. PyTorch Neural Network class definition. Used to create CNN models dynamically using different initialisation parameters.
    - *sbvsData.py*.  Used to store custom PyTorch *Dataset* classes for LigityScore1D, LigityScore3D, and Pafnucy. These are then used by the PyTorch *DataLoader* class to load the input to the CNN.
    - *sbvsHelper.py*. Used to store multiple helper classes and functions that are used across multiple LigityScore scripts.
    - *sbvsHelperPafnucy.py*. Use to store multiple helper classes and functions that are used to replicate the Pafnucy model.

4. **jupyter-notebooks**. This directory contains a number of jupyter notebooks that were used during development of LigityScore for initial testing. Some of the notebooks (*results-plots.ipynb* and *Hotpoint Features Diagram.ipynb*) were used to generate figures and plots used in this dissertation.

5. **pafnucy replication**. Contains the source code used for our Pafnucy implementation.

    
6. **LigityScore_Output**. This directory is used to store any output files produced by the LigityScore scipts. These include the output from pre-processing, HotSpots extraction, feature generation and its split into Training, Validation, Test, and Test2013 sets. This directory contains all the data files used to run the experiments listed in Tables 4.2, 4.3, and 4.4. As an example the *PDB2016Hotpoints1-4.zip* represents the HotSpots dataset using PDBbind v2016 with a PIP distance threshold of 1.4. The folders with names *Ligty1D1-4* contains the LigityScore features, 
*PDB2016-Lig-Features1D1-4.zip* extracted using *PDB2016Hotpoints1-4.zip* hotspots.  *Ligty1D1-4* also include the datasets split from *PDB2016-Lig-Features1D1-4.zip* for Training, Validation, Test, and Test2013.

7. **finalResults-batch-training**. This folder includes all the csv files used to defined the experiments of Tables 4.2, 4.3, and 4.4.

8. **conda-envs**. Contains .yml files for Anaconda Python environments used in LigityScore.

9. The root *ligityscore* directory contains all the source code for LigityScore models. The LigityScore scripts are summarised below.



## Scripts

The scripts used in LigityScore are summarised below:


1. **pdb-file-preprocess.py**. Used to extract a DataFrame containing information on each complex available in PDBbind (PDBCode, experimental binding affinity, year, ligand name etc), and to check that their corresponding molecular files are available. 

2. **pdb-LigHotpoints.py**. Program to determine the LigityScore Hotpoints in a PL complex. All the hotspots for each protein-ligand complex in PDBbind are extracted and collected in one DataFrame.

3. **pdb-LigFeatures.py**. Program to generate LigityScore Features using the HotSpots DataFrame. It is used to generate both LigityScore1D feature matrix, and the LigityScore3D 3D feature hypercube. Parameter 'LigDimension' controls which LigityScore feature to generate.

4. **pdb-split-dataset.py**. Script to split the DataFrame obtained from *pdb-LigFeatures.py* into Test, Test2013, Training, and Validation sets.

5. **pdb-training-ligityScore1D.py**. Train a CNN model based on LigtyScore1D features. Model parameters can be defined by a number of script arguments.

6. **pdb-training-ligityScore3D.py**. Train a CNN model based on LigtyScore3D features. Model parameters can be defined by a number of script arguments.

7. **pdb-predictions-ligityScore.py**. Load a previously trained LigityScore CNN Model, and run predictions for Training, Validation, CASF-2013, and CASF-2016 datasets.

8. **pdb-batch-training.py**. Training and Predictions for the LigityScore Scoring Function Experiments. Multiple experiments can be defined in a csv file to be executed sequentially. For each experiment a model is trained using *pdbtrainingligityXDfunction.py* and predictions results are calculated using *pdbpredictionsligityfunction*.  The test protein-ligand complexes in CASF-2013 and CASF-2016 are used to measure the performance of the experiment using RMSE, MAE, R, and SD metrics. The full results for each experiments are output as a csv file.

9. **pdbtrainingligity1Dfunction.py**. Used by *pdb-batch-training.py* to launch CNN training for LigityScore1D. This script is the same as *pdb-training-ligityScore1D.py* but re-written as a function.

10. **pdbtrainingligity3Dfunction.py**. Used by *pdb-batch-training.py* to launch CNN training for LigityScore3D. This script is the same as *pdb-training-ligityScore3D.py* but re-written as a function.

11. **pdbpredictionsligityfunction.py**. This script is the same as *pdb-predictions-ligityScore.py* but was re-written as a function to be used by pdb-batch-training.py. The function returns the predictions for Training, Validation, Core2016, and Core2013 sets, using RMSE, MAE, R, and SD metrics.

## User Guide

The following user guide lists the steps required to run LigityScore3D for the PDBbind v2016. In our case, the CNN training was run on a **g4dn.8xlarge** AWS EC2 instance as it requires more than 64G of RAM. For LigityScore1D **g4dn.2xlarge** instance type can be used.


1. Clone the contents of the provided *ligityscore* folder. 

2. Make *ligityscore* folder as your 'present working directory'.

    `$ cd ligityscore`

3. Unzip the v2016.zip file under the *pdbbind* folder. This contains the PDBbind v2016 dataset.

4. Create conda environments --- two environments are required. *msc-ligityscore-features* environment is required for preprocessing, PIP generation, and feature generation. Environment *msc-ligityscore-cnn* is used for CNN training and predictions. The *.yml* files for these conda environments are located in *conda-env* directory.

    `
    $ conda env create -f conda-envs/msc-ligityscore-features.yml
    $ conda env create -f conda-envs/msc-ligityscore-cnn.yml
    `

5. In the next steps we are going to 1) Process the PDBbind data, 2) Extract the LigityScore PIP Generation, 3) Generate the LigityScore Descriptors. The files generated for the experiments run in Chapter 4 by these 3 modules are provided in the *LigityScore_Output* folder. Therefore, if you do not want to generate the LigityScore features again, you can skip to **Step 11** directly to start the CNN training.

    Activate *msc-ligityscore-features* conda environment

    `
    $ conda activate msc-ligityscore-features
    `

6. **LigityScore PDBBind Preprocessing**. This step will preprocesses the PDBbind dataset and output a file named *PDB2016dataframe-example.zip* containing a DataFrame saved in compressed pickle format of all the protein-ligand complexes available and their properties.

    
    `$ python3 pdb-file-preprocess.py --input_dir="./pdbbind/v2016" --output_dir='./LigityScore_Output'--output_Filename= 'PDB2016dataframe-example' --pymolFetch 2>&1 | tee preprocess-console-output-pdb-2016-example.txt 
    `

7. **LigityScore PIP Generation**. The below command will execute the *pdb-LigHotpoints.py* to generate the PIP dataset with a PIP distance threshold factor of 1.4. The file *PDB2016Hotpoints1-4-example.zip* will be saved in the *LigityScore_Output*  folder.


    `
    $ python3 pdb-LigHotpoints.py --input_filename './PDB2016dataframe-example' --it -1 --verbose 1 output_filename_Hot "PDB2016Hotpoints1-4-example" --pip_threshold 1.4 2>&1 | tee hotpoints-console-output-threshold-1-4-2016-example.txt 
    `


8. **LigityScore Feature Generation**. The below command will execute the *pdb-LigFeatures.py* script to generate a hybpercube for each protein-ligand complex from the PIPs generated in previous step (*PDB2016Hotpoints1-4-example.zip*).

    `
    $ python3 pdb-LigFeatures.py --df_in_hotpoint_filename 
        PDB2016Hotpoints1-4-example  --input_dir_df "./LigityScore_Output" 
        --output_dir "./LigityScore_Output" --df_out_features_filename
        "PDB2016-Lig-Features3D1-4-example"  --LigDimension 3 
        2>&1 | tee ligfeatures-console-output-3D-1-4-2016.txt
    `

9. Activate *msc-ligityscore-cnn* conda environment.

    `
    $ conda activate msc-ligityscore-cnn
    `

10. Create folder named *Ligity3D1-4-example*. Move the file *PDB2016-Lig-Features3D1-4-example* to *Ligity3D1-4-example*.

     
11. **LigityScore DataSet Split**. The *pdb-split-dataset.py* script will take the LigityScore features generated in Step 3, and split it into Training, Validation, Test (Core-2016), and Test-2013 (Core-2013) sets. Four files in *./LigityScore_Output/Ligity3D1-4-example* folder will be created corresponding to these four datasets.
    

    `
    $ python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity3D1-4-example" --df_in_filname "PDB2016-Lig-Features3D1-4-example"  --output_dir "./LigityScore_Output/Ligity3D1-4-example" 2>&1 | tee dataSplit-console-output-3D-1-4-2016.txt
    `


11. Create experiment batch file (.csv) containing a list of experiments and their respective CNN parameters. File *msc-exp-LigScore3D-2016-experiment-example.csv* is provided as an example containing the all parameters to train the best LigityScore3D model. This file is located in the *finalResults-batch-training* folder.


12. **Launch Training and Prediction script**. The *pdb-batch-training.py* will launch the CNN training using the parameters defined in *msc-exp-LigScore3D-2016-experiment-example.csv*. The RMSE for the validation set is computed after every epoch and up to 10 CNN model checkpoints are saved at *tensorboard-run/exp-LS-2016-3D-example/models*, that correspond to the lowest RMSE values achieved during training. The prediction script will be executed right after training is complete, to predict binding affinity values using the CNN model checkpoint with the best RMSE error for the validation set.

    `
    #python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore3D-2016-experiment-example.csv" 2>&1 | tee experiments-console-output-example-2016-3D.txt
    `


13. Start TensorBoard and browse to its portal (http://localhost:6006) to view training progress. From another shell:

        `
        $ conda activate msc-ligityscore-cnn
        $ cd ./ligityscore
        $ tensorboard --logdir="tensorboard-runs"
        `

    To redirect your local port (6006) directly to your server's port (6006)  you can use the following ssh command.

    `
    ssh -i <path-to-your-ssh-server-cert> -N -f -L 6006:localhost:6006 <username>@<your-server-ipaddress>
    `


14. Collect results from *finalResults-batch-training* folder when training and predictions are ready. The output file is named *results-msc-exp-LigScore3D-2016-experiment-example.csv*, and includes the following results:
    
    - Experiment name, description, and best training epoch
    - Training Time
    - Results in RMSE, MAE, SD, and R for the training, validation Core-2013, and Core-2016 datasets.


"""
# Course: MSc AI
# Date: 02/02/2020
# Author: Joseph Azzopardi
# Credits:
# File: pdb-batch-training.py
# Version: 1.0
# Description: Training and Predictions for the LigityScore Scoring Function Experiments. The test protein-ligand complexes in CASF-2013 and CASF-2016 are 
#              used to measure the performance of the experiment using RMSE, MAE, R, and SD metrics. Experiment paramaters are defined in a CSV file.

"""


import pandas as pd
import numpy  as np
import re
import os
import errno
import warnings
import argparse
import gc


import pdbtrainingligity1Dfunction as train1d
import pdbtrainingligity3Dfunction as train3d
import pdbpredictionsligityfunction as predict

from sbvscnnmsc.sbvsHelper import cnnResults
from sbvscnnmsc.sbvsHelper import loadDataFrame
from sbvscnnmsc.sbvsHelper import saveDataFrame

# Scripts Commands to generate Final Results.
##1D
# ----2016
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore1D-2016-batch1-params.csv" 2>&1 | tee msc-results-experiments-console-output-batch1-2016.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore1D-2016-batch2-params.csv" 2>&1 | tee msc-results-experiments-console-output-batch2-2016.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore1D-2016-batch2b-params.csv" 2>&1 | tee msc-results-experiments-console-output-batch2b-2016.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore1D-2016-batch3-params.csv" 2>&1 | tee msc-results-experiments-console-output-batch3-2016.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore1D-2016-batch4-params.csv" 2>&1 | tee msc-results-experiments-console-output-batch4-2016.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore1D-2016-batch4b-params.csv" 2>&1 | tee msc-results-experiments-console-output-batch4b-2016.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore1D-2016-batch5-params.csv" 2>&1 | tee msc-results-experiments-console-output-batch5-2016.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore1D-2016-batch5b-params.csv" 2>&1 | tee msc-results-experiments-console-output-batch5b-2016.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore1D-2016-batch6-params.csv" 2>&1 | tee msc-results-experiments-console-output-batch6-2016.txt
# ----
# ----2018
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore1D-2018-batch1-params.csv" 2>&1 | tee msc-results-experiments-console-output-1D-batch1-2018.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore1D-2018-batch2-params.csv" 2>&1 | tee msc-results-experiments-console-output-1D-batch2-2018.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore1D-2018-batch3-params.csv" 2>&1 | tee msc-results-experiments-console-output-1D-batch3-2018.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore1D-2018-batch4-params.csv" 2>&1 | tee msc-results-experiments-console-output-1D-batch4-2018.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore1D-2018-batch4b-params.csv" 2>&1 | tee msc-results-experiments-console-output-1D-batch4b-2018.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore1D-2018-batch5-params.csv" 2>&1 | tee msc-results-experiments-console-output-1D-batch5-2018.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore1D-2018-batch6-params.csv" 2>&1 | tee msc-results-experiments-console-output-1D-batch6-2018.txt
#
#
###3D
# ----2016
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore3D-2016-batch1-params.csv" 2>&1  | tee msc-results-experiments-console-output-batch1-2016-3D.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore3D-2016-batch2-params.csv" 2>&1  | tee msc-results-experiments-console-output-batch2-2016-3D.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore3D-2016-batch3-params.csv" 2>&1  | tee msc-results-experiments-console-output-batch3-2016-3D.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore3D-2016-batch3b-params.csv" 2>&1  | tee msc-results-experiments-console-output-batch3b-2016-3D.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore3D-2016-batch4-params.csv" 2>&1  | tee msc-results-experiments-console-output-batch4-2016-3D.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore3D-2016-batch4b-params.csv" 2>&1  | tee msc-results-experiments-console-output-batch4b-2016-3D.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore3D-2016-batch5-params.csv" 2>&1  | tee msc-results-experiments-console-output-batch5-2016-3D.txt
#python3 pdb-batch-training.py --input_dir "finalResults-batch-training" --output_dir "finalResults-batch-training" --input_file "msc-exp-LigScore3D-2016-batch6-params.csv" 2>&1  | tee msc-results-experiments-console-output-batch6-2016-3D.txt
# 
#

# Script Arguments
# -------------------------------
parser = argparse.ArgumentParser(description='SBVS Affinity Prediction based on CNN.',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

io_group = parser.add_argument_group('Input & Output Files-Directories')
io_group.add_argument('--input_dir', '-i', default='./results',
                       help='Directory with CSV for training parameters.')
io_group.add_argument('--input_file', '-iF', default='batch-3.csv',
                       help='Filename of CSV file containing training parameters.')
io_group.add_argument('--output_dir', '-o', default='./results',
                       help='Output results for each experiment in the CSV entries.')

runtime_group = parser.add_argument_group('Runtime Variables')
runtime_group.add_argument('--verbose', '-v', default=1, type=int,
                            help='Enable Script Print output.')


args = parser.parse_args()


# Classes & Procedure Definition
# -------------------------------

class cnnParams:
    def __init__(self):
       self.TestName = "Sample Test"
       self.Comments = "Sample Test"
       self.LigityVersion = 1
       self.input_dir = "./LigityScore_Output/LigityScore1D"
       self.log_dir = "./tensorboard-runs/LigityScore1D"
       self.log_testname = "run-ligity-1D"+self.TestName
       self.train_set = "Train"
       self.val_set = "Validation"
       self.epoch = 140
       self.epochSave = 15
       self.checkpoints = 15
       self.epochValidate = 1
       self.earlyStop = False
       self.earlyStopFactor = 0.1
       self.min_epochs = 30
       self.batchSize = 20
       self.rotations = 1
       self.learningRate = 0.00001
       self.regl2 = 0.001
       self.dataWorkers = 4
       self.weightInit = 0
       self.new_shape = [98, 98, 54]
       self.convModule = 2
       self.convKSize = 5
       self.convStride = 1
       self.convPad = 2
       self.convDim = [64, 128, 256]
       self.normalizationFn = "instance"
       self.normalizationList = [0, 1, 1, 1]
       self.convDropout = [0, 0.1, 0.1, 0.1]
       self.convDropoutList = [0, 1, 1, 1]
       self.in_channels = 1
       self.mpoolWindow = 2
       self.mpoolStride = 2
       self.mpoolPad = 0
       self.mpool = [0, 1, 1, 1]
       self.activation = "relu"
       self.activationList = [1, 1, 1, 1]
       self.FCdim = [1000, 500, 200, 1]
       self.dropout = [0.5, 0.5, 0.5, 0]
       self.dropoutList = [1, 1, 1, 0]
       self.verbose = 0
       self.pred_model_dir = ""
       self.pred_model_file = ""
       

# Variable Definition
# -------------------------------
pd.set_option('display.max_columns', 20)
pd.set_option('display.max_rows', 50)

verbose = args.verbose

# DataFrame definition to store experiment results
keys = ["TestName", "TestDetails", "BestEpoch", "TrainingTime", "RMSE_tr", "MAE_tr", "STD_tr", "R_tr", "RMSE_v", "MAE_v", "STD_v", "R_v", "RMSE_t", "MAE_t", "STD_t", "R_t", "RMSE_t3", "MAE_t3", "STD_t3", "R_t3"]
results_df = pd.DataFrame(columns=keys)

# Load CSV Files (experiments)
paramFile = os.path.join(args.input_dir, args.input_file)
df_param = pd.read_csv(paramFile) 

print ("CNN Parameters Sample Output:\n")
print(df_param.head())

col = df_param.columns.values



# Loop through experiments in CSV file.
for index, row in df_param.iterrows():
    print (" ************************* Training Test %d from %s.\n\n" % (index, paramFile))

    # Initialise cnnParams class
    t = cnnParams()

    # Initialise results class
    trainResults = cnnResults()

    # Assign CNN parameters to cnnParams class
    for item in zip(col, row):
        if verbose:
            print ("Parameter: %20s \t Value: %35s \t Type: %15s" % (item[0], str(item[1]), type(item[1]) ))   
        setattr(t, item[0], item[1] ) # load dataframe. #use setattr from class. 

    # Cast to Integers all List Parameters
    t.convDim = [int(i) for i in t.convDim.split()]
    t.normalizationList = [int(i) for i in t.normalizationList.split()]
    t.convDropout = [float(i) for i in t.convDropout.split()]
    t.convDropoutList = [int(i) for i in t.convDropoutList.split()]
    t.mpool = [int(i) for i in t.mpool.split()]
    t.activationList = [int(i) for i in t.activationList.split()]
    t.FCdim = [int(i) for i in t.FCdim.split()]
    t.dropout = [float(i) for i in t.dropout.split()]
    t.dropoutList = [int(i) for i in t.dropoutList.split()]
    t.new_shape = [int(i) for i in t.new_shape.split()]

    print ("\n\n>>>>>>>>>>> Test %s - %s Starting:\n\n" % (t.TestName, t.Comments))  

    # Launch Training Script with required parameters
    if t.LigityVersion == 1:

        try:
            trainResults = train1d.main(t.input_dir, t.log_dir, t.log_testname, t.train_set, t.val_set, t.epoch, t.epochSave, t.checkpoints, t.epochValidate, 
                                    t.batchSize, t.rotations, t.learningRate, t.regl2, t.dataWorkers, t.weightInit, t.earlyStop, t.earlyStopFactor, 
                                    t.min_epochs, t.convModule, t.convKSize, t.convStride, t.convPad, t.convDim, t.normalizationFn, t.normalizationList, 
                                    t.convDropout, t.convDropoutList, t.in_channels, t.mpoolWindow, t.mpoolStride, t.mpoolPad, t.mpool, t.activation, 
                                    t.activationList, t.FCdim, t.dropout, t.dropoutList)
        except Exception as e: 
            print (">>>>>>>>>> Training Failed for %s.\n" % t.TestName)
            print (e)
            continue
 
    elif t.LigityVersion == 3:

        try:
            trainResults = train3d.main(t.input_dir, t.log_dir, t.log_testname, t.train_set, t.val_set, t.epoch, t.epochSave, t.checkpoints, t.epochValidate, 
                            t.batchSize, t.rotations, t.learningRate, t.regl2, t.dataWorkers, t.weightInit, t.new_shape, t.earlyStop, t.earlyStopFactor, 
                            t.min_epochs, t.convModule, t.convKSize, t.convStride, t.convPad, t.convDim, t.normalizationFn, t.normalizationList, 
                            t.convDropout, t.convDropoutList, t.in_channels, t.mpoolWindow, t.mpoolStride, t.mpoolPad, t.mpool, t.activation, 
                            t.activationList, t.FCdim, t.dropout, t.dropoutList)
        except Exception as e: 
            print (">>>>>>>>>> Training Failed for test %s.\n" % t.TestName)
            print (e)
            continue

    else:
        print ("LigityScore Version not correct.")

    #print ("+++++++++++++", t.mpool, t.convDim[1:], t.FCdim[1:] )

    # Define Predictions Parameters to use in prediction scripts
    t.pred_model_dir = os.path.join(t.log_dir, t.log_testname, "models")
    t.pred_model_file = "model-cnn-epoch" + str(trainResults.e)
    test_dataset_name = "Test"

    gc.collect()

    # Launch Prediction Script to Get Test Results
    try:
        predictResults = predict.main(t.input_dir, t.pred_model_dir, t.pred_model_file, test_dataset_name, t.verbose, t.LigityVersion, t.train_set, t.val_set, 
                                    t.new_shape, t.batchSize, t.rotations, t.learningRate, t.regl2, t.dataWorkers, t.convModule, t.convKSize, t.convStride, 
                                    t.convPad, t.convDim[1:], t.normalizationFn, t.normalizationList, t.convDropout, t.convDropoutList, t.in_channels, 
                                    t.mpoolWindow, t.mpoolStride, t.mpoolPad, t.mpool, t.activation, t.activationList, t.FCdim[1:], t.dropout, t.dropoutList)

        #print ("++++++++++++++++:", predictResults.RMSE_t)
        #print ("++++++++++++++++:", predictResults.RMSE_t3)
    
    except Exception as e:
        print ("+++++++++++ Prediction Failed for test %s.\n" % t.TestName)
        print (e)
        print('Failed to run prediction: '+ str(e))
        continue



    # Collect Training and Predictions results from scenario and store in dataframe. Include both Training and Testing Results
    values = [t.TestName, t.Comments, trainResults.e, trainResults.time, 
              trainResults.RMSE_tr, trainResults.MAE_tr, trainResults.STD_tr, trainResults.R_tr, 
              trainResults.RMSE_v, trainResults.MAE_v, trainResults.STD_v, trainResults.R_v, 
              predictResults.RMSE_t, predictResults.MAE_t, predictResults.STD_t, predictResults.R_t,
              predictResults.RMSE_t3, predictResults.MAE_t3, predictResults.STD_t3, predictResults.R_t3  ]

    
    # Append results stored in 'row' dictionary to dataframe
    row = dict(zip(keys, values))
    results_df = results_df.append(row , ignore_index=True)

    # Print DataFrame Results
    print (results_df)

    

# Print DataFrame Results
print (results_df)

# Export DataFrame with results to CSV
#output_filename = "batchresults" + str(t.LigityVersion) + "D.csv"
output_filename = "results-" + args.input_file
output_filepath = os.path.join (args.output_dir, output_filename) 
results_df.to_csv (output_filepath, index = False, header=True)

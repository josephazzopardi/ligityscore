"""
# Course: MSc AI
# Date: 02/03/2020
# Author: Joseph Azzopardi
# Credits:
# File: pdb-split-dataset.py
# Version: 1.0
# Description: Split Dataset into Training - Validation - Test Groups (Core-2013 and Core-2016). Program assumes the use of the PDBbind dataset and structure.
#              DataFrame for each set is saved in output directory.

#Features:
    1) Core set is the Test as this is the benchmark (CASF-2016).
    2) Core2013 is the Test2013 as this is another benchmark (CASF-2013)
    2) Choose number of validation proteins, and from with set, or mix
    3) Select if we: a) keep all for training, b)remained of refined set, c) general only
"""


import pandas as pd

import argparse
import math

from math import ceil

from sbvscnnmsc.sbvsHelper import loadDataFrame
from sbvscnnmsc.sbvsHelper import saveDataFrame

# Commands for Final Results 
##1D
# ----2016
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-0"  --df_in_filname "PDB2016-Lig-Features1D1-0" --output_dir "./LigityScore_Output/Ligity1D1-0" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-0-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-1"  --df_in_filname "PDB2016-Lig-Features1D1-1" --output_dir "./LigityScore_Output/Ligity1D1-1" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-1-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-25" --df_in_filname "PDB2016-Lig-Features1D1-25" --output_dir "./LigityScore_Output/Ligity1D1-25" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-25-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-4"  --df_in_filname "PDB2016-Lig-Features1D1-4" --output_dir "./LigityScore_Output/Ligity1D1-4" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-4-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-5"  --df_in_filname "PDB2016-Lig-Features1D1-5" --output_dir "./LigityScore_Output/Ligity1D1-5" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-5-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-6"  --df_in_filname "PDB2016-Lig-Features1D1-6" --output_dir "./LigityScore_Output/Ligity1D1-6" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-6-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-4L" --df_in_filname "PDB2016-Lig-Features1D1-4L" --output_dir "./LigityScore_Output/Ligity1D1-4L" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-4L-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-5L" --df_in_filname "PDB2016-Lig-Features1D1-5L" --output_dir "./LigityScore_Output/Ligity1D1-5L" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-5L-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-4-30" --df_in_filname "PDB2016-Lig-Features1D1-4-30" --output_dir "./LigityScore_Output/Ligity1D1-4-30" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-4-30-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-4-40" --df_in_filname "PDB2016-Lig-Features1D1-4-40" --output_dir "./LigityScore_Output/Ligity1D1-4-40" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-4-40-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-4-0-5" --df_in_filname "PDB2016-Lig-Features1D1-4-0-5" --output_dir "./LigityScore_Output/Ligity1D1-4-0-5" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-4-0-5-2016.txt
#
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-4"  --df_in_filname "PDB2016-Lig-Features1D1-4" --output_dir "./LigityScore_Output/Ligity1D1-4-seed1" --seed True 2>&1 | tee msc-results-dataSplit-console-output-1D-1-4-2016-seed1.txt
#
# ----2018
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-0-2018"  --df_in_filname "PDB2018-Lig-Features1D1-0" --output_dir "./LigityScore_Output/Ligity1D1-0-2018" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-0-2018.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-1-2018"  --df_in_filname "PDB2018-Lig-Features1D1-1" --output_dir "./LigityScore_Output/Ligity1D1-1-2018" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-1-2018.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-25-2018" --df_in_filname "PDB2018-Lig-Features1D1-25" --output_dir "./LigityScore_Output/Ligity1D1-25-2018" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-25-2018.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-4-2018"  --df_in_filname "PDB2018-Lig-Features1D1-4" --output_dir "./LigityScore_Output/Ligity1D1-4-2018" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-4-2018.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-5-2018"  --df_in_filname "PDB2018-Lig-Features1D1-5" --output_dir "./LigityScore_Output/Ligity1D1-5-2018" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-5-2018.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-6-2018"  --df_in_filname "PDB2018-Lig-Features1D1-6" --output_dir "./LigityScore_Output/Ligity1D1-6-2018" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-6-2018.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-4L-2018" --df_in_filname "PDB2018-Lig-Features1D1-4L" --output_dir "./LigityScore_Output/Ligity1D1-4L-2018" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-4L-2018.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-5L-2018" --df_in_filname "PDB2018-Lig-Features1D1-5L" --output_dir "./LigityScore_Output/Ligity1D1-5L-2018" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-5L-2018.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-4-30-2018" --df_in_filname "PDB2018-Lig-Features1D1-4-30" --output_dir "./LigityScore_Output/Ligity1D1-4-30-2018" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-4-30-2018.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-4-40-2018" --df_in_filname "PDB2018-Lig-Features1D1-4-40" --output_dir "./LigityScore_Output/Ligity1D1-4-40-2018" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-4-40-2018.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-4-0-5-2018" --df_in_filname "PDB2018-Lig-Features1D1-4-0-5" --output_dir "./LigityScore_Output/Ligity1D1-4-0-5-2018" 2>&1 | tee msc-results-dataSplit-console-output-1D-1-4-0-5-2018.txt
#
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity1D1-6-2018"  --df_in_filname "PDB2018-Lig-Features1D1-6" --output_dir "./LigityScore_Output/Ligity1D1-6-2018-seed1" --seed True 2>&1 | tee msc-results-dataSplit-console-output-1D-1-6-2018-seed1.txt
#
##3D
# ----2016
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity3D1-0"  --df_in_filname "PDB2016-Lig-Features3D1-0"  --output_dir "./LigityScore_Output/Ligity3D1-0"  2>&1 | tee msc-results-dataSplit-console-output-3D-1-0-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity3D1-1"  --df_in_filname "PDB2016-Lig-Features3D1-1"  --output_dir "./LigityScore_Output/Ligity3D1-1"  2>&1 | tee msc-results-dataSplit-console-output-3D-1-1-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity3D1-25" --df_in_filname "PDB2016-Lig-Features3D1-25" --output_dir "./LigityScore_Output/Ligity3D1-25" 2>&1 | tee msc-results-dataSplit-console-output-3D-1-25-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity3D1-4"  --df_in_filname "PDB2016-Lig-Features3D1-4"  --output_dir "./LigityScore_Output/Ligity3D1-4"  2>&1 | tee msc-results-dataSplit-console-output-3D-1-4-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity3D1-5"  --df_in_filname "PDB2016-Lig-Features3D1-5"  --output_dir "./LigityScore_Output/Ligity3D1-5"  2>&1 | tee msc-results-dataSplit-console-output-3D-1-5-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity3D1-6"  --df_in_filname "PDB2016-Lig-Features3D1-6"  --output_dir "./LigityScore_Output/Ligity3D1-6"  2>&1 | tee msc-results-dataSplit-console-output-3D-1-6-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity3D1-4L" --df_in_filname "PDB2016-Lig-Features3D1-4L" --output_dir "./LigityScore_Output/Ligity3D1-4L" 2>&1 | tee msc-results-dataSplit-console-output-3D-1-4L-2016.txt
#python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity3D1-5L" --df_in_filname "PDB2016-Lig-Features3D1-5L" --output_dir "./LigityScore_Output/Ligity3D1-5L" 2>&1 | tee msc-results-dataSplit-console-output-3D-1-5L-2016.txt
#
##python3 pdb-split-dataset.py --input_dir "./LigityScore_Output/Ligity3D1-4"  --df_in_filname "PDB2016-Lig-Features3D1-4" --output_dir "./LigityScore_Output/Ligity3D1-4-seed1" --seed True 2>&1 | tee msc-results-dataSplit-console-output-3D-1-4-seed1.txt
#
#
#python3 pdb-split-dataset.py --input_dir "/home/ubuntu/mymodel/dissertation-scripts/Ligity_Output"  --df_in_filname "PDB2016-Lig-Features-int16" --output_dir "/home/ubuntu/mymodel/dissertation-scripts/Ligity_Output/split-int16"
#python3 pdb-split-dataset.py --df_in_filname "PDB2016-Lig-Features"



# Script Arguments
# -------------------------------
parser = argparse.ArgumentParser(description='Ligity Hotpoint Extraction',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

io_group = parser.add_argument_group('Input & Output Files-Directories')
io_group.add_argument('--input_dir',  '-i', default='/home/joseph/msc/dissertation/myscripts/Ligity_Output',
                      help='Directory Dataset containing dataset to split.')
io_group.add_argument('--df_in_filname', default='PDB2016-Lig-Features',
                      help='Filename for saved dataframe to split.')
io_group.add_argument('--output_dir', '-o', default='/home/joseph/msc/dissertation/myscripts/Ligity_Output',
                      help='Directory to store output after Train-Test-Validation split.')


split_group = parser.add_argument_group('Variables to control how dataset will be split.')

split_group.add_argument('--valSamples', '-vS',  default=1000, type=int, 
                         help='Number of samples to use for Validation. Default is 1000.')
split_group.add_argument('--val_R_Only', '-vRO', default=True, 
                         help='Boolean variable - Set to \'True\' to set all Validation samples from Refined Set. Set to False to include also general.  Default is False')
split_group.add_argument('--valRatio',   '-vR',  default=[0.5, 0.5], 
                         help='f val_R_only is set to False, valRatio is used to split the Validation ratio from Refined and General sets respectively.')
split_group.add_argument('--trainOption','-t0',  default=3, type=int, 
                         help='Option to define which samples to use for Training. 1-remained of refined set, 2-general only, and 3-to keep all for training.')
split_group.add_argument('--seed',    '-s',   default=False, type=bool,
                         help='Set to True for different selection each time. Keep False for reproducibility (default)')
split_group.add_argument('--verbose',    '-v',   default=0, type=int, 
                         help='Enable Script Print output.')

args = parser.parse_args()


pd.set_option('display.max_columns', 20)

class datasets:
    """
    Class to store Return output. Train-Test-Vadidation datasets are class instances.
    """

    def __init__(self):
        self.Test = None
        self.Test2013 = None
        self.Validation = None
        self.Train = None



def pdbSplit(df, s, valSamples=1000, val_R_Only = True, valRatio = [0.5, 0.5], trainOption = 3, verbose=0):
    """
    Function to Split dataframe into Test-Training-Validation sets.

    Paramaters:
        df (DataFrame): Dataframe to split in Train-Test-Validation sets.
        s: seed value
        valSamples (int):  Number of samples to use for Validation. Default is 1000 
        val_R_Only (bool): Boolean variable - Set to 'True' to set all Validation samples from Refined Set. Set to False to include also general. 
        valRatio (List): If val_R_only is set to False, valRatio is used to split the Validation ratio from Refined and General sets respectively. 
        trainOption (int): Option to define which samples to use for Training. 1-remained of refined set, 2-general only, and 3-to keep all for training
    Return: 
        data (datasets): return of class datasets to includes are 4 Train-Test-Test2013-Validation datasets.
    """
    
    data = datasets()
    
    trainDict = {  1:"Use remaining of refined set", 2:"Use remaining General Only", 3:"Keep all for training (refined and general)",}

    if (sum(valRatio) != 1):
        raise ValueError('Total must be one. Provided %3.1f, but expected 1.0' % sum(valRatio) )
    if not(trainOption in range (1,4)):
        raise ValueError('Incorrect trainOption value. Provided %d, but expected %s' % (trainOption, range(1,3)))
    
    print ("Processing dataset to split in TRAIN - TEST- VALIDATION sets.\n")
    if verbose:
        print ("Input Dataframe Size: %30s" % str(df.shape))
        print ("Number of Validation Samples: %18d" % valSamples)
        print ("Used Refined Set Only for Validation: %10s" % (val_R_Only))
        print ("If Redined is not used for Validation only use %s split" % str(valRatio))
        print ("Training Option chosen: %10d" % trainOption)
        print ("\t", trainDict[trainOption])
        print ("\n\n")


    ## TEST SET 2013

    #drop only refined and general, since core are used for Test2016

    # Test = Core.Select the Test to core.
    testSet2013 = df[df['C2013'] == "core2013"]
    
    print (df.head(10))

    # drop test2013 samples in general set, and refined set only. 
    core2013_gen_ref_indices = (df[(df['pdbSplit'] == "core2013") & (df['C2013'] == "core2013")]).index
    print(df[(df['pdbSplit'] == "core2013") & (df['C2013'] == "core2013")])
    print("++++++++++++++++++", core2013_gen_ref_indices)
    df = df.drop(core2013_gen_ref_indices)
    
    #core2013_refined_indices = (df[(df['pdbSplit'] == "refined") & (df['C2013'] == "core2013")]).index
    #df = df.drop(core2013_refined_indices)

    df.reset_index(drop=True, inplace=True)


    
    ## TEST SET 2016
    # Test = Core.Select the Test to core.
    df['split'][df['pdbSplit'] == "core"] = "test"
    testSet = df[df['split'] == "test"]
    
    # drop test samples
    core_df_indices = df[df['split'] == "test"].index
    df = df.drop(core_df_indices)
    
    df.reset_index(drop=True, inplace=True)

    
    ## VALIDATION
    
    if (val_R_Only):   
        validationSetIndices = df[df['pdbSplit'] == "refined"].sample(n=valSamples, random_state=s).index
        validationSet = df.iloc[validationSetIndices, :]
        validationSet['split'] = "validation"
        #drop validation samples
        df = df.drop(validationSetIndices)
        print ("\n\nValidation Set Extracted from Refined Set only.\n\n")
        #(a['split'][a['pdbSplit'] == "refined"]).sample(n=1000, random_state=s) = "validation"
        #validationSet = a.select(a['split'] == "validation")
    else:
        R_samples = ceil(valRatio[0] * valSamples) 
        G_samples = ceil(valRatio[1] * valSamples)

        validationSetIndices_A = df[df['pdbSplit'] == "refined"].sample(n=R_samples, random_state=s).index
        validationSetIndices_B = df[df['pdbSplit'] == "general"].sample(n=G_samples, random_state=s).index
        validationSet = df.iloc[validationSetIndices_A, :]
        validationSet = validationSet.append(df.iloc[validationSetIndices_B, :])
        validationSet['split'] = "validation"
        
        #drop validation samples
        df = df.drop(validationSetIndices_A)
        df = df.drop(validationSetIndices_B)
    
    df.reset_index(drop=True, inplace=True)
      
    
    # TRAINING
    if not(trainOption in range (1,4)):
        raise ValueError('Incorrect trainOption value. Provided %d, but expected %s' % (trainOption, range(1,3)))
    else:
        if (trainOption == 1):
            # refined only for training
            trainSet = df[df['pdbSplit'] == "refined"]
            trainSet['split'] = "train"
        elif (trainOption == 2):
            # general only for training
            trainSet = df[df['pdbSplit'] == "general"]
            trainSet['split'] = "train"
        elif (trainOption == 3):
            # keep all remaining for training
            trainSet = df
            trainSet['split'] = "train"
        trainSet.reset_index(drop=True, inplace=True)
    
    data.Test  = testSet
    data.Test2013  = testSet2013
    data.Train = trainSet
    data.Validation = validationSet

        
    return data #return a class of type datasets
    

####################################################
####################################################
# Main Program
# --------------------------------------------------

# Variables Definition
# --------------------------------------------------
splitSets   = { "v":"Validation", "t":"Test", "tr":"Train", "t3":"Test2013"}

# Argument to specify different seed if a different validation set is required each time. Kept this same in compare models running on same Validation-Training sets
if args.seed == True:
    s = None
else:
    s = 123

print ("+++++", s)

# load Pandas DataFrame
# --------------------------------------------------
df = loadDataFrame (args.input_dir, args.df_in_filname, c="zip")


# Split Dataset and Save Output.
# --------------------------------------------------

# split datasets
dataset = pdbSplit(df, s, args.valSamples, args.val_R_Only, args.valRatio, args.trainOption, args.verbose)


# Validation 
print("Dataset Split:\n")
print("Validation set is of size: %15s." % str(dataset.Validation.shape))
print("Train set is of size: %20s." % str(dataset.Train.shape))
print("Test  set is of size: %20s." % str(dataset.Test.shape))
print("Test2013  set is of size: %16s.\n" % str(dataset.Test2013.shape))

# Enumerate Datasets names and save DataFrames to disk
for i, Set in enumerate(splitSets):
    #print (splitSets[Set])
    filename = splitSets[Set] + "-pkl"
    #saveDataFrame((dataset.Train), directory, file)
    saveDataFrame( eval("dataset.%s" % splitSets[Set]) , args.output_dir, filename)
    print ("Delete %s" % str(eval("dataset.%s" % splitSets[Set])))
    #eval("del dataset.%s" % splitSets[Set])




